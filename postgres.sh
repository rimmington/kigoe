#!/usr/bin/env bash

set -euo pipefail

MYTMPDIR="$(mktemp -d)"
trap 'rm -rf -- "$MYTMPDIR"' EXIT

socket="$MYTMPDIR/.s.PGSQL.5432"

printf 'Expecting socket at %s\n' "$socket"
mkdir -p pgsql-socket
ln -sf "$socket" pgsql-socket

initdb --no-locale -E UTF8 "$MYTMPDIR"
postgres -D "$MYTMPDIR" -k "$MYTMPDIR" -c "log_statement=all" -c "listen_addresses="
