{-# language GADTs, OverloadedStrings, PartialTypeSignatures, StrictData, UndecidableInstances #-}

module Main (main) where

import Control.Category qualified as C
import Data.Coerce (coerce)
import Data.Fixed (Fixed (MkFixed), Micro, Pico)
import Data.Foldable (find)
import Data.List.NonEmpty (head, nonEmpty, tail)
import Data.Scientific (Scientific, scientific)
import Data.SOP (All, I (..), K (..), NP (..))
import Data.Vector.Unboxed qualified as U
import Database.Kigoe
  (Array, Attr ((:@)), D1, D2, D3, ErrorResponse (ErrorResponse, code), KnownAttr, List, Named, Rel,
  SchemaRef, SqlNum, SqlType, Table, Ø, apply, arrayAgg, arrayAggSelectiveDistinct,
  arrayAggSelectiveOrderBy, asc, attempt, boolAnd, common, count, countSelective, desc, dontRetry,
  extend, firstNOrdered, forget, insert, is, isValues, join, joinOn, lit, manyDArray, max, nicht,
  oneDArray, ordered, outer, param, paramValues, project, query, queryInto, readOnly, readWrite,
  reexpress, remainder, rename, rollback, select, some, sumInt32, summarise, table, transact, unI,
  uncheckedFunction, uncheckedTableSchema, unnestAs, unnestWithOrdinality, unordered, update,
  values, withCommon, (!*), (<~), (=..), (∧), (∨), (≠), (≥))
import Database.Kigoe.Fielded (Fielded (..))
import Database.Kigoe.InformationSchema (informationSchema, no)
import Database.Kigoe.Internal.Array (dVectorArray, oneD, unsafeManyD)
import Database.Kigoe.Internal.AST
  (A (..), Agg (..), BinOp (..), C (Insert), Dir (..), E (..), L (..), LProj (..), Lit (..), T (..),
  UnaryOp (..), WithOrd (WithoutOrdinality), la, renderC, renderL, wa)
import Database.Kigoe.Internal.AST qualified as AST
import Database.Kigoe.Internal.PG qualified as P
import Database.Kigoe.Internal.SQL qualified as S
import Database.Kigoe.Internal.Transaction (isolationLevel)
import Database.Kigoe.Internal.Typed (Row (..))
import Database.Kigoe.Migrate
  (Check, ColDef, TransactionStateInvalid, addConstraint, boolean, check, col, createTable,
  dropConstraint, dropTable, emptySchema, ensureSequence, inSingleTransaction, int4, ioAction_,
  migration, migrationNames, mismatchedTableName, missingTableName, noTransaction, projectName,
  runMigration, runSequence, tableExists, text, trackingIn, unique, using, utcTime)
import Database.Kigoe.Null
  (coalesce, forNull, liftNull, maxSelective, selectNotNull, selectNullable)
import Database.Kigoe.Numeric (Numeric (..))
import Generics.SOP qualified as SOP
import GHC.Exts (fromList)
import GHC.Records (getField)
import Hedgehog hiding (Size, check)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Prelude (toEnum)
import RIO hiding (Int8, join, max, optional, some)
import RIO qualified
import RIO.ByteString (hPut)
import RIO.List (sort)
import RIO.Time (UTCTime (UTCTime), addUTCTime, getCurrentTime, secondsToNominalDiffTime)
import System.Environment (lookupEnv)
import System.Process.Typed (proc, runProcess_, withProcessTerm)
import Test.Hspec
import Test.Hspec.Hedgehog (hedgehog, modifyMaxDiscardRatio, modifyMaxSuccess)

main :: IO ()
main = withPostgres $ hspec spec

newtype X
  = X Int64
  deriving newtype (Eq, Ord, Show, SqlNum, SqlType)

newtype Y
  = Y (Maybe Int64)
  deriving newtype (Eq, Show, SqlType)

newtype R
  = R { y :: Int32 }
  deriving stock (Eq, Generic, Show)
  deriving anyclass (SOP.Generic, SOP.HasDatatypeInfo)

noRows :: [NP (Named I) as]
noRows = []

c0 :: SchemaRef '["c0" :@ Table '[] '[]]
c0 = uncheckedTableSchema Nothing #c0 Proxy

c2 :: SchemaRef '["c2" :@ Table '[] '["x" :@ Int64, "y" :@ Int64]]
c2 = uncheckedTableSchema (Just "public") #c2 Proxy

dbl :: SchemaRef '["dbl" :@ Table '[] '["x" :@ Int64, "twice" :@ Int64]]
dbl = uncheckedTableSchema Nothing #dbl Proxy

type RoomsTbl =
   '[ "number" :@ Text
    , "building" :@ Maybe Text
    , "area" :@ Int32
    , "populations" :@ Array D1 Int32
    , "opened" :@ UTCTime
    ]

rooms :: SchemaRef '["rooms" :@ Table '[] RoomsTbl]
rooms = uncheckedTableSchema Nothing #rooms Proxy

spec :: Spec
spec = dbSpec *> arraySpec *> fieldedSpec

dbSpec :: Spec
dbSpec = beforeAll_ setupTables $ around withConnection do
    describe "query interface" do
        it "executes an example" \c -> do
            let r :: List '[ "one" :@ Int32, "two" :@ Int64, "four" :@ Int64
                           , "false" :@ Bool, "true" :@ Bool ]
                          '["yo" :@ Int32]
                r = values [ #in <~ param #two  :* #keep <~ param #true
                           , #in <~ param #four :* #keep <~ param #true
                           , #in <~ param #two  :* #keep <~ param #false
                           ]
                    & rename #in #x
                    & join (table dbl #dbl)
                    & joinOn #twice
                        (values [#twice <~ param #four :* #yok <~ param #one])
                    & extend (#yo <~ #yok)
                    & select (#keep ∧ nicht (lit False) ∨ lit False)
                    & project #yo
                    & unordered
                ps = #one   `is` 1 :*
                     #two   `is` 2 :*
                     #four  `is` 4 :*
                     #false `is` False :*
                     #true  `is` True :*
                     Nil
            res <- query c r ps
            res `shouldBe` [#yo <~ I 1]
            ((\((forget #yo -> I yo) :* _) -> yo) <$> res) `shouldBe` [1]

        it "returns empty rows" \c -> do
            let r :: List '[] '["yo" :@ Int32]
                r = values [] & unordered
            query c r Nil `shouldReturn` noRows

        it "returns no empty rows" \c -> do
            let r :: List '[] '[]
                r = values [] & unordered
            query c r Nil `shouldReturn` noRows

        it "returns some empty rows" \c -> do
            let r :: List '[] '[]
                r = values [Nil, Nil]
                    & join (values [Nil, Nil])
                    & unordered
            query c r Nil `shouldReturn` replicate 4 Nil

        it "joins to a table with an empty heading" \c -> do
            let r :: List '[] '[ "x" :@ Bool ]
                r = values [#x <~ lit False]
                    & join (values [Nil, Nil])
                    & join (values [Nil, Nil])
                    & unordered
            query c r Nil `shouldReturn` replicate 4 (#x <~ I False)

        it "returns rows in order" \c -> do
            let r :: List '[] '[ "n" :@ Int32, "m" :@ X ]
                r = values (v <$> vs)
                    & firstNOrdered 14 (asc #n :| [asc #m])
                v (n, m) = #n <~ lit n :* #m <~ lit (X m)
                o (n, m) = #n <~ I n :* #m <~ I (X m)
                vs = [ (2, 4), (18, 13), (10, 12), (8, 13), (13, 9), (18, 9), (6, 20), (5, 1)
                     , (6, 5), (5, 2), (18, 17), (4, 7), (3, 10), (13, 2), (19, 7) ]
            query c r Nil `shouldReturn` o <$> take 14 (sort vs)

        it "works with OverloadedStrings" \c -> do
            let r :: List '[] '[ "x" :@ Text ]
                r = values [#x <~ "hello"]
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I "hello"]

        it "supports basic arithmetic" \c -> do
            let r :: List '[ "x" :@ X ] '[ "x" :@ X ]
                r = values [ #x <~ (param #x - 1)
                           , #x <~ signum (param #x)
                           ]
                    & ordered (asc #x :| [])
            query c r (#x `is` X 5 :* Nil) `shouldReturn`
                [ #x <~ I (X 1)
                , #x <~ I (X 4)
                ]

        it "supports applying a provided function" \c -> do
            let r :: List '[] '[ "l" :@ Int32, "t" :@ Text ]
                r = values [ #t <~ "abc", #t <~ "defghi" ]
                    & extend (#l <~ apply fun (#t :* Nil))
                    & ordered (asc #t :| [])
                fun = uncheckedFunction "character_length" (Proxy @'[Text]) (Proxy @Int32)
            query c r Nil `shouldReturn`
                [ #l <~ I 3 :* #t <~ I "abc"
                , #l <~ I 6 :* #t <~ I "defghi"
                ]

        it "works with Maybe" \c -> do
            let r :: List '[] '[ "x" :@ Maybe Int64, "y" :@ Y ]
                r = values [#x <~ lit Nothing :* #y <~ lit (Y Nothing)]
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I Nothing :* #y <~ I (Y Nothing)]

        it "works with Arrays" \c -> do
            let r :: List '[] '[ "x" :@ Array D1 Int64 ]
                r = values [#x <~ lit (oneDArray (fromList [1, 2, 3]))]
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I (oneDArray (fromList [1, 2, 3]))]

        it "works with Numeric" \c -> hedgehog do
            s10 <- forAll $ genNumeric 10 5
            let n10 :: Numeric 10 5
                n10 = Numeric s10
            m10 <- evalIO $ query c (values [#n <~ lit n10] & unordered) Nil
            [#n <~ I n10] === m10
            s128 <- forAll $ genNumeric 128 0
            let n128 :: Numeric 128 0
                n128 = Numeric s128
            m128 <- evalIO $ query c (values [#n <~ lit n128] & unordered) Nil
            [#n <~ I n128] === m128

        it "can unnest" \c -> do
            let r :: List '[] '[ "x" :@ Int32 ]
                r = unnestAs #x (lit . oneDArray $ fromList [1])
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I 1]

        it "can unnest with ordinality" \c -> do
            let r :: List '[] '[ "x" :@ Int32, "o" :@ Int64 ]
                r = unnestWithOrdinality #x #o (lit . oneDArray $ fromList [2, 1])
                    & ordered (asc #o :| [])
            query c r Nil `shouldReturn`
                [ #x <~ I 2 :* #o <~ I 1
                , #x <~ I 1 :* #o <~ I 2
                ]

        it "can count all" \c -> do
            let r :: List '[] '[ "c" :@ Int64 ]
                r = values (replicate 10 (#x <~ lit True))
                    & summarise Nil (#c <~ count)
                    & unordered
            query c r Nil `shouldReturn` [#c <~ I 10]

        it "can count groups" \c -> do
            let r :: List '[] '[ "x" :@ Bool, "c" :@ Int64 ]
                r = values [ #x <~ lit True
                           , #x <~ lit True
                           , #x <~ lit False
                           ]
                    & summarise #x (#c <~ count)
                    & ordered (asc #x :| [])
            query c r Nil `shouldReturn`
                [ #x <~ I False :* #c <~ I 1
                , #x <~ I True  :* #c <~ I 2
                ]

        it "returns zero groups for zero rows" \c -> do
            let r :: List '[] '[ "c" :@ Int64 ]
                r = values @_ @'[] []
                    & summarise Nil (#c <~ count)
                    & unordered
            query c r Nil `shouldReturn` noRows

        it "can countSelective" \c -> do
            let r :: List '[] '[ "c" :@ Int64 ]
                r = values [ #x <~ lit @Int32 1
                           , #x <~            2
                           , #x <~            2
                           ]
                    & summarise Nil (#c <~ countSelective (#x ≥ 2))
                    & unordered
            query c r Nil `shouldReturn` [#c <~ I 2]

        it "can sum" \c -> do
            let r :: List '[] '[ "s" :@ Int64 ]
                r = values [#x <~ 5, #x <~ 7]
                    & summarise Nil (#s <~ sumInt32 #x)
                    & unordered
            query c r Nil `shouldReturn` [#s <~ I 12]

        it "can max" \c -> do
            let r :: List '[] '[ "b" :@ Bool, "i" :@ Int64 ]
                r = values [ #b <~ lit True  :* #i <~ 5
                           , #b <~ lit False :* #i <~ 10
                           ]
                    & summarise Nil (#b <~ max #b :* #i <~ max #i)
                    & unordered
            query c r Nil `shouldReturn` [#b <~ I True :* #i <~ I 10]

        it "can boolAnd" \c -> do
            let r :: List '[] '[ "g" :@ Text, "a" :@ Bool ]
                r = values [ #g <~ "a" :* #x <~ lit True
                           , #g <~ "a" :* #x <~ lit True
                           , #g <~ "b" :* #x <~ lit True
                           , #g <~ "b" :* #x <~ lit False
                           ]
                    & summarise #g (#a <~ boolAnd #x)
                    & ordered (asc #g :| [])
            query c r Nil `shouldReturn`
                [ #g <~ I "a" :* #a <~ I True
                , #g <~ I "b" :* #a <~ I False
                ]

        it "can arrayAgg" \c -> do
            let r :: List '[] '[ "a" :@ Array D2 Int64 ]
                r = values [ #x <~ lit (oneDArray (fromList [1,2,3]))
                           , #x <~ lit (oneDArray (fromList [4,5,6]))
                           ]
                    & summarise Nil (#a <~ arrayAgg #x)
                    & unordered
            ex <- case manyDArray (K 2 :* K 3 :* Nil) (fromList [1..6]) of
                Just a  -> pure a
                Nothing -> error "bug: ex was Nothing"
            query c r Nil `shouldReturn` [#a <~ I ex]

        it "can arrayAggSelectiveDistinct" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = values
                        [ #x <~ 1 :* #y <~ lit @Int64 1
                        , #x <~ 2 :* #y <~ 5
                        , #x <~ 2 :* #y <~ 5
                        ]
                    & summarise Nil
                        (#a <~ arrayAggSelectiveDistinct (#y ≥ 5) #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [2])]

        it "can arrayAggSelectiveOrderBy" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = values
                        [ #x <~ 1
                        , #x <~ 2
                        , #x <~ 3
                        , #x <~ 1
                        ]
                    & summarise Nil
                        (#a <~ arrayAggSelectiveOrderBy
                            (#x `remainder` 2 ≥ 1)
                            (asc #x :| [])
                            #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [1, 1, 3])]

        it "can reexpress" \c -> do
            let r :: List '[] '[ "x" :@ Bool, "y" :@ Text ]
                r = values [#a <~ lit @Int32 1 :* #y <~ "hello" :* #z <~ "bye"]
                    & reexpress (#x <~ (#a ≥ 0) :* #y <~ #y)
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I True :* #y <~ I "hello"]

        it "can extend using a param" \c -> do
            let r :: List '[ "x" :@ Int64 ] '[ "y" :@ Int64 ]
                r = values [Nil]
                    & extend (#y <~ param #x)
                    & unordered
            query c r (#x `is` 5 :* Nil) `shouldReturn` [#y <~ I 5]

        it "can extend using both an existing attr and a param" \c -> do
            let r :: List '[ "x" :@ Int64 ] '[ "z" :@ Int64, "y" :@ Int64 ]
                r = values [#y <~ 10]
                    & extend (#z <~ (param #x + #y))
                    & unordered
            query c r (#x `is` 5 :* Nil) `shouldReturn` [#z <~ I 15 :* #y <~ I 10]

        it "can cte" \c -> do
            let r = withCommon (values [#y <~ lit @Int64 10]) common & unordered
            query c r Nil `shouldReturn` [#y <~ I 10]

        it "can nested cte" \c -> do
            let r = withCommon (withCommon (values [#y <~ lit @Int64 10]) common) common & unordered
            query c r Nil `shouldReturn` [#y <~ I 10]

        it "can cte nested" \c -> do
            let r = unordered $
                    withCommon (values [#y <~ lit @Int64 10]) \ys ->
                    withCommon (values [#z <~ lit @Int64 2, #z <~ 15]) \zs ->
                    common zs
                    & select (#z ≥ 12)
                    & join (common ys)
            query c r Nil `shouldReturn` [#z <~ I 15 :* #y <~ I 10]

        it "can select some" \c -> do
            let r = table dbl #dbl
                    & select (#x =.. some (table dbl #dbl & project #twice))
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I 2 :* #twice <~ I 4, #x <~ I 4 :* #twice <~ I 8]

        it "can select some after extend" \c -> do
            let r = table dbl #dbl
                    & extend (#y <~ (#x + 2))
                    & select (#y =.. some (table dbl #dbl & project #twice))
                    & unordered
            query c r Nil `shouldReturn`
                [ #y <~ I 4 :* #x <~ I 2 :* #twice <~ I 4
                , #y <~ I 6 :* #x <~ I 4 :* #twice <~ I 8
                ]

        it "can select some after join" \c -> do
            let r = table dbl #dbl
                    & join (table dbl #dbl & rename #twice #quad & rename #x #twice)
                    & select (#x =.. some (table dbl #dbl & project #twice))
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I 2 :* #twice <~ I 4 :* #quad <~ I 8]

        it "can express some" \c -> do
            let r = table dbl #dbl
                    & extend (#y <~ (#x + 2))
                    & extend (#reappears <~ (#x =.. some (table dbl #dbl & project #twice)))
                    & unordered
            query c r Nil `shouldReturn`
                [ #reappears <~ I False :* #y <~ I 3 :* #x <~ I 1 :* #twice <~ I 2
                , #reappears <~ I True  :* #y <~ I 4 :* #x <~ I 2 :* #twice <~ I 4
                , #reappears <~ I False :* #y <~ I 5 :* #x <~ I 3 :* #twice <~ I 6
                , #reappears <~ I True  :* #y <~ I 6 :* #x <~ I 4 :* #twice <~ I 8
                ]

        it "can use some in order" \c -> do
            let r = table dbl #dbl
                    & ordered (asc (#x =.. some (table dbl #dbl & project #twice)) :| [])
            query c r Nil `shouldReturn`
                [ #x <~ I 1 :* #twice <~ I 2
                , #x <~ I 3 :* #twice <~ I 6
                , #x <~ I 2 :* #twice <~ I 4
                , #x <~ I 4 :* #twice <~ I 8
                ]

        it "can use some in an aggregate expression" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Bool ]
                r = values
                        [ #x <~ lit @Int64 1
                        , #x <~ 2
                        , #x <~ 3
                        , #x <~ 1
                        ]
                    & summarise Nil
                        (#a <~ arrayAgg (#x =.. some (values [#x <~ 1])))
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [True, False, False, True])]

        it "can use some in an aggregate expression order by" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = values
                        [ #x <~ 1
                        , #x <~ 2
                        , #x <~ 3
                        , #x <~ 1
                        ]
                    & summarise Nil
                        (#a <~ arrayAggSelectiveOrderBy
                            (#x `remainder` 2 ≥ 1)
                            (asc (#x =.. some (values [#x <~ 1])) :| [])
                            #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [3, 1, 1])]

        it "can use some in an aggregate expression filter" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = values
                        [ #x <~ 1
                        , #x <~ 2
                        , #x <~ 3
                        , #x <~ 1
                        ]
                    & summarise Nil
                        (#a <~ arrayAggSelectiveOrderBy
                            (#x =.. some (values [#x <~ 1]))
                            (asc #x :| [])
                            #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [1, 1])]

        it "can use some containing count inside an aggregate expression" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Bool ]
                r = values
                        [ #x <~ lit @Int64 1
                        , #x <~ 2
                        , #x <~ 3
                        , #x <~ 1
                        ]
                    & summarise Nil
                        (#a <~ arrayAgg (#x =.. some (
                            values [#x <~ lit @Int64 1]
                            & summarise Nil (#c <~ count))))
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [True, False, False, True])]

        it "can use some containing max inside an aggregate expression" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Bool ]
                r = values
                        [ #x <~ lit @Int64 1
                        , #x <~ 2
                        , #x <~ 3
                        , #x <~ 1
                        ]
                    & summarise Nil
                        (#a <~ arrayAgg (#x =.. some (
                            values [#x <~ lit @Int64 1]
                            & summarise Nil (#c <~ max #x))))
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [True, False, False, True])]

        it "can use some containing count inside an aggregate expression filter" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = values
                        [ #x <~ 1
                        , #x <~ 2
                        , #x <~ 3
                        , #x <~ 1
                        ]
                    & summarise Nil
                        (#a <~ arrayAggSelectiveOrderBy
                            (#x =.. some (
                                values [#x <~ lit @Int64 1]
                                & summarise Nil (#c <~ count)))
                            (asc #x :| [])
                            #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [1, 1])]

        it "can use some containing max inside an aggregate expression filter" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = values
                        [ #x <~ 1
                        , #x <~ 2
                        , #x <~ 3
                        , #x <~ 1
                        ]
                    & summarise Nil
                        (#a <~ arrayAggSelectiveOrderBy
                            (#x =.. some (
                                values [#x <~ lit @Int64 1]
                                & summarise Nil (#c <~ max #x)))
                            (asc #x :| [])
                            #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [1, 1])]

        it "can select some containing summary" \c -> do
            let r = table dbl #dbl
                    & select (#x =.. some (table dbl #dbl & summarise Nil (#c <~ count)))
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I 4 :* #twice <~ I 8]

        it "can use outer" \c -> do
            let r = table dbl #dbl
                    & select (lit @Int64 1 =.. some (
                        table dbl #dbl
                        & select (#x ≥ outer #twice)
                        & reexpress (#i <~ 1)))
                    & unordered
            query c r Nil `shouldReturn`
                [ #x <~ I 1 :* #twice <~ I 2
                , #x <~ I 2 :* #twice <~ I 4
                ]

        it "can use outer in an aggregate expression" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Bool ]
                r = table dbl #dbl
                    & summarise Nil
                        (#a <~ arrayAgg (#x =.. some (values [#x <~ outer #twice])))
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [False, False, False, False])]

        it "can use outer in an aggregate expression order by" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = table dbl #dbl
                    & summarise Nil
                        (#a <~ arrayAggSelectiveOrderBy
                            (#x `remainder` 2 ≥ 1)
                            (asc (#x =.. some (values [#x <~ outer #twice])) :| [])
                            #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [1, 3])]

        it "can use outer in an aggregate expression filter" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = table dbl #dbl
                    & summarise Nil
                        (#a <~ arrayAggSelectiveOrderBy
                            (#x =.. some (values [#x <~ outer #twice]))
                            (asc #x :| [])
                            #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [])]

        it "can use summary and outer (under from) in an aggregate expression" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Bool ]
                r = table dbl #dbl
                    & summarise Nil
                        (#a <~ arrayAgg (#x =.. some (
                            values [#x <~ outer #twice]
                            & summarise Nil (#c <~ count))))
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [True, False, False, False])]

        it "can use summary and outer (under from) in an aggregate expression filter" \c -> do
            let r :: List '[] '[ "a" :@ Array D1 Int64 ]
                r = table dbl #dbl
                    & summarise Nil
                        (#a <~ arrayAggSelectiveOrderBy
                            (#x =.. some (
                                values [#x <~ outer #twice]
                                & summarise Nil (#c <~ count)))
                            (asc #x :| [])
                            #x)
                    & unordered
            query c r Nil `shouldReturn`
                [#a <~ I (oneDArray $ fromList [1])]

        it "can cte in some" \c -> do
            let r = table dbl #dbl
                    & select (#x =.. some (
                        withCommon (table dbl #dbl)
                        \dbl' ->
                        common dbl' & project #twice))
                    & unordered
            query c r Nil `shouldReturn` [#x <~ I 2 :* #twice <~ I 4, #x <~ I 4 :* #twice <~ I 8]

    describe "paramValues" do
        it "returns rows" \c -> do
            let r :: List '[ "vs" :@ Rel Ø '[ "i" :@ Int32, "x" :@ Text ]
                           , "ws" :@ Rel Ø '[ "x" :@ Text ]
                           ]
                          '[ "i" :@ Int32 ]
                r = paramValues #vs
                    & join (paramValues #ws)
                    & project #i
                    & unordered
                ps = #vs `isValues`
                        [ #i <~ I 5 :* #x <~ I "g"
                        , #i <~ I 6 :* #x <~ I "f"
                        , #i <~ I 8 :* #x <~ I "g"
                        ] :*
                     #ws `isValues`
                        [ #x <~ I "g"
                        ] :*
                     Nil
            query c r ps `shouldReturn`
                [ #i <~ I 8
                , #i <~ I 5
                ]

        it "returns some empty rows" \c -> do
            let r :: List '[ "vs" :@ Rel Ø '[] ] '[]
                r = paramValues #vs & unordered
                ps = #vs `isValues` [Nil, Nil] :*
                     Nil
            query c r ps `shouldReturn` [Nil, Nil]

        it "works with Maybe" \c -> do
            let r :: List '[ "vs" :@ Rel Ø '[ "i" :@ Maybe Int32 ] ] '[ "i" :@ Maybe Int32 ]
                r = paramValues #vs & ordered (asc #i :| [])
                vs = [#i <~ I (Just 1), #i <~ I Nothing]
                ps = #vs `isValues` vs :* Nil
            query c r ps `shouldReturn` vs

        it "works with Numeric" \c -> hedgehog do
            s10 <- forAll $ genNumeric 10 5
            let n10 :: Numeric 10 5
                n10 = Numeric s10
                r :: List '["ns" :@ Rel Ø '[ "n" :@ Numeric 10 5 ]] '[ "n" :@ Numeric 10 5 ]
                r = paramValues #ns & ordered (asc #n :| [])
                vs = [#n <~ I n10]
                ps = #ns `isValues` vs :* Nil
            m10 <- evalIO $ query c r ps
            [#n <~ I n10] === m10

        it "can unpack into a type" \c -> do
            let r :: List '[] '[ "x" :@ Bool, "y" :@ Int32 ]
                r = values [#x <~ lit True :* #y <~ 5]
                    & unordered
            queryInto c r Nil `shouldReturn` [R 5]

    describe "Database.Kigoe.Null" do
        it "has forNull" \c -> do
            let r :: List '[] '[ "b" :@ Maybe Bool, "i" :@ Maybe Int32 ]
                r = values [#i <~ lit (Just (1 :: Int32)), #i <~ lit Nothing]
                    & extend (#b <~ forNull #i (≥ 1))
                    & ordered (asc #i :| [])
            query c r Nil `shouldReturn`
                [ #b <~ I (Just True) :* #i <~ I (Just 1)
                , #b <~ I Nothing     :* #i <~ I Nothing
                ]

        it "has liftNull" \c -> do
            let r :: List '[] '[ "b" :@ Maybe Bool ]
                r = values [#b <~ lit True]
                    & extend (#b <~ liftNull #b)
                    & unordered
            query c r Nil `shouldReturn` [#b <~ I (Just True)]

        it "has coalesce" \c -> do
            let r :: List '[] '[ "b" :@ Bool ]
                r = values [ #b <~ lit Nothing
                           , #b <~ lit (Just True)
                           ]
                    & extend (#b <~ coalesce (#b :| []) (lit False))
                    & ordered (desc #b :| [])
            query c r Nil `shouldReturn`
                [#b <~ I True, #b <~ I False]

        it "has selectNullable" \c -> do
            let r :: List '[] '[ "b" :@ Maybe Bool ]
                r = values [ #b <~ lit Nothing
                           , #b <~ lit (Just True)
                           ]
                    & selectNullable (forNull #b id)
                    & unordered
            query c r Nil `shouldReturn` [#b <~ I (Just True)]

        it "has selectNotNull" \c -> do
            let r :: Rel Ø '[ "b" :@ Maybe Bool, "i" :@ Maybe Int32 ]
                r = values [ #b <~ lit Nothing :* #i <~ lit Nothing
                           , #b <~ lit Nothing :* #i <~ lit (Just 1)
                           , #b <~ lit (Just True) :* #i <~ lit Nothing
                           , #b <~ lit (Just True) :* #i <~ lit (Just 1)
                           ]
                exec ::
                    (All KnownAttr as)
                 => List '[] as -> IO [NP (Named I) as]
                exec s = query c s Nil
            exec (r & selectNotNull #b & ordered (asc #i :| [])) `shouldReturn`
                [ #b <~ I True :* #i <~ I (Just 1)
                , #b <~ I True :* #i <~ I Nothing
                ]
            exec (r & selectNotNull #i & ordered (asc #b :| [])) `shouldReturn`
                [ #i <~ I 1 :* #b <~ I (Just True)
                , #i <~ I 1 :* #b <~ I Nothing
                ]
            exec (r & selectNotNull (#i :* #b :* Nil) & unordered) `shouldReturn`
                [ #i <~ I 1 :* #b <~ I True ]

        it "has maxSelective" \c -> do
            let r :: List '[] '[ "b" :@ Maybe Bool ]
                r = values [ #b <~ lit True  :* #i <~ lit @Int64 5
                           , #b <~ lit False :* #i <~            10
                           ]
                    & summarise Nil (#b <~ maxSelective (#i ≥ 9) #b)
                    & unordered
            query c r Nil `shouldReturn` [#b <~ I (Just False)]

    describe "AST + PG" do
        it "returns params" \c -> hedgehog do
            n <- forAll $ Gen.text (Range.linear 1 40) Gen.alphaNum
            i <- forAll $ Gen.int32 Range.linearBounded
            let a = A n Int4
                e = Param Int4 1
                w = Unordered $ AST.Values [a] [pure e]
                s = S.renderSelect . fst $ renderL (U.singleton 1) w
            res <- evalIO $ P.oneshot c s [P.param i] [P.oneOid (Proxy @Int32)]
            res === [Row (#a <~ I i)]

        it "generates exprs of correct type" \_c -> hedgehog do
            pts <- forAll $ Gen.nonEmpty (Range.linear 1 20) genT
            t <- forAll genT
            e <- forAll $ genExprOf Any Have{haveParams = pts, haveAttrs = [], haveOuter = []} t
            AST.et e === t

        tweakHedgehog $ it "runs generated queries" \c -> hedgehog do
            ts <- forAll $ Gen.nonEmpty (Range.linear 1 20) genT
            w  <- forAll $ genL ts
            ps <- forAll $ traverse genLit ts
            (s, exec) <- evalNF $ prepPrunedL (litParam <$> toList ps) w
            annotateShow s
            void . evalIO $ exec c

        tweakHedgehog $ it "runs generated inserts" \c -> hedgehog do
            ts <- forAll $ Gen.nonEmpty (Range.linear 1 20) genT
            ic <- forAll $ genInsert ts
            ps <- forAll $ traverse genLit ts
            let ps' = litParam <$> toList ps
                (ii@S.Insert{src}, addParams) = renderC (U.fromList [1..length ps]) ic
                ps'' = ps' <> toList addParams
                rc = S.renderInsert ii
                Insert _ l = ic
                selg = S.selg src
                sName = \case
                    S.SelColumn al n -> fromMaybe n al
                    S.SelValue n _   -> n
            annotateShow rc
            -- Insert SQL gen expects column names to line up
            case nonEmpty $ aName <$> la l of
                Nothing -> length selg === 1
                Just ns -> ns === (sName <$> selg)
            void . evalIO $ P.oneshotCmd c rc ps''

    describe "insert" do
        it "inserts values" \c -> do
            -- postgresql-pure can't read the truncate table tag, so delete
            void $ P.oneshot @() c (S.RenderedSelect "delete from c2") [] []
            insert c c2 #c2 (values [#x <~ lit (1 :: Int64) :* #y <~ lit (2 :: Int64)]) Nil
                `shouldReturn` 1
            query c (table c2 #c2 & unordered) Nil
                `shouldReturn` [#x <~ I 1 :* #y <~ I 2]

        it "inserts a query" \c -> do
            void $ P.oneshot @() c (S.RenderedSelect "delete from c2") [] []
            let q = table dbl #dbl
                    & join (values [#x <~ lit 3])
                    & rename #twice #y
            insert c c2 #c2 q Nil `shouldReturn` 1
            query c (table c2 #c2 & unordered) Nil
                `shouldReturn` [#x <~ I 3 :* #y <~ I 6]

        it "handles the zero column case" \c -> do
            void $ P.oneshot @() c (S.RenderedSelect "delete from c0") [] []
            insert c c0 #c0 (values [Nil]) Nil `shouldReturn` 1

        it "handles the zero row case" \c -> do
            insert c c0 #c0 (values @_ @'[] []) Nil `shouldReturn` 0

        it "throws the expected exception on primary key violation" \c -> do
            P.oneshotCmd c (S.RenderedCmd "insert into p2 (x, y) values (1, 2), (1, 3)") []
                `shouldThrow` (\(ErrorResponse t code _ _) -> t == "ERROR" && code == "23505")

    describe "update" do
        it "updates values" \c -> do
            void $ P.oneshot @() c (S.RenderedSelect "delete from c2") [] []
            void $ P.oneshot @() c (S.RenderedSelect "insert into c2 (x, y) values (1, 2), (3, 4)") [] []
            update c c2 #c2
                (values [#x <~ lit (1 :: Int64) :* #b <~ lit True])
                #b
                (#y <~ lit 1)
                Nil
                `shouldReturn` 1
            query c (table c2 #c2 & ordered (asc #x :| [])) Nil
                `shouldReturn` [#x <~ I 1 :* #y <~ I 1, #x <~ I 3 :* #y <~ I 4]

        it "handles common and some" \c -> do
            update c c2 #c2
                (withCommon (values [Nil]) common)
                (#y =.. some (table dbl #dbl & project #twice))
                (#x <~ lit 1)
                Nil
                `shouldReturn` 1
            query c (table c2 #c2 & ordered (asc #y :| [])) Nil
                `shouldReturn` [#x <~ I 1 :* #y <~ I 1, #x <~ I 1 :* #y <~ I 4]

    describe "transactions" do
        for_ [minBound..maxBound] \iso -> before_ clearC0 $ do
            let descr = show iso
                mode = readWrite{ isolationLevel = iso }
            it ("commits " <> descr) \c -> do
                transact c mode (insert c c0 #c0 (values [Nil]) Nil)
                    `shouldReturn` 1
                query c (table c0 #c0 & unordered) Nil
                    `shouldReturn` [Nil]
            it ("rolls back " <> descr) \c -> do
                attempt c mode (insert c c0 #c0 (values [Nil]) Nil $> Left @() @() ())
                    `shouldReturn` Left ()
                query c (table c0 #c0 & unordered) Nil
                    `shouldReturn` []
        it "works with readOnly" \c ->
            transact c readOnly
                (query c
                    (table dbl #dbl & firstNOrdered 1 (pure (desc #x)))
                    Nil)
                `shouldReturn` [#x <~ I 4 :* #twice <~ I 8]
        it "retries on serialisation failure" \c1 -> do
            void $ P.oneshot @() c1 (S.RenderedSelect "delete from c2") [] []
            ready1 <- newEmptyMVar
            ready2 <- newEmptyMVar
            attemptCount <- newIORef (0 :: Integer)
            let tr1 = do
                    i <- transact c1 (readWrite & dontRetry) do
                        q <- query c1
                            (table c2 #c2 & summarise Nil (#count <~ count) & unordered)
                            Nil
                        putMVar ready1 ()
                        takeMVar ready2
                        n <- case q of
                            []                            -> pure 0
                            [(forget #count -> x) :* Nil] -> pure $ unI x
                            _                             -> error "Bad q"
                        insert c1 c2 #c2 (values [#x <~ lit (100 :: Int64) :* #y <~ lit n]) Nil
                    putMVar ready1 ()
                    putMVar ready1 ()
                    putMVar ready1 ()
                    pure i
                tr2 = withConnection \c2' -> transact c2' readWrite do
                    modifyIORef' attemptCount (+1)
                    takeMVar ready1
                    q <- query c2'
                        (table c2 #c2 & summarise Nil (#count <~ count) & unordered)
                        Nil
                    putMVar ready2 ()
                    takeMVar ready1
                    n <- case q of
                        []                            -> pure 0
                        [(forget #count -> x) :* Nil] -> pure $ unI x
                        _                             -> error "Bad q"
                    insert c2' c2 #c2 (values [#x <~ lit (101 :: Int64) :* #y <~ lit n]) Nil
            timeout 5e6 (concurrently tr1 tr2) `shouldReturn` Just (1, 1)
            readIORef attemptCount `shouldReturn` 2
        it "works with currentTransactionState" \c -> do
            P.currentTransactionState c `shouldReturn` P.Idle
            transact c readOnly (P.currentTransactionState c) `shouldReturn` P.Block

    describe "migrations" do
        it "preserves name order in Sequence" \_ ->
            migrationNames (migration "a" C.id >>> migration "b" C.id >>> migration "c" C.id)
                `shouldBe` ["a", "b", "c"]

        let roomsSchema :: NP ColDef RoomsTbl
            roomsSchema =
                text #number
             :* col #building (Proxy @(Maybe Text))
             :* int4 #area
             :* col #populations (Proxy @(Array D1 Int32))
             :* utcTime #opened
             :* Nil
        it "CREATE TABLE" \c -> do
            let q = query c (table (uncheckedTableSchema Nothing #rooms (Proxy @'[])) #rooms & unordered) Nil
            q `shouldThrow` (\ErrorResponse{} -> True)
            _ <- runMigration c emptySchema $
                createTable @'[] #rooms roomsSchema
            q `shouldReturn` []

        context "tableExists" do
            it "works" \c -> void $
                runMigration c emptySchema $ tableExists #rooms roomsSchema Nil

            it "notices if the table doesn't exist" \c -> do
                runMigration c emptySchema (tableExists #norooms roomsSchema Nil)
                    `shouldThrow` ((== "norooms") . missingTableName)

            it "notices nullability" \c -> do
                let badSchema =
                        text #number
                     :* text #building
                     :* int4 #area
                     :* col #populations (Proxy @(Array D1 Int32))
                     :* utcTime #opened
                     :* Nil
                runMigration c emptySchema (tableExists #rooms badSchema Nil)
                    `shouldThrow` ((== "rooms") . mismatchedTableName)

            it "notices array element type mismatch" \c -> do
                let badSchema =
                        text #number
                     :* col #building (Proxy @(Maybe Text))
                     :* int4 #area
                     :* col #populations (Proxy @(Array D1 Int64))
                     :* utcTime #opened
                     :* Nil
                runMigration c emptySchema (tableExists #rooms badSchema Nil)
                    `shouldThrow` ((== "rooms") . mismatchedTableName)

            it "doesn't notice array dimension mismatch" \c -> void do
                let badSchema =
                        text #number
                     :* col #building (Proxy @(Maybe Text))
                     :* int4 #area
                     :* col #populations (Proxy @(Array D2 Int32))
                     :* utcTime #opened
                     :* Nil
                runMigration c emptySchema (tableExists #rooms badSchema Nil)

            it "works with boolean" \c -> do
                void $ runMigration c emptySchema $ createTable #bools (boolean #someBool :* Nil)
                sch <- runMigration c emptySchema (tableExists #bools (boolean #someBool :* Nil) Nil)
                void $ runMigration c sch (dropTable #bools)

        context "unique constraints" do
            it "can be created" \c -> void .
                runMigration c rooms $
                    addConstraint #rooms (unique #location (#number :* #building))

            it "work with tableExists" \c -> void .
                runMigration c emptySchema $
                    tableExists #rooms roomsSchema
                        (unique #location (#number :* #building) :* Nil)

            it "tableExists notices a missing unique constraint" \c ->
                runMigration c emptySchema
                    (tableExists #rooms roomsSchema
                        (  unique #location (#number :* #building)
                        :* unique #area #area
                        :* Nil
                        ))
                    `shouldThrow` ((== "rooms") . mismatchedTableName)

            it "tableExists notices an extra unique constraint" \c ->
                runMigration c emptySchema (tableExists #rooms roomsSchema Nil)
                    `shouldThrow` ((== "rooms") . mismatchedTableName)

            it "can be dropped" \c -> void do
                let rooms' = coerce @_ @(SchemaRef '["rooms" :@ Table '["location" :@ '["number", "building"]] RoomsTbl]) rooms
                runMigration c rooms' $ dropConstraint #rooms #location

        context "check constraints" do
            it "can be created" \c -> void .
                runMigration c rooms $
                    addConstraint #rooms (check #has_space (#area ≥ 1))

            it "work with tableExists" \c -> void .
                runMigration c emptySchema $
                    tableExists #rooms roomsSchema
                        (check #has_space (#area ≥ 1) :* Nil)

            it "tableExists notices a missing check constraint" \c ->
                runMigration c emptySchema
                    (tableExists #rooms roomsSchema
                        (  check #has_space (#area ≥ 1)
                        :* check #not_massive (nicht (#area ≥ 1000))
                        :* Nil
                        ))
                    `shouldThrow` ((== "rooms") . mismatchedTableName)

            it "tableExists notices an extra check constraint" \c ->
                runMigration c emptySchema (tableExists #rooms roomsSchema Nil)
                    `shouldThrow` ((== "rooms") . mismatchedTableName)

            it "tableExists notices if a check constraint uses different columns" \c ->
                runMigration c emptySchema
                    (tableExists #rooms roomsSchema
                        (  check #has_space (#number ≠ "")
                        :* Nil
                        ))
                    `shouldThrow` ((== "rooms") . mismatchedTableName)

            it "are enforced" \c -> do
                sch <- runMigration c emptySchema $
                    tableExists #rooms roomsSchema
                        (check #has_space (#area ≥ 1) :* Nil)
                insert c sch #rooms
                    (values [ #area <~ 0
                           :* #number <~ "1"
                           :* #building <~ lit (Just "A")
                           :* #populations <~ lit (oneDArray mempty)
                           :* #opened <~ lit (UTCTime (toEnum 0) 0)
                           ])
                    Nil
                    `shouldThrow` ((== "23514") . code)

            it "can be dropped" \c -> void do
                let rooms' = coerce @_ @(SchemaRef '["rooms" :@ Table '["has_space" :@ Check] RoomsTbl]) rooms
                runMigration c rooms' $ dropConstraint #rooms #has_space

        it "can include an IO action" \c -> void $
            attempt c readWrite do
                sch <- runMigration c emptySchema $
                    createTable #test (text #col :* Nil) >>>
                    ioAction_ (\sr -> insert c sr #test (values [#col <~ lit "a"]) Nil)
                query c (table sch #test & unordered) Nil `shouldReturn` [#col <~ I "a"]
                pure $ Left ()

        let testSequence = migration "m1" m1 >>> migration "m2" m2
            m1 = createTable #tm1 (text #col :* Nil)
            m2 = createTable #tm2 (text #col :* Nil)

        it "runSequence applies without checking" \c ->
            attempt c readWrite (do
                _ <- runSequence c emptySchema testSequence
                _ <- runSequence c emptySchema testSequence
                pure $ Left ()
            ) `shouldThrow` ((== "42P07") . code)

        it "ensureSequence does check" \c -> void $
            attempt @IO c readWrite do
                _ <- ensureSequence (using c & noTransaction) emptySchema (migration "m1" m1)
                _ <- ensureSequence (using c & noTransaction) emptySchema testSequence
                _ <- ensureSequence (using c & noTransaction) emptySchema testSequence
                pure $ Left ()

        it "tracks in the same schema by default" \c -> do
            _ <- ensureSequence (using c)
                (uncheckedTableSchema (Just "another_schema") #zilch Proxy)
                (migration "anothert" (createTable #anothert Nil))
            let q = table informationSchema #tables
                    & join (values [#table_schema <~ lit "another_schema"])
                    & project #table_name
                    & ordered (asc #table_name :| [])
            query c q Nil `shouldReturn`
                [ #table_name <~ I "anothert"
                , #table_name <~ I "kigoe_migrations"
                ]

        it "can track in another schema" \c -> do
            ensureSequence (using c & trackingIn "another_schema_again")
                (uncheckedTableSchema (Just "another_schema") #zilch Proxy)
                (migration "anothert" (createTable #anothert Nil))
                `shouldThrow`
                ((== "42P07") . code)
            let q = table informationSchema #tables
                    & join (values [#table_schema <~ lit "another_schema_again"])
                    & project #table_name
                    & ordered (asc #table_name :| [])
            query c q Nil `shouldReturn`
                [ #table_name <~ I "kigoe_migrations"
                ]

        it "uses project name" \c -> void $
            attempt c readWrite (do
                _ <- ensureSequence
                    (using c & noTransaction)
                    emptySchema
                    testSequence
                _ <- ensureSequence
                    (using c & noTransaction & projectName "x")
                    emptySchema
                    testSequence
                pure $ Left ()
            ) `shouldThrow` ((== "42P07") . code)

        it "can run different projects in parallel" \c -> void $
            attempt c readWrite do
                _ <- ensureSequence (using c & noTransaction) emptySchema
                    (migration "m1" (createTable #tm1 (text #col :* Nil)))
                _ <- ensureSequence (using c & noTransaction & projectName "x") emptySchema
                    (migration "m1" (createTable #tm2 (text #col :* Nil)))
                pure $ Left ()

        it "checks if an IO action alters the transaction state" \c ->
            ensureSequence (using c & inSingleTransaction) emptySchema (
                migration "m1" $
                    m1 >>>
                    ioAction_ (\_ -> rollback c))
            `shouldThrow` const @_ @TransactionStateInvalid True

        it "can run in a single transaction" \c -> do
            let q = query c (table (uncheckedTableSchema Nothing #tm1 (Proxy @'[])) #tm1 & unordered) Nil
            _ <- runMigration c emptySchema (createTable #tm2 (text #col :* Nil))
            q `shouldThrow` (\ErrorResponse{} -> True)
            ensureSequence (using c & inSingleTransaction) emptySchema testSequence
                `shouldThrow` ((== "42P07") . code)
            q `shouldThrow` (\ErrorResponse{} -> True)

        it "can run using multiple transactions" \c -> do
            let q = query c (table (uncheckedTableSchema Nothing #tm1 (Proxy @'[])) #tm1 & unordered) Nil
            q `shouldThrow` (\ErrorResponse{} -> True)
            ensureSequence (using c) emptySchema testSequence
                `shouldThrow` ((== "42P07") . code)
            q `shouldReturn` []

        it "throws if already in a transaction" \c -> do
            transact c readWrite (ensureSequence (using c) emptySchema testSequence)
                `shouldThrow` const @_ @TransactionStateInvalid True

        it "DROP TABLE" \c -> do
            let q = query c (table (uncheckedTableSchema Nothing #rooms (Proxy @'[])) #rooms & unordered) Nil
            q `shouldReturn` []
            _ <- runMigration c rooms $ dropTable #rooms
            q `shouldThrow` (\ErrorResponse{} -> True)

    describe "Database.Kigoe.InformationSchema" do
        it "can list tables" \c -> do
            let q = table informationSchema #tables
                    & join (values [#table_schema <~ lit "public"])
                    & project #table_name
                    & firstNOrdered 2 (asc #table_name :| [])
            query c q Nil `shouldReturn`
                [ #table_name <~ I "c0"
                , #table_name <~ I "c2"
                ]

        it "can list columns" \c -> do
            let q = table informationSchema #columns
                    & join (values [#table_name <~ lit "c2"])
                    & project (#column_name :* #data_type :* #is_nullable)
                    & ordered (asc #column_name :| [])
            query c q Nil `shouldReturn`
                [ #column_name <~ I "x" :* #data_type <~ I "bigint" :* #is_nullable <~ I no
                , #column_name <~ I "y" :* #data_type <~ I "bigint" :* #is_nullable <~ I no
                ]

    describe "timestamp with time zone" $
        it "round-trips" \c -> do
            UTCTime day offset <- getCurrentTime
            -- Round - timestamptz has microsecond precision
            let now = UTCTime day (realToFrac (realToFrac offset :: Micro))
            sch <- runMigration c emptySchema $ createTable #times (utcTime #time :* Nil)
            insert c sch #times (values [#time <~ lit now]) Nil `shouldReturn` 1
            insert c sch #times (values [#time <~ param #time]) (#time `is` now :* Nil) `shouldReturn` 1
            query c (table sch #times & unordered) Nil `shouldReturn` replicate 2 (#time <~ I now)
  where
    tweakHedgehog = modifyMaxDiscardRatio (const 1000) . modifyMaxSuccess (const 2000)
    clearC0 = withConnection \c -> void $ P.oneshot @() c (S.RenderedSelect "delete from c0") [] []

arraySpec :: Spec
arraySpec = describe "Array" do
    it "can be produced with manyDArray" do
        manyDArray (K 6 :* K 2 :* K 2 :* Nil) (fromList [1..24::Int]) `shouldSatisfy` isJust
        manyDArray (K 2 :* Nil) (fromList [1::Int]) `shouldSatisfy` isNothing

    it "can be indexed with !*" do
        let r = manyDArray (K 6 :* K 2 :* K 2 :* Nil) (fromList [1..24::Int])
        r `shouldSatisfy` isJust
        for_ r \arr -> do
            arr !* (K 0 :* K 0 :* K 0 :* Nil) `shouldBe` Just 1
            arr !* (K 0 :* K 0 :* K 1 :* Nil) `shouldBe` Just 2
            arr !* (K 5 :* K 1 :* K 1 :* Nil) `shouldBe` Just 24
            arr !* (K 5 :* K 1 :* K 2 :* Nil) `shouldBe` Nothing
            arr !* (K 5 :* K 2 :* K 1 :* Nil) `shouldBe` Nothing
            arr !* (K 6 :* K 1 :* K 1 :* Nil) `shouldBe` Nothing
            arr !* (K (-1) :* K 0 :* K 0 :* Nil) `shouldBe` Nothing

fieldedSpec :: Spec
fieldedSpec = describe "Database.Kigoe.Fielded" $
    it "can getField" do
        let f = Fielded (#x <~ I True :* #y <~ I 1 :* #z <~ I False)
        getField @"y" f `shouldBe` (1 :: Int)

data Some f
  = forall a. Some (f a)

genInsert :: forall m. MonadGen m => NonEmpty T -> m C
genInsert ps = do
    w <- genW Have{haveParams = ps, haveAttrs = [], haveOuter = []}
    let as = wa w
        ia = (`A` Int8)
    case filter ((== Int8) . aType) as of
        [] -> pure $ Insert (S.object Nothing "c0") (Unordered $ AST.Project [] w)
        [a] -> do
            n <- Gen.filterT (`notElem` ("x" : (aName <$> as))) genIdentifier
            e <- genExprOf Any Have{haveParams = ps, haveAttrs = as, haveOuter = []} Int8
            pure .
                Insert (S.object Nothing "c2") .
                Unordered .
                AST.Rename n (ia "y") .
                AST.Rename (aName a) (ia "x") .
                AST.Project [a, A n Int8] $
                AST.Extend w (pure (n, e))
        (a:b:_) -> do
            n <- Gen.filterT (`notElem` [aName a, aName b, "y"]) genIdentifier
            pure .
                Insert (S.object Nothing "c2") .
                Unordered .
                AST.Rename n (ia "x") .
                AST.Rename (aName a) (ia "y") .
                AST.Rename (aName b) (ia n) $
                AST.Project [a, b] w

genL :: forall m. MonadGen m => NonEmpty T -> m L
genL ps = do
    w <- genW have
    Gen.choice
        [ pure $ Unordered w
        , Ordered <$> genOrders (wa w) <*> pure All <*> genLim <*> pure w ]
 where
    genOrders as = Gen.nonEmpty (Range.linear 1 (length as)) (genOrder have{haveAttrs = as})
    genLim = Gen.element [Nothing, Just 1, Just maxBound]
    have = Have{haveParams = ps, haveAttrs = [], haveOuter = []}

data Have
  = Have
    { haveParams :: NonEmpty T
    , haveAttrs  :: [A]
    , haveOuter  :: [A]
    }

genW :: forall m. MonadGen m => Have -> m AST.W
genW have = go
  where
    go = Gen.recursive Gen.choice
        [ genValues =<< genHeading
        , genTable
        , genUnnest
        ]
        [ Gen.subtermM (Gen.filterT (not . null. wa) go) genRename
        , Gen.subtermM go genProject
        , Gen.subtermM go genExtend
        , Gen.subtermM go genReexpress
        , Gen.subtermM go genJoin
        , Gen.subtermM go genSummarise
        , Gen.subtermM go genSelect
        ]
    genTable = pure $ AST.Table [A "x" Int8, A "twice" Int8] (S.object Nothing "dbl")
    genHeading = do
        ns <- genUniques (Range.linear 0 10) genIdentifier
        traverse (\n -> A n <$> genT) ns
    genValues want = do
        -- let subhave = Have{haveParams = haveParams have, haveAttrs = [], haveOuter = []}
        es <- Gen.list (Range.linear 1 10) $ traverse (genExprOf Any have{haveAttrs = []} . aType) want
        pure $ AST.Values (toList want) es
    genUnnest = do
        n <- genIdentifier
        elemT <- genArrayElemT
        -- let subhave = Have{haveParams = haveParams have, haveAttrs = [], haveOuter = []}
        e <- genExprOf Any have{haveAttrs = []} $ arrayT elemT
        pure $ AST.Unnest WithoutOrdinality (A n (baseT elemT)) e
    genRename w = do
        let as = wa w
        n <- genNewId as
        a <- Gen.element as
        pure $ AST.Rename (aName a) a{aName = n} w
    genProject w = do
        as <- Gen.filterT (not . null) . Gen.subsequence $ wa w
        as' <- Gen.shuffle as
        pure $ AST.Project as' w
    genExtend w = do
        ns <- genUniquesNE (Range.linear 1 10) . genNewId $ wa w
        nes <- traverse (\n -> (n,) <$> genExprAny have{haveAttrs = wa w}) ns
        pure $ AST.Extend w nes
    genReexpress w = do
        ns <- genUniques (Range.linear 0 10) genIdentifier
        nes <- traverse (\n -> (n,) <$> genExprAny have{haveAttrs = wa w}) ns
        pure $ AST.Reexpress w nes
    genJoin w = do
        let compatT n = case find ((== n) . aName) asl of
                Just a  -> pure $ aType a
                Nothing -> genT
            nsl = aName <$> asl
            asl = wa w
        nsr <- genUniques (Range.linear 0 10) $ Gen.frequency $
            (3, genIdentifier) : optional (not $ null nsl) [(1, Gen.element nsl)]
        asr <- traverse (\n -> A n <$> compatT n) nsr
        Gen.subterm2 (pure w) (genValues asr) AST.Join
    genSummarise w = do
        by <- Gen.subsequence as
        ns <- genUniques (Range.linear 1 10) (genNewId by)
        aggs <- traverse (\n -> (n,) <$> genAgg subhave) ns
        pure $ AST.Summarise by aggs w
      where
        as = wa w
        subhave = have{haveAttrs = as}
    genAgg subhave = Gen.choice
        [ CountTuples <$> Gen.maybe (genExprOf NotOuter subhave Boolean)
        , SumInt32 <$> genExprOf NotOuter subhave Int4
        , BoolAnd  <$> genExprOf NotOuter subhave Boolean
        , Max      <$> (genExprOf NotOuter subhave =<< genT) <*> pure Nothing
        , genArrayAgg subhave
        ]
    genArrayAgg subhave = do
        es <- genExprOf Small subhave . underT =<< genArrayElemT
        d <- Gen.element [S.Distinct, S.Indistinct]
        p <- Gen.maybe (genExprOf NotOuter subhave Boolean)
        os <- case d of
            S.Distinct   -> pure []
            S.Indistinct -> Gen.list (Range.linear 0 (length (haveAttrs subhave))) (genOrder subhave)
        pure $ ArrayAgg es d p os
    genSelect w = do
        b <- genExprOf Any have{haveAttrs = wa w} Boolean
        pure $ AST.Select b w
    genNewId as = Gen.filterT (`notElem` (aName <$> as)) genIdentifier
    genUniques :: (Ord a) => Range.Range Int -> m a -> m [a]
    genUniques r = fmap toList . Gen.set r
    genUniquesNE :: (Ord a) => Range.Range Int -> m a -> m (NonEmpty a)
    genUniquesNE r = Gen.justT . fmap nonEmpty . genUniques r

genIdentifier :: MonadGen m => m Text
genIdentifier = Gen.text (Range.linear 1 40) Gen.alphaNum

genOrder :: (MonadGen m) => Have -> m (E, Dir)
genOrder have = (,) <$> genExprAny have <*> Gen.element [Asc, Desc]

genExprAny :: (MonadGen m) => Have -> m E
genExprAny have = genExprOf Any have =<< genT

optional :: Bool -> [a] -> [a]
optional = flip $ bool []

data Size = Small | NotOuter | Any
  deriving (Eq)

genExprOf :: (MonadGen m) => Size -> Have -> T -> m E
genExprOf size have t = go
  where
    go = scale $ Gen.recursive Gen.choice simpl recur
    simpl = case size of
        Small ->
            [ Lit <$> genSmallLit t ]
        NotOuter ->
            notSmall
        Any ->
            notSmall
            ++ optional (not . null $ haveOuter')
            [ OuterE <$> Gen.element haveOuter' ]
      where
        notSmall =
            [ Lit <$> genLit t ]
            ++ optional (size == Any && (not . null $ ps'))
            [ uncurry Param <$> Gen.element ps' ]
            ++ optional (not . null $ haveAttrs')
            [ AttrE <$> Gen.element haveAttrs' ]
    recur =
        optional (t == Boolean)
        [ Gen.subtermM2 go go genLogical
        , Gen.subterm go (UnaryOp Not Boolean)
        -- Not subterms, cannot shrink to either side
        , genCmp
        , genQuant
        ]
        ++
        optional (t `elem` [Int2, Int4, Int8])
        [ Gen.subtermM2 (genExprOf Small have t) (genExprOf Small have t) (genBinArith t)
        , Gen.subtermM  (genExprOf size have t) (genUnArith t)
        ]
        ++
        optional (t == Int4)
        [ -- Not subterm, don't shink to a different type
          (\b -> FunOp "octet_length" Int4 [b]) <$> genExprOf size have Bytea
        ]
    haveOuter' = filter ((== t) . aType) (haveOuter have)
    haveAttrs' = filter ((== t) . aType) (haveAttrs have)
    ps' = filter ((== t) . fst) $ zip (toList (haveParams have)) [1..]
    scale = case size of
        Any      -> id
        NotOuter -> id
        Small    -> Gen.scale $ min 3
    genLogical l r = do
        op <- Gen.element [And, Or]
        pure $ BinOp op Boolean l r
    genBinArith t' l r = do
        let canRem = case r of
                Lit (LitInt2 i) -> i /= 0
                Lit (LitInt4 i) -> i /= 0
                Lit (LitInt8 i) -> i /= 0
                _               -> False
        op <- Gen.element $
            [Plus, Subtract, Mul]
            ++ optional canRem [Rem]
        pure $ BinOp op t' l r
    genUnArith t' e = do
        op <- Gen.element [Abs, Signum]
        pure $ UnaryOp op t' e
    genCmp = do
        st <- genT
        l <- genExprOf size have st
        r <- genExprOf size have st
        op <- Gen.element [GEq, NEq]
        pure $ BinOp op Boolean l r
    genQuant = do
        (w, as) <- Gen.justT $
            genW have{haveAttrs = [], haveOuter = haveAttrs have} <&>
            \w -> (w,) <$> nonEmpty (wa w)
        op <- Gen.element [GEq, NEq, Eq]
        e <- genExprOf size have (aType (head as))
        let w' = bool (AST.Project [head as] w) w (null (tail as))
        pure $ QuantOp op AST.Some e w'

genLit :: MonadGen m => T -> m Lit
genLit Int2    = LitInt2 <$> Gen.int16 Range.linearBounded
genLit Int4    = LitInt4 <$> Gen.int32 Range.linearBounded
genLit Int8    = LitInt8 <$> Gen.int64 Range.linearBounded
genLit Boolean = LitBoolean <$> Gen.bool
genLit TimestampTz = LitTimestampTz <$> genUTC (MkFixed 0) (MkFixed (fromIntegral (maxBound :: Int64)))
genLit (NumericFixed p s) = LitNumF p s <$> genNumeric p s
genLit t@Varchar = LitAppendParam t . P.param <$> Gen.text (Range.linear 0 100) Gen.unicode
genLit t@Bytea = LitAppendParam t . P.param <$> Gen.bytes (Range.linear 0 100)
genLit t@(ArrayOf Int4) = LitAppendParam t . P.uvecParam . oneD . fromList @(U.Vector _) <$>
    Gen.list (Range.linear 0 10) (Gen.int32 Range.linearBounded)
genLit t@(ArrayOf Varchar) = LitAppendParam t . P.param . dVectorArray . oneD . fromList @(Vector Text) <$>
    Gen.list (Range.linear 0 10) (Gen.text (Range.linear 0 100) Gen.unicode)
genLit t@(ArrayOf (ArrayOf Int4)) = do
    d1 <- Gen.int $ Range.linear 0 4
    d2 <- Gen.int $ Range.linear 1 4
    vs <- Gen.list (Range.singleton (d1 * d2)) (Gen.int32 Range.linearBounded)
    pure . LitAppendParam t . P.uvecParam . unsafeManyD (K d1 :* K d2 :* Nil) $
        fromList @(U.Vector _) vs
genLit t@(ArrayOf (ArrayOf Boolean)) = do
    d1 <- Gen.int $ Range.linear 0 4
    d2 <- Gen.int $ Range.linear 1 4
    vs <- Gen.list (Range.singleton (d1 * d2)) Gen.bool
    pure . LitAppendParam t . P.uvecParam . unsafeManyD (K d1 :* K d2 :* Nil) $
        fromList @(U.Vector _) vs
genLit t@(ArrayOf (ArrayOf (ArrayOf Boolean))) = do
    d1 <- Gen.int $ Range.linear 0 4
    d2 <- Gen.int $ Range.linear 1 4
    d3 <- Gen.int $ Range.linear 1 4
    vs <- Gen.list (Range.singleton (d1 * d2 * d3)) Gen.bool
    pure . LitAppendParam t . P.uvecParam . unsafeManyD (K d1 :* K d2 :* K d3 :* Nil) $
        fromList @(U.Vector _) vs
genLit (ArrayOf _) = error "bug: genList ArrayOf incomplete"

-- No zero-sized arrays
genSmallLit :: MonadGen m => T -> m Lit
genSmallLit Int2    = LitInt2 <$> Gen.int16 (Range.linear (-4) 4)
genSmallLit Int4    = LitInt4 <$> Gen.int32 (Range.linear (-20) 20)
genSmallLit Int8    = LitInt8 <$> Gen.int64 (Range.linear (-100) 100)
genSmallLit Boolean = LitBoolean <$> Gen.bool
genSmallLit TimestampTz = LitTimestampTz <$> genUTC 1673754969 1682394969
genSmallLit (NumericFixed p s) = LitNumF p s <$> genNumeric (min 3 p) (min 2 s)
genSmallLit Varchar = LitAppendParam Varchar . P.param <$> Gen.text (Range.linear 0 10) Gen.unicode
genSmallLit t@Bytea = LitAppendParam t . P.param <$> Gen.bytes (Range.linear 0 10)
genSmallLit t@(ArrayOf Int4) = LitAppendParam t . P.param . dVectorArray . oneD . pure @Vector <$>
    Gen.int32 (Range.linear (-20) 20)
genSmallLit t@(ArrayOf Varchar) = LitAppendParam t . P.param . dVectorArray . oneD . pure @Vector <$>
    Gen.text (Range.linear 0 10) Gen.unicode
genSmallLit t@(ArrayOf (ArrayOf Int4)) =
    LitAppendParam t . P.param . dVectorArray . unsafeManyD (K 1 :* K 1 :* Nil) . pure @Vector <$>
    Gen.int32 (Range.linear (-20) 20)
genSmallLit t@(ArrayOf (ArrayOf Boolean)) =
    LitAppendParam t . P.param . dVectorArray . unsafeManyD (K 1 :* K 1 :* Nil) . pure @Vector <$> Gen.bool
genSmallLit t@(ArrayOf (ArrayOf (ArrayOf Boolean))) =
    LitAppendParam t . P.param . dVectorArray . unsafeManyD (K 1 :* K 1 :* K 1 :* Nil) . pure @Vector <$> Gen.bool
genSmallLit (ArrayOf _) = error "bug: genSmallLit ArrayOf incomplete"

genUTC :: MonadGen f => Pico -> Pico -> f UTCTime
genUTC bot top = (`addUTCTime` UTCTime (toEnum 0) 0) . secondsToNominalDiffTime <$>
    Gen.realFrac_ (Range.linearFrac bot top)

genNumeric :: MonadGen m => Int -> Int -> m Scientific
genNumeric p s = do
    sig <- Gen.integral (Range.linear 1 p)
    let mx = 10 ^ sig - 1
        minS = RIO.max 0 (sig - (p - s))
    scientific
        <$> Gen.integral (Range.linear (negate mx) mx)
        <*> Gen.int (Range.linear (negate s) (negate minS))

genT :: MonadGen m => m T
genT = Gen.frequency
    [ (5, Gen.element [Int2, Int4, Int8, Boolean, Varchar, TimestampTz, NumericFixed 128 2])
    , (1, arrayT <$> genArrayElemT)
    ]

data ArrayElemT
  = ArrayElemT
    { baseT  :: T
    , underT :: T
    }

arrayT :: ArrayElemT -> T
arrayT = ArrayOf . underT

-- | (elem, array)
genArrayElemT :: MonadGen m => m ArrayElemT
genArrayElemT = Gen.element
    [ ArrayElemT Int4 Int4
    , ArrayElemT Varchar Varchar
    , ArrayElemT Int4 (ArrayOf Int4)
    , ArrayElemT Boolean (ArrayOf (ArrayOf Boolean))
    ]

prepPrunedL ::
    [P.Param]
 -> L
 -> (S.RenderedSelect, P.Connection -> IO [Some Row])
prepPrunedL passed l = (s, exec)
  where
    (S.renderSelect -> s, addParams) = renderL (U.fromList [1..length passed]) l'
    ps = passed <> toList addParams
    exec c = case la l' of
        [] -> fmap (const . Some $ Row Nil) <$> P.oneshot @Bool c s ps [P.oneOid (Proxy @Bool)]
        [A{aType = t}] -> case t of
            Int2        -> fmap Some <$> P.oneshot @(Row '[ "" :@ Int16 ]) c s ps [P.oneOid (Proxy @Int16)]
            Int4        -> fmap Some <$> P.oneshot @(Row '[ "" :@ Int32 ]) c s ps [P.oneOid (Proxy @Int32)]
            Int8        -> fmap Some <$> P.oneshot @(Row '[ "" :@ Int64 ]) c s ps [P.oneOid (Proxy @Int64)]
            Boolean     -> fmap Some <$> P.oneshot @(Row '[ "" :@ Bool ]) c s ps [P.oneOid (Proxy @Bool)]
            Varchar     -> fmap Some <$> P.oneshot @(Row '[ "" :@ Text ]) c s ps [P.oneOid (Proxy @Text)]
            Bytea       -> fmap Some <$> P.oneshot @(Row '[ "" :@ ByteString ]) c s ps [P.oneOid (Proxy @ByteString)]
            TimestampTz -> fmap Some <$> P.oneshot @(Row '[ "" :@ UTCTime ]) c s ps [P.oneOid (Proxy @UTCTime)]
            NumericFixed _ _ -> fmap Some <$> P.oneshot @(Row '[ "" :@ Numeric 128 2 ]) c s ps [P.oneOid (Proxy @Scientific)]
            ArrayOf Int4 ->
                fmap Some <$> P.oneshot @(Row '[ "" :@ Array D1 Int32 ]) c s ps [P.arrOid (Proxy @Int32)]
            ArrayOf Varchar ->
                fmap Some <$> P.oneshot @(Row '[ "" :@ Array D1 Text ]) c s ps [P.arrOid (Proxy @Text)]
            ArrayOf (ArrayOf Int4) ->
                fmap Some <$> P.oneshot @(Row '[ "" :@ Array D2 Int32 ]) c s ps [P.arrOid (Proxy @Int32)]
            ArrayOf (ArrayOf (ArrayOf Boolean)) ->
                fmap Some <$> P.oneshot @(Row '[ "" :@ Array D3 Bool ]) c s ps [P.arrOid (Proxy @Bool)]
            ArrayOf _ -> error "bug: prepPrunedL ArrayOf incomplete"
        [A{aType = Int4}, A{aType = Int4}] ->
            fmap Some <$> P.oneshot @(Row '[ "" :@ Int32, "" :@ Int32 ]) c s ps [P.oneOid (Proxy @Int32), P.oneOid (Proxy @Int32)]
        [A{aType = Int4}, A{aType = Int4}, A{aType = Int4}] ->
            fmap Some <$> P.oneshot @(Row '[ "" :@ Int32, "" :@ Int32, "" :@ Int32 ]) c s ps [P.oneOid (Proxy @Int32), P.oneOid (Proxy @Int32), P.oneOid (Proxy @Int32)]
        _ -> error "bug: > 3 attrs in prepPrunedL"
    l'
      | ints == as = l
      | [] <- ints = bool (proj (take 1 as) l) l (length as == 1)
      | otherwise  = proj ints l
      where
        ints = take 3 $ filter ((== Int4) . aType) as
        as = la l
    proj as' = \case
        Unordered w      -> Unordered $ AST.Project as' w
        Ordered os _ f w -> Ordered os (LProj as') f w

litParam :: Lit -> P.Param
litParam = \case
    LitInt2 i        -> P.param i
    LitInt4 i        -> P.param i
    LitInt8 i        -> P.param i
    LitBoolean i     -> P.param i
    LitTimestampTz i -> P.param i
    LitNumF _ _ n    -> P.param n
    LitNull t -> case t of
      Int2             -> P.param (Nothing :: Maybe Int16)
      Int4             -> P.param (Nothing :: Maybe Int32)
      Int8             -> P.param (Nothing :: Maybe Int64)
      Boolean          -> P.param (Nothing :: Maybe Bool)
      Varchar          -> P.param (Nothing :: Maybe Text)
      Bytea            -> P.param (Nothing :: Maybe ByteString)
      TimestampTz      -> P.param (Nothing :: Maybe UTCTime)
      NumericFixed _ _ -> P.param (Nothing :: Maybe Scientific)
      ArrayOf u        -> nullArray u
    LitAppendParam _ p -> p
  where
    nullArray :: T -> P.Param
    nullArray = \case
        Int2             -> P.param (Nothing :: Maybe (Array '[] Int16))
        Int4             -> P.param (Nothing :: Maybe (Array '[] Int32))
        Int8             -> P.param (Nothing :: Maybe (Array '[] Int64))
        Boolean          -> P.param (Nothing :: Maybe (Array '[] Bool))
        Varchar          -> P.param (Nothing :: Maybe (Array '[] Text))
        Bytea            -> P.param (Nothing :: Maybe (Array '[] ByteString))
        TimestampTz      -> P.param (Nothing :: Maybe (Array '[] UTCTime))
        NumericFixed _ _ -> P.param (Nothing :: Maybe (Array '[] Scientific))
        ArrayOf t        -> nullArray t

setupTables :: IO ()
setupTables = withConnection \c -> do
    let ddl stmt = void $ P.oneshotCmd c (S.RenderedCmd (encodeUtf8 stmt)) []
    ddl "create schema another_schema"
    ddl "create schema another_schema_again"
    ddl "create table dbl (x int8 not null, twice int8 not null)"
    ddl "create table p2 (x int8 not null, y int8 not null, primary key (x))"
    ddl "create table c2 (x int8 not null, y int8 not null)"
    ddl "create table c0 (§γη†ħempty boolean not null)"
    ddl "insert into dbl values (1, 2), (2, 4), (3, 6), (4, 8)"

withConnection :: (P.Connection -> IO a) -> IO a
withConnection = P.withConnection P.defConfig

withPostgres :: IO () -> IO ()
withPostgres act = withSystemTempDirectory "postgres" \tmp -> do
    runProcess_ $ proc "initdb" ["--no-locale", "-E", "UTF8", "-U", "postgres", tmp]
    inNixShell <- lookupEnv "IN_NIX_SHELL"
    when (isJust inNixShell) $
        withFile (tmp <> "/postgresql.conf") AppendMode (`hPut` "log_statement = 'all'\n")
    withProcessTerm (proc "postgres" ["-D", tmp, "-k", tmp]) . const $
        threadDelay 1e6 *> act
