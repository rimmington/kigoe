{
  inputs.haskell-nix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskell-nix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.haskell-nix-utils.url = "gitlab:rimmington/haskell-nix-utils";
  inputs.stylish-haskell = {
    url = "github:rimmington/stylish-haskell/fl";
    inputs.haskell-nix.follows = "haskell-nix";
    inputs.haskell-nix-utils.follows = "haskell-nix-utils";
    inputs.flake-utils.follows = "flake-utils";
  };
  inputs.tuple = {
    url = "github:rimmington/tuple/onetuple-0.3";
    flake = false;
  };
  inputs.sop-static = {
    url = "gitlab:rimmington/sop-static";
    flake = false;
  };
  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };
  inputs.postgresql-pure = {
    url = "github:rimmington/postgresql-pure/changes";
    flake = false;
  };
  outputs = { self, nixpkgs, flake-utils, haskell-nix, haskell-nix-utils, tuple, sop-static, postgresql-pure, stylish-haskell, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
    let
      overlays = [
        haskell-nix.overlay
        haskell-nix-utils.overlays.default
      ];
      pkgs = import nixpkgs { inherit system overlays; inherit (haskell-nix) config; };
      utils = pkgs.haskell-nix-utils;
      project = utils.stackageProject' {
        resolver = "lts-22.4";
        name = "kigoe";
        src = utils.cleanGitHaskellSource { name = "kigoe-src"; src = self; };
        cabalFile = ./kigoe.cabal;
        fromSrc.sop-static.src = "${sop-static}";
        fromSrc.homotuple.src = "${tuple}/homotuple";
        fromSrc.postgresql-pure.src = "${postgresql-pure}";
        fromSrc.single-tuple.src = "${tuple}/single-tuple";
        fromHackage = utils.hackage-sets.hls_2_5_0_0 // {
          # postgresql-pure = "0.2.3.0";
            either-result = "0.3.1.0";
            # homotuple = "0.2.0.0";
            list-tuple = "0.1.3.0";
            postgresql-placeholder-converter = "0.2.0.0";
            # single-tuple = "0.1.2.0";
        };
        modules = [
          {
            packages.kigoe.components.tests.unit.testWrapper = [
              "env PATH=${pkgs.postgresql_15}/bin:$PATH"
            ];
          }
          {
            packages.homotuple.components.library.doHaddock = false;
            packages.list-tuple.components.library.doHaddock = false;
            packages.postgresql-pure.components.library.doHaddock = false;
          }
          {
            # https://github.com/haskell/haskell-language-server/issues/3185#issuecomment-1250264515
            packages.hlint.flags.ghc-lib = true;
            # https://github.com/haskell/haskell-language-server/blob/5d5f7e42d4edf3f203f5831a25d8db28d2871965/cabal.project#L67
            packages.ghc-lib-parser-ex.flags.auto = false;
            packages.stylish-haskell.flags.ghc-lib = true;

            # Disable unused formatters that take a while to build
            packages.haskell-language-server.flags.fourmolu = false;
            packages.haskell-language-server.flags.ormolu = false;
          }
        ];
        shell = {
          withHoogle = false;
          exactDeps = true;
          withHaddock = true;
          nativeBuildInputs = [
            pkgs.cabal-install
            pkgs.postgresql_15
            stylish-haskell.packages.${system}."stylish-haskell:exe:stylish-haskell"
            project.hsPkgs.haskell-language-server.components.exes.haskell-language-server
            project.roots
          ];
        };
      };
      additionalPackages = {
        cabal-docspec = pkgs.fetchurl {
          name = "cabal-docspec";
          url = "https://github.com/phadej/cabal-extras/releases/download/cabal-docspec-0.0.0.20211114/cabal-docspec-0.0.0.20211114.xz";
          sha256 = "sha256-wNvmiRsLb/4rgUkGQAMZv5j0TB4rtmM+KFSJ4tJFy0s=";
          downloadToTemp = true;
          executable = true;
          postFetch = "${pkgs.xz}/bin/xz -d < $downloadedFile > $out && chmod +x $out";
        };
        hackageDoc = utils.hackageLinkedDoc project.hsPkgs.kigoe.components.library.doc;
        ciBaseImage = utils.baseImage {
          name = "base-image";
          compilerNixName = project.pkg-set.config.compiler.nix-name;
          extraPaths = [
            { name = "haskell-nix-src"; path = haskell-nix.outPath; }
          ];
        };
      };
      additionalChecks = {
        docspec = pkgs.stdenv.mkDerivation {
          name = "docspec-kigoe";
          src = self;
          phases = [ "unpackPhase" "checkPhase" ];
          doCheck = true;
          nativeBuildInputs = [ (project.ghcWithPackages (ps: [ ps.kigoe ])) ];
          checkPhase = ''
            export HOME=$NIX_BUILD_TOP/home LANG=C.UTF-8
            mkdir -p ~/.cabal
            touch ~/.cabal/config
            ${additionalPackages.cabal-docspec} -vvv --no-cabal-plan kigoe.cabal 2>&1 | tee $out
          '';
        };
      };
    in pkgs.lib.recursiveUpdate project.flake' {
      checks = additionalChecks;
      packages = additionalPackages;
      hydraJobs.checks = additionalChecks;
      hydraJobs.packages = { inherit (additionalPackages) hackageDoc; };
    });

  nixConfig = {
    allow-import-from-derivation = "true";
  };
}
