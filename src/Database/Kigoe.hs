-- | For some of the examples in this module, define:
--
-- @
-- someTable n as = table (uncheckedTableSchema Nothing n as) n
-- @
module Database.Kigoe
  ( -- * Bag relations
    Rel, Attr (..)
    -- ** Introduction
  , Label
  , values, paramValues, table
  , unnestAs, unnestWithOrdinality
  , CommonRef, withCommon, common
    -- ** Modification
  , AttrRef
  , project, select, rename, extend, reexpress, summarise
   -- ** Combination
  , join, joinOn
    -- * Lists
  , List
    -- ** Introduction
  , unordered, ordered, firstNOrdered
  , Order, asc, desc
    -- * Expressions
  , Expr
    -- ** Introduction
  , lit, param
    -- ** Modification
  , nicht
  , characterLength, octetLength, byteLength
  , FunRef, uncheckedFunction, apply
    -- ** Combination
  , (∧), (∨)
  , (≥), (≠)
  , remainder
    -- ** Quantified comparisons
  , Quant, some, outer
  , (=..), (≠..), (≥..)
    -- * Summaries
  , Summary
  , count, countSelective, sumInt32, max, boolAnd
  , arrayAgg, arrayAggSelectiveDistinct, arrayAggSelectiveOrderBy
    -- * Naming
  , Named
  , (<~), forget
  , Name
    -- * Execution
  , query, queryInto, insert, update
    -- ** Providing parameters
  , Is
  , is, isValues
    -- ** Exceptions
  , ErrorResponse (..), UnexpectedResponse (..)
    -- * Arrays
  , Array, Dim (D), D1, D2, D3
    -- ** Introduction
  , oneDArray, manyDArray
    -- ** Elimination
  , arrayElements, arrayLengths
  , (!?), (!*)
    -- * Schemas
  , SchemaRef, Table
  , uncheckedTableSchema
    -- * Connection
  , Config (..), Address (..), defConfig
  , Connection, withConnection, connect, disconnect
    -- * Transactions
  , transact, attempt
  , TransactionSettings, readOnly, readWrite
  , serialisable, repeatableRead, readCommitted, readUncommitted
  , retrySerializationFailure, dontRetry
  , startTransaction, commit, rollback
    -- * Classes
  , SqlType, SqlNum, SqlOrd, ArrayOf, KnownAttr, KnownNonArrayAttr
    -- ** Context
  , Ø, HasAttr, HasParam, HasOuter, QuantCtx, CommonCtx
    -- * Re-exports
  , NP (..), I (..), unI
  ) where

import Data.SOP.BasicFunctors (I (..), unI)
import Data.SOP.NP (NP (..))
import Database.Kigoe.Fielded (Label, Name, Named, forget, (<~))
import Database.Kigoe.Internal.Array
  (Array, D1, D2, D3, Dim (..), arrayElements, arrayLengths, manyDArray, oneDArray, (!*), (!?))
import Database.Kigoe.Internal.Ctx (CommonCtx, HasAttr, HasOuter, HasParam, QuantCtx, Ø)
import Database.Kigoe.Internal.PG
  (Config (..), Connection, UnexpectedResponse (..), defConfig, withConnection)
import Database.Kigoe.Internal.Schema (SchemaRef, Table, uncheckedTableSchema)
import Database.Kigoe.Internal.Set (Attr (..))
import Database.Kigoe.Internal.Transaction
  (TransactionSettings, attempt, commit, dontRetry, readCommitted, readOnly, readUncommitted,
  readWrite, repeatableRead, retrySerializationFailure, rollback, serialisable, startTransaction,
  transact)
import Database.Kigoe.Internal.Typed
import Database.PostgreSQL.Pure.List (Address (..), ErrorResponse (..), connect, disconnect)
