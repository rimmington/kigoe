{-# language OverloadedStrings, UndecidableInstances #-}

-- | The information schema consists of a set of views that contain information about the objects defined in the current database.
--
-- https://www.postgresql.org/docs/current/information-schema.html
module Database.Kigoe.InformationSchema
    ( InformationSchema
    , CharacterData (..)
    , int4DataType, int8DataType, textDataType, booleanDataType
    , SqlIdentifier (..)
    , YesOrNo, yes, no
    , CardinalNumber (..)
    , informationSchema
    ) where

import Database.Kigoe (Attr (..), SqlOrd, SqlType, Table)
import Database.Kigoe.Internal.Schema (SchemaRef (SchemaRef))
import RIO

type InformationSchema =
   '[ "constraint_column_usage" :@ Table '[]
       '[ "table_catalog"      :@ SqlIdentifier
        , "table_schema"       :@ SqlIdentifier
        , "table_name"         :@ SqlIdentifier
        , "column_name"        :@ SqlIdentifier
        , "constraint_catalog" :@ SqlIdentifier
        , "constraint_schema"  :@ SqlIdentifier
        , "constraint_name"    :@ SqlIdentifier
        ]
    , "columns" :@ Table '[]
       '[ "table_catalog"     :@ SqlIdentifier
        , "table_schema"      :@ SqlIdentifier
        , "table_name"        :@ SqlIdentifier
        , "column_name"       :@ SqlIdentifier
        , "is_nullable"       :@ YesOrNo
        , "data_type"         :@ CharacterData
        , "udt_schema"        :@ SqlIdentifier
        , "udt_name"          :@ SqlIdentifier
        , "numeric_precision" :@ Maybe CardinalNumber
        , "numeric_scale"     :@ Maybe CardinalNumber
        ]
    , "tables" :@ Table '[]
       '[ "table_catalog" :@ SqlIdentifier
        , "table_schema"  :@ SqlIdentifier
        , "table_name"    :@ SqlIdentifier
        ]
    , "table_constraints" :@ Table '[]
        '[ "table_catalog"      :@ SqlIdentifier
         , "table_schema"       :@ SqlIdentifier
         , "table_name"         :@ SqlIdentifier
         , "constraint_catalog" :@ SqlIdentifier
         , "constraint_schema"  :@ SqlIdentifier
         , "constraint_name"    :@ SqlIdentifier
         , "constraint_type"    :@ CharacterData
         ]
    ]

newtype CharacterData
  = CharacterData Text
  deriving newtype (Eq, Hashable, IsString, Ord, Show, SqlOrd, SqlType)

newtype SqlIdentifier
  = SqlIdentifier Text
  deriving newtype (Eq, Hashable, IsString, Ord, Show, SqlType)

newtype YesOrNo
  = YesOrNo Text
  deriving newtype (Eq, Hashable, Ord, Show, SqlType)

newtype CardinalNumber
  = CardinalNumber Int32
  deriving newtype (Enum, Eq, Hashable, Integral, Num, Ord, Real, Show, SqlType)

yes :: YesOrNo
yes = YesOrNo "YES"

no :: YesOrNo
no = YesOrNo "NO"

int4DataType :: CharacterData
int4DataType = "integer"

int8DataType :: CharacterData
int8DataType = "bigint"

textDataType :: CharacterData
textDataType = "text"

booleanDataType :: CharacterData
booleanDataType = "boolean"

-- | The information schema is available (unless a superuser has deleted it).
informationSchema :: SchemaRef InformationSchema
informationSchema = SchemaRef $ Just "information_schema"
