module Database.Kigoe.Numeric where

import Data.Scientific (Scientific)
import GHC.TypeLits (Nat)
import RIO

-- | Fixed-precision numeric type.
-- @precision@ is the total count of significant digits in the whole number.
-- @scale@ is the count of decimal digits in the fractional part, to the right of the
-- decimal point.
-- The scale is a fixed allocation of space to the fractional part;
-- a value must round to an absolute value less than @10 ^ (precision - scale)@.
newtype Numeric (precision :: Nat) (scale :: Nat)
  = Numeric { getScientific :: Scientific }
  deriving newtype (Eq, Hashable, Ord, Show)
