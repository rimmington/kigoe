module Database.Kigoe.Migrate
  ( -- * Schemas
    SchemaRef, Table, emptySchema, uncheckedSchemaExists
    -- * Single migrations
  , Migration, runMigration
  , createTable, tableExists, dropTable, ioAction, ioAction_
  , ColDef, int2, int4, int8, boolean, text, bytea, utcTime, col
  , DropTable
    -- ** Table constraints
  , addConstraint, dropConstraint
  , unique, check
  , AddConstraint, DropConstraint, Constraint (..), ColRef, ConstraintDef
    -- * Sequences
  , Sequence
  , ensureSequence, runSequence, migrationNames
  , migration
  , Config, using, projectName, trackingIn, noteInfo
  , perMigrationTransactions, inSingleTransaction, noTransaction
    -- * Exceptions
  , TableMismatch, mismatchedTableName
  , TableMissing, missingTableName
  , TransactionStateInvalid
  ) where

import Database.Kigoe.Internal.Migrate
import Database.Kigoe.Internal.Schema
