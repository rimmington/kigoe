module Database.Kigoe.Internal.Schema where

import Database.Kigoe.Fielded (Label)
import Database.Kigoe.Internal.Set (Attr (..))
import Database.Kigoe.Internal.SQL qualified as S
import GHC.TypeLits (KnownSymbol, symbolVal)
import RIO
import RIO.Text qualified as T

-- | A reference to an existing schema.
newtype SchemaRef schema
  = SchemaRef (Maybe Text)

-- Making this "type data" causes haddock to panic
-- May be https://github.com/haskell/haddock/issues/1601
data SchemaPart
  = Tbl [Attr] [Attr]
  | Unused

-- | A base SQL table.
type Table constraints columns = 'Tbl constraints columns

-- | Assert a database table exists, without checking.
uncheckedTableSchema ::
    forall name as.
    Maybe Text -- ^ Schema name, or 'Nothing' for the current schema
 -> Label name -- ^ Table name
 -> Proxy as
 -> SchemaRef '[name :@ Table '[] as]
uncheckedTableSchema sn _ _ = SchemaRef sn

schemaObject :: (KnownSymbol name) => SchemaRef schema -> Label name -> S.ObjectName
schemaObject (SchemaRef sn) lbl = S.object sn (T.pack (symbolVal lbl))
