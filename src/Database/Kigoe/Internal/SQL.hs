{-# language DuplicateRecordFields, OverloadedStrings, StrictData #-}

module Database.Kigoe.Internal.SQL
  ( Select (..), Selecting (..), CTE (..)
  , FromPart (..), Alias
  , WithOrdinality (..)
  , Value (..), Distinct (..), Order (..), T (..)
  , RenderedSelect (..), renderSelect
  , Insert (..), Merge (..)
  , RenderedCmd (..), renderInsert, renderMerge
  , Nullable (..), Col (..), renderCreateTable, renderDropTable
  , renderAddCheckConstraint, renderAddUniqueConstraint, renderDropConstraint
  , ObjectName, object
  , mergeSourceName, mergeTargetName
  ) where

import Data.Scientific (FPFormat (Fixed), Scientific, formatScientific)
import RIO hiding (set)
import RIO.ByteString qualified as BS
import RIO.Time (UTCTime, defaultTimeLocale, formatTime)

data Insert
  = Insert
    { tbl  :: ObjectName
    , cols :: NonEmpty Text
    , src  :: Select
    }

data Merge
  = Merge
    { ctes :: [CTE]
    , tbl  :: ObjectName
    , src  :: Select
    , cond :: Value
    , asgn :: NonEmpty (Text, Value)
    }

data Select
  = Select
    { ctes :: [CTE]
    , selg :: NonEmpty Selecting
    , frmp :: Maybe FromPart
    , wher :: [Value]
    , gpby :: [Value]
    , ordr :: [Order]
    , limt :: Maybe Word32
    }
  deriving (Show)

data CTE
  = CTE Text Select
  deriving (Show)

data Selecting
  = SelColumn (Maybe Text) Text
  | SelValue Text Value
  deriving (Show)

data WithOrdinality
  = WithOrdinality Text
  | WithoutOrdinality
  deriving (Show)

type Alias = Int

data FromPart
  = ValuesAs (NonEmpty Text) (NonEmpty (NonEmpty Value)) Alias -- Need alias to name columns
  | UnnestAs WithOrdinality (NonEmpty Text) (NonEmpty Value) Alias
  | Table ObjectName (Maybe Alias)
  | Subselect Select Alias -- Subselect in from must have an alias
  | Join FromPart (NonEmpty FromPart)
  deriving (Show)

data T
  = Int2
  | Int4
  | Int8
  | Boolean
  | Varchar
  | Bytea
  | TimestampTz
  | NumericFixed Int Int
  | ArrayOf T
  deriving (Eq, Ord, Show)

data Value
  = Param Int
  | ColRef ObjectName
  | LitInt2 Int16
  | LitInt4 Int32
  | LitInt8 Int64
  | LitBoolean Bool
  | LitTimestampTz UTCTime
  | LitNumF Int Int Scientific
  | LitEmptyArray T
  | InfixOp Value ByteString Value
  | PrefixOp ByteString Value
  | SuffixOp Value ByteString
  | FunOp ByteString [Value]
  | AggOp ByteString Distinct [Value] (Maybe Value) [Order]
  | QuantOp ByteString Value Select
  | Cast T Value
  | NullOf T
  deriving (Show)

data Distinct = Distinct | Indistinct
  deriving (Show)

data Order
  = Asc Value
  | Desc Value
  deriving (Show)

-- | Pre-escaped, possibly-qualified object name.
newtype ObjectName
  = ObjectName { onBs :: ByteString }
  deriving newtype (Show)

-- | A statement not expected to return records/tuples.
newtype RenderedCmd
  = RenderedCmd ByteString
  deriving newtype (Eq, NFData, Show)

-- | A @SELECT@ statement.
newtype RenderedSelect
  = RenderedSelect { rsBs :: ByteString }
  deriving newtype (NFData, Show)

data Col
  = Col Text T Nullable
  deriving (Show)
data Nullable = Nullable | NotNullable
  deriving (Show)

renderCreateTable :: ObjectName -> [Col] -> RenderedCmd
renderCreateTable n cs = RenderedCmd $
    "create table " <> onBs n <> " " <> parens (commaMap rc cs)
  where
    rc (Col cn t nl) = nullrobe nl $ identifier cn <> " " <> renderT t
    nullrobe Nullable    = id
    nullrobe NotNullable = (<> " not null")

renderDropTable :: ObjectName -> RenderedCmd
renderDropTable n = RenderedCmd $
    "drop table " <> onBs n <> " restrict"

renderAddUniqueConstraint ::
    Text -- ^ constraint name
 -> ObjectName -- ^ table name
 -> NonEmpty Text -- ^ column names
 -> RenderedCmd
renderAddUniqueConstraint cn tn cs = RenderedCmd $
    "alter table " <> onBs tn <>
    " add constraint " <> identifier cn <>
    " unique " <> parens (commaMap identifier cs)

renderAddCheckConstraint ::
    Text -- ^ constraint name
 -> ObjectName -- ^ table name
 -> Value -- ^ predicate
 -> RenderedCmd
renderAddCheckConstraint cn tn p = RenderedCmd $
    "alter table " <> onBs tn <>
    " add constraint " <> identifier cn <>
    " check " <> parens (renderValue p)

renderDropConstraint ::
    Text -- ^ constraint name
 -> ObjectName -- ^ table name
 -> RenderedCmd
renderDropConstraint cn tn = RenderedCmd $
    "alter table " <> onBs tn <>
    " drop constraint " <> identifier cn <>
    " restrict"

renderInsert :: Insert -> RenderedCmd
renderInsert Insert{tbl, cols, src} = RenderedCmd $
    "insert into " <> onBs tbl <> " " <>
    parens (commaMap identifier cols) <> " " <>
    rsBs (renderSelect src)

renderMerge :: Merge -> RenderedCmd
renderMerge (Merge ctes tbl src cond asgn) = RenderedCmd $
    renderWith ctes <> "merge into " <> onBs tbl <>
    encodeUtf8 " as §γη†ħtgt using " <> parens (rsBs (renderSelect src)) <>
    encodeUtf8 " as §γη†ħsrc on " <> renderValue cond <>
    " when matched then update set " <> commaMap set asgn
  where
    set (t, v) = identifier t <> " = " <> renderValue v

mergeSourceName :: Text
mergeSourceName = "§γη†ħsrc"

mergeTargetName :: Text
mergeTargetName = "§γη†ħtgt"

-- TODO: use builder
renderSelect :: Select -> RenderedSelect
renderSelect (Select ctes cols from wher gpby ords n) =
    let sels = renderSelecting <$> toList cols
        ord  = renderOrders ords
        lim  = maybe "" (\i -> " fetch first " <> fromString (show i) <> " rows only") n
        grp
          | null gpby = ""
          | otherwise = " group by " <> commaMap renderValue gpby
        frm = maybe "" ((" from " <>) . renderFrom) from
        cte = renderWith ctes
    in RenderedSelect $
        cte <> "select " <> commaSep sels <> frm <> renderWhere wher <> grp <> ord <> lim

renderWith :: [CTE] -> ByteString
renderWith ctes
  | null ctes = ""
  | otherwise = "with " <> commaMap renderCTE ctes <> " "

renderCTE :: CTE -> ByteString
renderCTE (CTE t (renderSelect -> RenderedSelect s)) = identifier t <> " as " <> parens s

renderSelecting :: Selecting -> ByteString
renderSelecting (SelColumn Nothing  c) = identifier c
renderSelecting (SelColumn (Just n) c) = identifier c  <> " as " <> identifier n
renderSelecting (SelValue n v)         = renderValue v <> " as " <> identifier n

renderFrom :: FromPart -> ByteString
renderFrom (ValuesAs ns rs alias) =
    "(values " <> commaMap renderRow rs <>
    ") as " <> renderAlias alias <> " (" <> commaMap identifier ns <> ")"
  where
    renderRow vs = parens $ commaMap renderValue vs
renderFrom (UnnestAs wOrd ns es alias) =
    "unnest(" <> commaMap renderValue es <>
    inf <> renderAlias alias <> " (" <> commaSep ids <> ")"
  where
    ids = identifier <$> case wOrd of
        WithoutOrdinality -> toList ns
        WithOrdinality n  -> toList ns ++ [n]
    inf = case wOrd of
        WithoutOrdinality -> ") as "
        WithOrdinality _  -> ") with ordinality as "
renderFrom (Subselect (renderSelect -> RenderedSelect s) alias) =
    parens s <> " as " <> renderAlias alias
renderFrom (Join p0 ps) =
    BS.intercalate " natural join " froms
  where
    froms = renderFrom <$> p0 : toList ps
renderFrom (Table (ObjectName n) malias) = n <> renderAliasing malias

renderAliasing :: Maybe Alias -> ByteString
renderAliasing = \case
    Nothing -> mempty
    Just t  -> " as " <> renderAlias t

renderAlias :: Int -> ByteString
renderAlias i = encodeUtf8 "§γη†ħt" <> fromString (show i)

renderWhere :: [Value] -> ByteString
renderWhere [] = mempty
renderWhere vs = " where " <> BS.intercalate " and " (renderValue <$> vs)

renderValue :: Value -> ByteString
renderValue (Param i)          = "$" <> fromString (show i)
renderValue (ColRef n)         = onBs n
renderValue (NullOf t)         = "null::" <> renderT t
renderValue (LitInt2 i)        = fromString (show i) <> "::int2"
renderValue (LitInt4 i)        = fromString (show i) <> "::int4"
renderValue (LitInt8 i)        = fromString (show i) <> "::int8"
-- For some daft reason PG broke "group by TRUE" in 15+ but adding a cast is fine
-- https://www.postgresql.org/message-id/flat/CAJBCwCS7OuEUmGkrnu5Q8SmBP10njnEiX7vJiaxJURZXcCORyw%40mail.gmail.com
renderValue (LitBoolean True)  = "TRUE::boolean"
renderValue (LitBoolean False) = "FALSE::boolean"
renderValue (LitTimestampTz i) =
    "timestamp with time zone '" <>
    fromString (formatTime defaultTimeLocale "%0Y-%m-%d %H:%M:%S%06Q" i) <>
    "+00'"
renderValue (LitNumF p s n) =
    fromString (formatScientific Fixed (Just s) n) <>
    "::numeric(" <> fromString (show p) <> ", " <> fromString (show s) <> ")"
renderValue (LitEmptyArray t)  = "'{}'::" <> renderT t <> "[]"
renderValue (InfixOp l o r)    = parens $
    renderValue l <> " " <> o <> " " <> renderValue r
renderValue (PrefixOp o e) = parens $ o <> " " <> renderValue e
renderValue (SuffixOp e o) = parens $ renderValue e <> " " <> o
renderValue (FunOp o es) = o <> parens (commaMap renderValue es)
renderValue (AggOp o d es p os) =
    -- https://www.postgresql.org/docs/10/sql-expressions.html#SYNTAX-AGGREGATES
    o <> parens (db <> commaMap renderValue es <> renderOrders os) <> pb
  where
    db = case d of
        Distinct   -> "distinct "
        Indistinct -> mempty
    pb = case p of
        Nothing -> mempty
        Just pe -> " filter (where " <> renderValue pe <> ")"
renderValue (Cast t e) = "cast(" <> renderValue e <> " as " <> renderT t <> ")"
renderValue (QuantOp o e s) = parens $
    renderValue e <> " " <> o <> " " <> parens (case renderSelect s of RenderedSelect b -> b)

renderT :: T -> ByteString
-- SQL only specifies the integer types integer (or int), smallint, and bigint.
-- The type names int2, int4, and int8 are extensions, which are also used by some other SQL
-- database systems.
renderT Int2               = "smallint"
renderT Int4               = "integer"
renderT Int8               = "bigint"
renderT Boolean            = "boolean"
-- The notations varchar(n) and char(n) are aliases for character varying(n) and character(n),
-- respectively. If specified, the length must be greater than zero and cannot exceed 10485760.
-- If character varying is used without length specifier, the type accepts strings of any size.
-- The latter is a PostgreSQL extension.
-- In addition, PostgreSQL provides the text type, which stores strings of any length. Although
-- the type text is not in the SQL standard, several other SQL database management systems have
-- it as well.
renderT Varchar            = "text"
renderT Bytea              = "bytea"
renderT TimestampTz        = "timestamp with time zone"
renderT (NumericFixed p s) = "numeric(" <> fromString (show p) <> ", " <> fromString (show s) <> ")"
renderT (ArrayOf t)        = renderT t <> "[]"

renderOrders :: [Order] -> ByteString
renderOrders [] = ""
renderOrders os = " order by " <> commaMap renderOrder os

renderOrder :: Order -> ByteString
renderOrder (Asc v)  = renderValue v
renderOrder (Desc v) = renderValue v <> " desc"

commaSep :: [ByteString] -> ByteString
commaSep = BS.intercalate ", "

commaMap :: Foldable f => (a -> ByteString) -> f a -> ByteString
commaMap f = commaSep . foldr ((:) . f) []

parens :: ByteString -> ByteString
parens x = "(" <> x <> ")"

identifier :: Text -> ByteString
identifier x = "\"" <> encodeUtf8 x <> "\""

object :: Maybe Text -> Text -> ObjectName
object q n = ObjectName . maybe id (\qn nb -> identifier qn <> "." <> nb) q $ identifier n
