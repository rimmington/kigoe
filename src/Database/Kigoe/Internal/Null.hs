{-# language OverloadedStrings, TypeFamilies, UndecidableInstances #-}
{-# options_ghc -Wno-redundant-constraints #-}

module Database.Kigoe.Internal.Null where

import Data.Containers.ListUtils (nubOrdOn)
import Data.SOP (All, K (..))
import Data.SOP.NP (NP (..), cmap_NP, collapse_NP)
import Data.Type.Equality (type (~))
import Database.Kigoe.Internal.AST
import Database.Kigoe.Internal.Ctx (Context (Across))
import Database.Kigoe.Internal.Set
import Database.Kigoe.Internal.Typed
import GHC.Exts (proxy#)
import GHC.OverloadedLabels (IsLabel (..))
import RIO

-- $setup
-- >>> :set -Wno-simplifiable-class-constraints
-- >>> import RIO
-- >>> import Database.Kigoe
-- >>> import Database.Kigoe.Internal.Set (Elem)
-- >>> someTable n as = table (uncheckedTableSchema Nothing n as) n

-- | Add 'Maybe' to a type if it isn't already there.
--
-- /Examples:/
--
-- >>> :k! Nulled ()
-- Nulled () :: *
-- = Maybe ()
--
-- >>> :k! Nulled (Maybe ())
-- Nulled (Maybe ()) :: *
-- = Maybe ()
type family Nulled a where
    Nulled (Maybe a) = Maybe a
    Nulled a         = Maybe a

-- | Use a nullable expression with a non-nullable expression, producing a final nullable
-- expression.
--
-- /Example:/
--
-- >>> e = forNull (lit (Just 5 :: Maybe Int32)) (≥ 2) :: Expr ctx (Maybe Bool)
forNull ::
    Expr ctx (Maybe a)
 -> (Expr ctx a -> Expr ctx b)
 -> Expr ctx (Nulled b)
forNull (Expr e) f = case f (Expr e) of
    Expr e' -> Expr e'

-- | A non-nullable expression can be treated as if it were nullable.
liftNull :: (SqlType (Maybe a)) => Expr ctx a -> Expr ctx (Maybe a)
liftNull (Expr e) = Expr e

-- | Return the first expression that is not null.
coalesce ::
    forall a ctx.
    (SqlType (Maybe a))
 => NonEmpty (Expr ctx (Maybe a))
 -> Expr ctx a
 -> Expr ctx a
coalesce es (Expr e) = Expr $ FunOp "coalesce" (haskT (proxy# @(Maybe a))) $
    toList ((\(Expr x) -> x) <$> es) ++ [e]

-- | Keep only those rel-tuples that satisfy a predicate that could produce null.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #people (Proxy @'["age" :@ Int32, "height" :@ Maybe Int32])
--   & selectNullable (forNull #height (≥ 180))
--   :: Rel ctx '["age" :@ Int32, "height" :@ Maybe Int32]
-- :}
selectNullable :: Expr (Across as ctx) (Maybe Bool) -> Rel ctx as -> Rel ctx as
selectNullable (Expr e) (Rel w) = Rel $ Select e w

-- | A reference to an existing nullable attribute in @ctx@.
--
-- /Example:/
--
-- >>> a = #frob :: Elem ctx "frob" i (Maybe t) => NullableAttrRef ctx ("frob" :@ t)
data NullableAttrRef (ctx :: [Attr]) (a :: Attr) = NullableAttrRef

instance (Elem ctx k i (Maybe t), a ~ (k :@ t)) => IsLabel k (NullableAttrRef ctx a) where
    fromLabel = NullableAttrRef

-- | As a convenience, a single label can be provided to 'selectNotNull'.
instance (Elem ctx k i (Maybe t), as ~ '[k :@ t]) => IsLabel k (NP (NullableAttrRef ctx) as) where
    fromLabel = NullableAttrRef :* Nil

-- | Keep only those rel-tuples where the specified attributes are not null, and reflect this
-- in the heading.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #people (Proxy @'["age" :@ Int32, "height" :@ Maybe Int32])
--   & selectNotNull #height
--   :: Rel ctx '["height" :@ Int32, "age" :@ Int32]
-- :}
selectNotNull ::
    (All KnownAttr bs)
 => NP (NullableAttrRef as) bs
 -> Rel ctx as
 -> Rel ctx (UnionAttrs bs as)
selectNotNull bs (Rel w) = Rel case rsl of
    []   -> w
    r:rs -> Project asl $ Select (foldl' (BinOp And Boolean) r rs) w
  where
    bsl = collapse_NP $ cmap_NP (Proxy @KnownAttr) mkA bs
    rsl = UnaryOp IsNotNull Boolean . AttrE <$> bsl
    asl = nubOrdOn aName $ bsl <> wa w
    mkA :: forall a ctx. KnownAttr a => NullableAttrRef ctx a -> K A a
    mkA _ = K $ attribute (Proxy @a)

-- | Maximum value of an expression over all tuples in the group where an
-- additional predicate holds.
maxSelective ::
    (SqlOrd t)
 => Expr ctx Bool
 -> Expr ctx t
 -> Summary ctx (Nulled t)
maxSelective (Expr p) x@(Expr e) = ord x . Summary $ Max e (Just p)
