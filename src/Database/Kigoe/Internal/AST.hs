{-# language GADTs, OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}
{-# HLINT ignore "Use if" #-}

module Database.Kigoe.Internal.AST
  ( A (..)
  , T (..)
  , W (..), WithOrd (..), Name (..)
  , L (..), Dir (..), LProj (..)
  , C (..), U (..)
  , E (..), et
  , Lit (..), BinOp (..), UnaryOp (..)
  , Agg (..)
  , Q (..)
  , Offsets
  , ptsOffsets, renderC, renderU, renderL, renderE, la, wa, withDepth
  ) where

import Data.Containers.ListUtils (nubOrdOn)
import Data.Foldable (find)
import Data.IntMap.Strict qualified as IM
import Data.IntSet qualified as IS
import Data.List.NonEmpty qualified as NE
import Data.Scientific (Scientific)
import Data.Vector.Unboxed qualified as U
import Database.Kigoe.Internal.PG qualified as P
import Database.Kigoe.Internal.SQL (Distinct (..), T (..))
import Database.Kigoe.Internal.SQL qualified as S
import RIO hiding (Int8)
import RIO.HashMap qualified as HM
import RIO.NonEmpty (head, nonEmpty, (<|))
import RIO.State (State, gets, modify, runState, state)
import RIO.Time (UTCTime)
import RIO.Vector (snoc)

data A
  = A
    { aName :: Text
    , aType :: T
    }
  deriving (Eq, Ord, Show)

data W
  = Project [A] W
  | Rename Text A W
  -- ^ Rename from to
  | Extend W (NonEmpty (Text, E))
  | Reexpress W [(Text, E)]
  | Join W W
  | Select E W
  | Values [A] [[E]]
  | ParamValues [A] Int
  | Table [A] S.ObjectName
  | Unnest WithOrd A E
  | Summarise [A] [(Text, Agg)] W
  | CAbs Name W W
  | COcc [A] Name
  deriving (Show)

newtype Name
  = Name Int
  deriving stock (Show)

data L
  = Unordered W
  | Ordered (NonEmpty (E, Dir)) LProj (Maybe Word32) W
  deriving (Show)

data C
  = Insert S.ObjectName L
  deriving (Show)

data U
  = Update S.ObjectName W [A] E (NonEmpty (Text, E))

data Dir = Asc | Desc
  deriving (Show)

data WithOrd
  = WithOrdinality Text
  | WithoutOrdinality
  deriving (Show)

data LProj
  = All
  | LProj [A] -- ^ for testing
  deriving (Show)

data E where
  Param :: T -> Int -> E
  AttrE :: A -> E
  OuterE :: A -> E
  Lit :: Lit -> E
  BinOp :: BinOp -> T -> E -> E -> E
  UnaryOp :: UnaryOp -> T -> E -> E
  FunOp :: Text -> T -> [E] -> E
  QuantOp :: BinOp -> Q -> E -> W -> E

deriving instance (Show E)

data Agg
  = CountTuples (Maybe E)
  | SumInt32 E
  | BoolAnd E
  | Max E (Maybe E)
  | ArrayAgg E Distinct (Maybe E) [(E, Dir)]
  deriving (Show)

data Q = Some
  deriving (Show)

-- | A Haskell value corresponding to some PostgreSQL value.
data Lit
  = LitInt2 Int16
  | LitInt4 Int32
  | LitInt8 Int64
  | LitBoolean Bool
  | LitTimestampTz UTCTime
  | LitNumF Int Int Scientific
  | LitAppendParam T P.Param
  | LitNull T
  deriving (Show)

data BinOp = And | Or | GEq | Eq | NEq | Plus | Subtract | Mul | Rem
  deriving (Eq, Show)

data UnaryOp = Not | Abs | Signum | IsNotNull
  deriving (Eq, Show)

et :: E -> T
et = \case
    Param t _ -> t
    AttrE a -> aType a
    OuterE a -> aType a
    Lit l -> case l of
        LitInt2 _          -> Int2
        LitInt4 _          -> Int4
        LitInt8 _          -> Int8
        LitBoolean _       -> Boolean
        LitTimestampTz _   -> TimestampTz
        LitNumF p s _      -> NumericFixed p s
        LitNull t          -> t
        LitAppendParam t _ -> t
    BinOp _ t _ _ -> t
    FunOp _ t _   -> t
    UnaryOp _ t _ -> t
    QuantOp{} -> Boolean

wa :: W -> [A]
wa = \case
    Project as _     -> as
    Rename f t w     -> t : filter (/= t{aName = f}) (wa w)
    Extend w es      -> nubOrdOn aName $ esa es <> wa w
    Reexpress _ es   -> esa es
    Join w0 w1       -> nubOrd $ wa w0 <> wa w1
    Select _ w       -> wa w
    Values as _      -> as
    ParamValues as _ -> as
    Table  as _      -> as
    Unnest wOrd a _  -> case wOrd of
      WithOrdinality no -> [a, A no Int8]
      WithoutOrdinality -> [a]
    Summarise by a _ -> by <> (aggA <$> a)
    CAbs _ _ w       -> wa w
    COcc as _        -> as
  where
    esa :: (Foldable f) => f (Text, E) -> [A]
    esa = fmap (uncurry A . fmap et) . toList
    aggA (n, agg) = case agg of
        CountTuples _    -> A n Int8
        SumInt32 _       -> A n Int8
        BoolAnd  _       -> A n Boolean
        Max      e _     -> A n $ et e
        ArrayAgg e _ _ _ -> A n . ArrayOf $ et e

la :: L -> [A]
la = \case
    Unordered     w          -> wa w
    Ordered _ All _ w        -> wa w
    Ordered _ (LProj as) _ _ -> as

withDepth :: W -> Int
withDepth (CAbs (Name i) _ _) = i + 1
withDepth COcc{}              = 0
withDepth Values{}            = 0
withDepth ParamValues{}       = 0
withDepth Table{}             = 0
withDepth Unnest{}            = 0
withDepth Summarise{}         = 0
withDepth (Project _ w)       = withDepth w
withDepth (Rename _ _ w)      = withDepth w
withDepth (Extend w _)        = withDepth w
withDepth (Reexpress w _)     = withDepth w
withDepth (Select _ w)        = withDepth w
withDepth (Join w0 w1)        = max (withDepth w0) (withDepth w1)

type Offsets = U.Vector Int

-- | Width of parameters -> offsets
ptsOffsets :: [Int] -> Offsets
ptsOffsets = U.unfoldr offset . (,1)
  where
    offset :: ([Int], Int) -> Maybe (Int, ([Int], Int))
    offset ([],  _) = Nothing
    offset (h:t, i) = Just (i, (t, i + h))

data St
  = St
    { aps     :: Vector P.Param
    , commons :: [S.Select]
    , uniqS   :: !Int
    , usedS   :: IntSet
      -- ^ uniq/aliases actually used
    }
data Rd
  = Rd
    { commonVars :: IntMap Int
    , beneathS   :: (S.Alias, [Text])
    , outerCtx   :: HashMap Text Int
    , offsets    :: U.Vector Int
    }
type M = ReaderT Rd (State St)

runM :: U.Vector Int -> M a -> (a, St)
runM o a = a `runReaderT` emptyRd `runState` emptySt
  where
    emptySt = St mempty mempty 0 mempty
    emptyRd = Rd mempty (0, []) mempty o

-- TODO: check common works
renderC :: Offsets -> C -> (S.Insert, Vector P.Param)
renderC o (Insert tgt src@(renderL o -> (s, ap))) = (S.Insert tgt cols s, ap)
  where
    cols = maybe emptyCol (fmap aName) . nonEmpty $ la src

renderU :: Offsets -> U -> (S.Merge, Vector P.Param)
renderU o (Update tbl w on e sets) = (S.Merge{ctes, tbl, src, cond, asgn}, aps st)
  where
    ctes = uncommon (commons st)
    ((src, cond, asgn), st) = runM o do
        src <- select w
        ev <- renderValue e
        let cond = foldr also ev on
            also a =
                S.InfixOp
                   (S.InfixOp
                        (S.ColRef . S.object (Just S.mergeSourceName) $ aName a)
                        "="
                        (S.ColRef . S.object (Just S.mergeTargetName) $ aName a))
                    "and"
        asgn <- traverse (traverse renderValue) sets
        pure (src, cond, asgn)

renderL :: Offsets -> L -> (S.Select, Vector P.Param)
renderL o l = (outer, aps st)
  where
    (inner, st) = runM o (list l)
    outer = case inner of
        S.Select{..} -> S.Select{ctes = uncommon (commons st), ..}

renderE :: Offsets -> E -> (S.Value, Vector P.Param)
renderE o = fmap aps . runM o . renderValue

uncommon :: [S.Select] -> [S.CTE]
uncommon = fmap cte . zip [0..] . reverse
  where
    cte (i, c) = S.CTE (commonText i) c

list :: L -> M S.Select
list (Unordered      w) = select w
list (Ordered nt p f w) = do
    i <- uniq
    orders <- beneath i (aName <$> wa w) $ traverse order (toList nt)
    frm <- S.Subselect <$> select w <*> pure i
    pure $ S.Select mempty (selecting (selCols ns)) (Just frm) [] [] orders f
  where
    ns = case p of
        All      -> wa w
        LProj as -> as

-- | Consider if 'beneath' is required before using this.
order :: (E, Dir) -> M S.Order
order (e, d) = case d of
    Asc  -> S.Asc <$> renderValue e
    Desc -> S.Desc <$> renderValue e

select :: W -> M S.Select
select v@(Values as _) = do
    i <- uniq
    fromPart i v <&> \case
        -- It's fine to drop the alias, because the subselects produced by fromPart for Values
        -- are trivial.
        S.Subselect s _ -> s
        part            -> selPart [] as part
select (Extend w es) = do
    i <- uniq
    let untouched = filter (`notElem` touched) wts
        wts = aName <$> wa w
        touched = fst <$> es
    evs <- beneath i wts $ traverse (traverse renderValue) es
    frm <- fromPart i w
    pure $ S.Select
        mempty
        ((uncurry S.SelValue <$> evs) `append` (S.SelColumn Nothing <$> untouched))
        (Just frm)
        [] [] [] Nothing
select (Reexpress w es) = do
    i <- uniq
    evs <- beneath i (aName <$> wa w) $ traverse (traverse renderValue) es
    frm <- fromPart i w
    pure $ S.Select
        mempty
        (selecting $ uncurry S.SelValue <$> evs)
        (Just frm)
        [] [] [] Nothing
select (Select he hw) = do
    i <- uniq
    vs <- beneath i (aName <$> botAs) $ traverse renderValue (conj =<< es)
    selPart vs botAs <$> fromPart i bot
  where
    strip (Select e w) acc = strip w $ e : acc
    strip w            acc = (acc, w)
    conj (BinOp And _ l r) = conj l <> conj r
    conj e                 = [e]
    (es, bot) = strip hw [he]
    botAs = wa bot
select (Rename hf ht hw) = do
    -- Stripping is fine because a plain rename can't possibly introduce some(..)
    i <- uniq
    frm <- fromPart i bot
    pure $ S.Select mempty cols (Just frm) [] [] [] Nothing
  where
    strip (Rename f t w) acc = case break ((== aName t) . fst) (toList acc) of
        (_, [])     -> strip w ((f, t) <| acc)
        (p, r : rs) -> strip w (prepend p $ (f, snd r) :| rs)
    strip w              acc = (acc, w)
    (mvs, bot) = strip hw (pure (hf, ht))
    cols =
        (mv <$> NE.reverse mvs) `append`
        (S.SelColumn Nothing . aName <$> orig)
    mv (f, t) = S.SelColumn (Just $ aName t) f
    orig = filter (\a -> isNothing $ find ((== aName a) . fst) mvs) (wa bot)
select (Summarise by es w) = do
    i <- uniq
    agg <- beneath i (aName <$> wa w) $ traverse (\(n, agg) -> S.SelValue n <$> renderAgg agg) es
    frm <- fromPart i w
    pure $ S.Select
        mempty
        (selecting $ selCols by <> agg)
        (Just frm) [] by' [] Nothing
  where
    by' = case by of
        -- Group by a constant to force Postgres to return zero groups given zero tuples
        -- https://github.com/tomjaguarpaw/haskell-opaleye/issues/159
        [] -> [S.LitBoolean True]
        as -> S.ColRef . S.object Nothing . aName <$> as
select (CAbs n c w)    = withCommon n c $ select w
select (Project as w)  = selPart [] as     <$> fromPartNewI w
select j@Join{}        = selPart [] (wa j) <$> fromPartNewI j
select t@Table{}       = selPart [] (wa t) <$> fromPartNewI t
select t@COcc{}        = selPart [] (wa t) <$> fromPartNewI t
select u@Unnest{}      = selPart [] (wa u) <$> fromPartNewI u
select v@ParamValues{} = selPart [] (wa v) <$> fromPartNewI v

fromPartNewI :: W -> M S.FromPart
fromPartNewI w = do
    i <- uniq
    fromPart i w

selecting :: [S.Selecting] -> NonEmpty S.Selecting
selecting (nonEmpty -> Just ss) = ss
selecting _                     = pure $ S.SelValue (head emptyCol) emptyVal

selCols :: [A] -> [S.Selecting]
selCols = fmap $ S.SelColumn Nothing . aName

selPart :: [S.Value] -> [A] -> S.FromPart -> S.Select
selPart wher as from = S.Select mempty (selecting (selCols as)) (Just from) wher [] [] Nothing

-- synthempty
emptyCol :: NonEmpty Text
emptyCol = pure "§γη†ħempty"

-- | The "empty value" needs to join to itself.
emptyVal :: S.Value
emptyVal = S.LitBoolean True

fromPart :: S.Alias -> W -> M S.FromPart
fromPart alias (Values asm vsm) = case (nonEmpty asm, nonEmpty vsm) of
    (Just as, Just vs) -> S.ValuesAs (aName <$> as) <$> traverse renderVs vs <*> pure alias
    (Nothing, Just vs) -> pure $ S.ValuesAs emptyCol (pure emptyVal <$ vs) alias
    (Just as, Nothing) -> pure $ S.Subselect
        (S.Select
            mempty
            (nullA <$> as)
            Nothing
            [S.LitBoolean False]
            [] [] Nothing)
        alias
    (Nothing, Nothing) -> pure $ S.Subselect
        (S.Select
            mempty
            (pure $ S.SelValue (head emptyCol) emptyVal)
            Nothing
            [S.LitBoolean False]
            [] [] Nothing)
        alias
  where
    renderVs asl = case nonEmpty asl of
        -- TODO: is beneath required here? can a values expr contain some(..)?
        Just as -> traverse renderValue as
        Nothing -> error "empty values list with non-empty heading"
    nullA A{aName, aType} = S.SelValue aName (S.NullOf aType)
fromPart alias (ParamValues asm ui) = do
    ih <- offsetParamI ui
    let it = S.Param <$> drop 1 (take (length asm) [ih..])
    pure case nonEmpty asm of
        Just as -> S.UnnestAs S.WithoutOrdinality (aName <$> as) (S.Param ih :| it) alias
        Nothing -> S.UnnestAs S.WithoutOrdinality emptyCol (S.Param ih :| []) alias
fromPart alias (Table _ n) = S.Table n <$> aliasM alias
fromPart alias j@(Join hw0 hw1) = do
    (bot0 :| botls) <- strip hw0
    botrs           <- strip hw1
    let joined = S.Join bot0 (prepend botls botrs)
    aliasM alias <&> \case
        Nothing -> joined
        Just _  -> S.Subselect (selPart [] (wa j) joined) alias
  where
    strip :: W -> M (NonEmpty S.FromPart)
    strip (Join w0 w1) = (<>) <$> strip w0 <*> strip w1
    strip w            = do
        i <- uniq
        frm <- fromPart i w
        pure (pure frm)
fromPart alias (Unnest wOrd a e) =
    -- It doesn't seem possible for the unnested expression to mention a since
    -- that would be infinitely recursive.
    -- TODO: double-check this
    S.UnnestAs sOrd (aName a :| []) . pure <$> renderValue e <*> pure alias
  where
    sOrd = case wOrd of
        WithoutOrdinality -> S.WithoutOrdinality
        WithOrdinality no -> S.WithOrdinality no
fromPart alias (COcc _ (Name n)) = asks (IM.lookup n . commonVars) >>= \case
    Nothing -> error "var lookup failure"
    Just i  -> S.Table (S.object Nothing (commonText i)) <$> aliasM alias
fromPart alias (CAbs n c w)  = withCommon n c $ fromPart alias w
fromPart alias w@Extend{}    = S.Subselect <$> select w <*> pure alias
fromPart alias w@Reexpress{} = S.Subselect <$> select w <*> pure alias
fromPart alias w@Project{}   = S.Subselect <$> select w <*> pure alias
fromPart alias w@Rename{}    = S.Subselect <$> select w <*> pure alias
fromPart alias w@Select{}    = S.Subselect <$> select w <*> pure alias
fromPart alias w@Summarise{} = S.Subselect <$> select w <*> pure alias

withCommon :: Name -> W -> M a -> M a
withCommon (Name n) c ma = do
    com <- select c
    i <- state \s@St{commons} ->
        (length commons, s{commons = com : commons})
    rd@Rd{commonVars} <- ask
    lift $ runReaderT ma rd{commonVars = IM.insert n i commonVars}

uniq :: M Int
uniq = state \st@St{uniqS} -> (uniqS, st{uniqS = uniqS + 1})

aliasM :: Int -> M (Maybe S.Alias)
aliasM i = do
    us <- gets usedS
    pure case i `IS.member` us of
        True  -> Just i
        False -> Nothing

commonText :: Int -> Text
commonText i = "§γη†ħcom" <> fromString (show i)

append :: NonEmpty a -> [a] -> NonEmpty a
append (a :| as) as' = a :| as <> as'

prepend :: [a] -> NonEmpty a -> NonEmpty a
prepend [] as                = as
prepend (a : as) (a1 :| as1) = a :| (as <> (a1 : as1))

beneath :: Int -> [Text] -> M a -> M a
beneath ui ts = local \s -> s{beneathS = (ui, ts)}

-- | Consider if 'beneath' is required before using this.
renderValue :: E -> M S.Value
renderValue (Param _ i) = S.Param <$> offsetParamI i
renderValue (AttrE a) = pure . S.ColRef . S.object Nothing $ aName a
renderValue (Lit l) = case l of
    LitInt2 i          -> pure $ S.LitInt2 i
    LitInt4 i          -> pure $ S.LitInt4 i
    LitInt8 i          -> pure $ S.LitInt8 i
    LitBoolean i       -> pure $ S.LitBoolean i
    LitTimestampTz i   -> pure $ S.LitTimestampTz i
    LitNumF n p s      -> pure $ S.LitNumF n p s
    LitAppendParam _ p -> do
        lastOffset <- lastOffsetI
        state \s@St{aps} -> (S.Param (lastOffset + length aps + 1), s{aps = aps `snoc` p})
    LitNull t          -> pure $ S.NullOf t
renderValue (BinOp op _ l r) = S.InfixOp <$> renderValue l <*> pure bs <*> renderValue r
  where
    bs = renderBinOp op
renderValue (UnaryOp op t e) = case op of
    Not       -> S.PrefixOp "not" <$> renderValue e
    Abs       -> S.FunOp "abs" . pure <$> renderValue e
    -- sign returns a double, so cast it back
    Signum    -> S.Cast t . S.FunOp "sign" . pure <$> renderValue e
    IsNotNull -> (`S.SuffixOp` "is not null") <$> renderValue e
renderValue (FunOp op _ es) = S.FunOp (encodeUtf8 op) <$> traverse renderValue es
renderValue (QuantOp op Some e w) = do
    (ambI, ambTs) <- asks beneathS
    let added r@Rd{outerCtx} = r{outerCtx = foldl' (\m t -> HM.insert t ambI m) outerCtx ambTs}
    S.QuantOp (renderBinOp op <> " some")
        <$> renderValue e
        <*> local added (select w)
renderValue (OuterE a) = do
    c <- asks outerCtx
    case HM.lookup (aName a) c of
        Nothing -> error $ "OuterE lookup " <> show a
        Just i  -> do
            modify \s@St{usedS} -> s{usedS = IS.insert i usedS}
            pure . S.ColRef $ S.object (Just ("§γη†ħt" <> fromString (show i))) (aName a)

renderBinOp :: BinOp -> ByteString
renderBinOp = \case
    And      -> "and"
    Or       -> "or"
    GEq      -> ">="
    Plus     -> "+"
    Subtract -> "-"
    Mul      -> "*"
    Rem      -> "%"
    NEq      -> "!="
    Eq       -> "="

-- | Consider if 'beneath' is required before using this.
renderAgg :: Agg -> M S.Value
renderAgg(CountTuples p) =
    S.AggOp "count" S.Indistinct [S.LitBoolean True] <$> traverse renderValue p <*> pure []
renderAgg(SumInt32 e) = S.FunOp "sum" . pure <$> renderValue e
renderAgg(BoolAnd  e) = S.FunOp "bool_and" . pure <$> renderValue e
renderAgg(Max e p) = do
    ev <- renderValue e
    pv <- traverse renderValue p
    let aggOp v = S.AggOp "max" S.Indistinct [v] pv []
    pure case et e of
        -- Postgres doesn't define max at boolean for some reason
        -- It works on boolean[] anyway
        Boolean -> S.Cast Boolean . aggOp $ S.Cast Int4 ev
        _       -> aggOp ev
renderAgg(ArrayAgg e d p oes) = do
    ev <- renderValue e
    ovs <- traverse order oes
    case p of
        Nothing -> pure $ S.AggOp "array_agg" d [ev] Nothing ovs
        Just pe -> do
            pv <- renderValue pe
            pure $ S.FunOp "coalesce"
                [S.AggOp "array_agg" d [ev] (Just pv) ovs, S.LitEmptyArray (et e)]

offsetParamI :: Int -> M Int
offsetParamI i = asks ((U.! (i - 1)) . offsets)

lastOffsetI :: M Int
lastOffsetI = asks offsets <&> \case
    o | U.null o  -> 0
      | otherwise -> U.last o
