{-# language GADTs, TypeFamilies, UndecidableInstances #-}
{-# options_ghc -Wno-redundant-constraints #-}
{-# options_haddock hide #-}

module Database.Kigoe.Internal.Absurdity where

import Data.Kind (Constraint)
import Data.Type.Equality (type (~))
import Database.Kigoe.Internal.Array (Array)
import GHC.TypeLits (ErrorMessage (..), TypeError)
import RIO (Bool (..), Maybe, Proxy)

type family IsMaybe a :: Bool where
    IsMaybe (Maybe a) = True
    IsMaybe a         = False

type family NotMaybe a :: Constraint where
    NotMaybe (Maybe a) = TypeError
        (ShowType a :<>: Text " appears in a context where Maybe is forbidden")
    NotMaybe a         = ()

type family NotArray a :: Constraint where
    NotArray (Maybe a)      = NotArray a
    NotArray (Array dims a) = TypeError
        (ShowType a :<>: Text " appears in a context where Array is forbidden")
    NotArray a              = ()

absurdArray :: (NotArray a, a ~ Array dims b) => Proxy a -> c
absurdArray = absurdArray
