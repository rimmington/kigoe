{-# language DuplicateRecordFields, GADTs, OverloadedStrings, StrictData, UndecidableInstances #-}
{-# options_ghc -Wno-ambiguous-fields -Wno-redundant-constraints #-}

module Database.Kigoe.Internal.Migrate where

import Control.Arrow (Kleisli (Kleisli))
import Control.Category qualified as C
import Data.Coerce (coerce)
import Data.SOP (I (..), K (..))
import Data.SOP.NP (NP (..), collapse_NP, cpure_NP)
import Data.Type.Equality (type (~))
import Database.Kigoe
  (Array, D1, KnownAttr, Ø, arrayAggSelectiveOrderBy, asc, insert, join, lit, oneDArray, ordered,
  project, query, queryInto, readWrite, select, some, summarise, table, transact, unordered, values,
  (<~), (=..))
import Database.Kigoe.Fielded (Label)
import Database.Kigoe.InformationSchema
  (CardinalNumber, CharacterData (..), SqlIdentifier (..), YesOrNo, informationSchema, no, yes)
import Database.Kigoe.Internal.AST qualified as AST
import Database.Kigoe.Internal.Ctx (Context (..))
import Database.Kigoe.Internal.PG (CommandTag (..), Connection, UnexpectedResponse (..), oneshotCmd)
import Database.Kigoe.Internal.PG qualified as P
import Database.Kigoe.Internal.Schema (SchemaRef (..), Table, schemaObject)
import Database.Kigoe.Internal.Set (Attr (..), Elem, NewKey, Remove, UnionAttrs, UniqKeys)
import Database.Kigoe.Internal.SQL
  (Col (..), Nullable (..), RenderedSelect (..), T (..), renderAddCheckConstraint,
  renderAddUniqueConstraint, renderCreateTable, renderDropConstraint, renderDropTable)
import Database.Kigoe.Internal.Typed (Expr (..), SqlType, haskN, haskT, reflects)
import Generics.SOP qualified as SOP
import GHC.Exts (proxy#)
import GHC.OverloadedLabels (IsLabel (..))
import GHC.TypeLits (KnownSymbol, Symbol, symbolVal)
import RIO hiding (join, some)
import RIO.List (isPrefixOf, sort, sortOn)
import RIO.Set qualified as Set
import RIO.State (StateT (..), evalStateT)
import RIO.Text (pack)
import RIO.Time (UTCTime)
import RIO.Vector (fromList)

-- $setup
-- >>> :set -Wno-simplifiable-class-constraints -XUndecidableInstances -XOverloadedStrings -XPartialTypeSignatures
-- >>> import Control.Category qualified as C
-- >>> import Data.SOP
-- >>> import Database.Kigoe
-- >>> import Database.Kigoe.Internal.Array (Array, D1)
-- >>> import Database.Kigoe.Fielded
-- >>> import Database.Kigoe.Internal.Schema
-- >>> import Database.Kigoe.Internal.Set
-- >>> import GHC.TypeLits
-- >>> import RIO hiding (Int8, join, some)
-- >>> let someTable n as = table (uncheckedTableSchema Nothing n as) n

-- | Configuration for applying migrations. Create with 'using'.
data Config
  = Config
    { c      :: Connection
    , proj   :: Text
    , migSch :: Maybe Text
    , info   :: Text
    , tm     :: TransactionMode
    }
data Cfg
  = Cfg
    { c        :: Connection
    , already  :: Int64
    , tracking :: Maybe Tracking
    }
data TransactionMode = Single | PerMigration | Caller
  deriving (Eq)
data Tracking
  = Tracking
    { proj   :: Text
    , before :: Int64
    , migSch :: SchemaRef MigrationSchema
    , info   :: Text
    , tm     :: TransactionMode
    }
-- | A sequence of migrations.
data Sequence from to
  = Sequence [Text] (Migration from to)
-- | A single migration; an indivisible series of schema transformations.
newtype Migration from to
  = Migration (Kleisli (StateT Cfg IO) (SchemaRef from) (SchemaRef to))

instance Category Migration where
    id = Migration C.id
    Migration g . Migration f = Migration (g C.. f)

instance Category Sequence where
    id = Sequence [] C.id
    (Sequence hl hf) . (Sequence gl gf) = Sequence (gl <> hl) (hf C.. gf)

newtype ColDef a
  = ColDef (K Col a)

-- | Use this connection to apply migrations,
-- place the tracking table in the same schema,
-- and run each migration in a transaction.
using :: Connection -> Config
using c = Config{c, proj = "", migSch = Nothing, info = "", tm = PerMigration}

-- | Specify a project name to differentiate this migration from others in the same schema.
projectName :: Text -> Config -> Config
projectName proj c = c{proj}

-- | Place the tracking table in this schema instead.
trackingIn :: Text -> Config -> Config
trackingIn s c = c{migSch = Just s}

-- | Store additional information regarding this migration application.
noteInfo :: Text -> Config -> Config
noteInfo info c = c{info}

-- | Run each migration in a transaction.
perMigrationTransactions :: Config -> Config
perMigrationTransactions c = c{tm = PerMigration}

-- | Run all migrations together in a single transaction;
-- all apply successfully or all are rolled back.
inSingleTransaction :: Config -> Config
inSingleTransaction c = c{tm = Single}

-- | Don't open a transaction - I'll manage it myself.
noTransaction :: Config -> Config
noTransaction c = c{tm = Caller}

-- | Run all migrations. Does not use a transaction.
runSequence :: Connection -> SchemaRef from -> Sequence from to -> IO (SchemaRef to)
runSequence c s (Sequence _ m) = runMigration c s m

-- | All of the migration names, in application order.
migrationNames :: Sequence from to -> [Text]
migrationNames (Sequence ns _) = ns

-- | Run a migration. Does not use a transaction.
runMigration :: Connection -> SchemaRef from -> Migration from to -> IO (SchemaRef to)
runMigration c = runMig (Cfg c 0 Nothing)

runMig :: Cfg -> SchemaRef from -> Migration from to -> IO (SchemaRef to)
runMig c s (Migration (Kleisli f)) = evalStateT (f s) c

type MigrationTbl =
   '[ "project"    :@ Text
    , "number"     :@ Int64
    , "name"       :@ Text
    , "applied_at" :@ UTCTime
    , "info"       :@ Text
    ]

type MigrationConstraints = '["kigoe_migrations_project_number" :@ Unique '["project", "number"]]

type MigrationSchema = '[MigrationTblName :@ Table MigrationConstraints MigrationTbl]

type MigrationTblName = "kigoe_migrations"

newtype MigInfo
  = MigInfo { name :: Text }
  deriving stock (Generic)
  deriving anyclass (SOP.Generic, SOP.HasDatatypeInfo)

-- | Apply any outstanding migrations. Migration status is tracked via an additional table.
ensureSequence :: Config -> SchemaRef from -> Sequence from to -> IO (SchemaRef to)
ensureSequence config sr@(SchemaRef tgtSch) (Sequence haveNames m) = bigTr do
    sn <- currentSchema c . SchemaRef $ migSch <|> tgtSch
    sch <- tableColInfo sn tn c >>= \case
        Nothing -> runMigration c (uncheckedSchemaExists sn) $
            createTable (fromLabel @MigrationTblName) cols >>>
            addConstraint (fromLabel @MigrationTblName) uq
        Just ci -> do
            unless (ci == expected) $ throwIO TrackingTableInvalid
            foundCons <- tableConstraintInfo sn tn c
            unless (foundCons == expectConstraints cons) $ throwIO TrackingTableInvalid
            pure (SchemaRef (Just sn))
    let q = table sch (fromLabel @MigrationTblName)
            & join (values [#project <~ lit proj])
            & ordered (asc #number :| [])
    foundNames <- coerce @[MigInfo] <$> queryInto c q Nil
    unless (foundNames `isPrefixOf` haveNames) $ throwIO $ SequenceMismatch{..}
    let track = Tracking proj (fromIntegral (length foundNames)) sch info tm
    evalStateT (coerce m sr) (Cfg c 0 (Just track))
  where
    tn = symbolText $ Proxy @MigrationTblName
    expected = expectColumns cols
    cols :: NP ColDef MigrationTbl
    cols = text #project :* int8 #number :* text #name :* utcTime #applied_at :* text #info :* Nil
    cons :: NP (ConstraintDef MigrationTbl) MigrationConstraints
    cons = uq :* Nil
    uq = unique #kigoe_migrations_project_number (#project :* #number)
    bigTr f = case tm of
        Single       -> checkTransactionState c P.Idle *> transact c readWrite f
        PerMigration -> checkTransactionState c P.Idle *> f
        Caller       -> f
    Config{c, migSch, proj, info, tm} = config

checkTransactionState :: Connection -> P.TransactionState -> IO ()
checkTransactionState c t = do
    s <- P.currentTransactionState c
    unless (s == t) $ throwIO TransactionStateInvalid{found = s, expected = t}

-- | Assume the current schema is empty.
emptySchema :: SchemaRef '[]
emptySchema = SchemaRef Nothing

-- | Assume a schema with the given name exists.
uncheckedSchemaExists :: Text -> SchemaRef '[]
uncheckedSchemaExists = SchemaRef . Just

-- | /Example:/
--
-- >>> :{
-- m = migration "add rooms" $
--   createTable #rooms (text #number :* text #building :* Nil)
-- :}
migration :: Text -> Migration from to -> Sequence from to
migration n m = Sequence [n] . Migration $ Kleisli \sr -> StateT \cfg@Cfg{already, tracking, c} -> do
    let apply = coerce m sr cfg
    sr' <- case tracking of
        Nothing -> fst <$> apply
        Just (Tracking proj i ms nfo tm)
          | already < i -> pure (coerce sr)
          | otherwise ->
            case tm of
                PerMigration -> transact c readWrite
                _            -> id
            do
                (sr', _ :: Cfg) <- apply
                _ <- insert c ms (fromLabel @MigrationTblName)
                    (values
                        [  #project <~ lit proj
                        :* #number <~ lit already
                        :* #name <~ lit n
                        :* #applied_at <~ statementTimestamp
                        :* #info <~ lit nfo
                        ])
                    Nil
                pure sr'
    pure (sr', cfg{already = already + 1})

statementTimestamp :: Expr ctx UTCTime
statementTimestamp = Expr $ AST.FunOp "statement_timestamp" TimestampTz []

-- $doesnottypecheck
-- >>> :{
-- m = migration "add rooms" (createTable #rooms (text #number :* text #building :* Nil))
--  C.>>> migration "again" (createTable #rooms (text #number :* text #building :* Nil))
-- :}
-- <BLANKLINE>
-- <interactive>:137:28: error: [GHC-64725]
--     • The attribute "rooms" cannot be declared multiple times
--     • In the second argument of ‘migration’, namely
--         ‘(createTable #rooms (text #number :* text #building :* Nil))’
--       In the second argument of ‘(>>>)’, namely
--         ‘migration
--            "again"
--            (createTable #rooms (text #number :* text #building :* Nil))’
--       In the expression:
--         migration
--           "add rooms"
--           (createTable #rooms (text #number :* text #building :* Nil))
--           >>>
--             migration
--               "again"
--               (createTable #rooms (text #number :* text #building :* Nil))

-- | /Example:/
--
-- >>> :{
-- m = createTable #rooms (text #number :* text #building :* Nil)
--   :: Migration ('[] :: [Attr]) '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]]
-- :}
createTable ::
    forall from name attrs.
    (KnownSymbol name, NewKey name from)
 => Label name
 -> NP ColDef attrs
 -> Migration from (name :@ Table '[] attrs : from)
createTable name cs = action \sr Cfg{c} -> do
    let sql = renderCreateTable (schemaObject sr name) (collapse_NP $ coerce cs)
    oneshotCmd c sql [] >>= \case
        CreateTableTag -> pure $ coerce sr
        tag            -> throwIO . UnexpectedResponse $ show tag

class DropTable from name to | from name -> to
instance
    ( Elem from name i (Table cs columns)
    , to ~ Remove (name :@ Table cs columns) from)
    => DropTable from name to

-- | Remove a table.
--
-- /Example:/
--
-- >>> :{
-- m = dropTable #rooms
--   :: Migration '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]] ('[] :: [Attr])
-- :}
dropTable ::
    forall from name to.
    (KnownSymbol name, DropTable from name to)
 => Label name
 -> Migration from to
dropTable name = action \sr Cfg{c} -> do
    oneshotCmd c (renderDropTable (schemaObject sr name)) [] >>= \case
        DropTableTag -> pure $ coerce sr
        tag          -> throwIO . UnexpectedResponse $ show tag

-- | Definition of a constraint for use with 'addConstraint' or 'tableExists'.
newtype ConstraintDef (columns :: [Attr]) a
  = ConstraintDef (K TableConstraintExpr a)
-- Laziness annotation required because of https://gitlab.haskell.org/ghc/ghc/-/issues/24620
type data Constraint
  -- | See 'unique'.
  = Unique ~[Symbol]
  -- | See 'check'.
  | Check
data TableConstraintExpr
  = UniqueC Text (NonEmpty Text)
  | CheckC Text AST.E [Text]
class AddConstraint from (table :: Symbol) (name :: Symbol) (constraint :: Constraint) to columns | from table name constraint -> to columns
instance
    ( UniqKeys cs'
    , cs' ~ (name :@ constraint : cs)
    , Elem from table i (Table cs columns)
    , to ~ UnionAttrs '[table :@ Table cs' columns] from)
    => AddConstraint from table name constraint to columns
class DropConstraint from (table :: Symbol) (name :: Symbol) to | from table name -> to
instance
    ( Elem from table i (Table cs columns)
    , Elem cs name j constraint
    , cs' ~ Remove (name :@ constraint) cs
    , to ~ UnionAttrs '[table :@ Table cs' columns] from)
    => DropConstraint from table name to

-- | Add a new constraint to a table.
--
-- /Example:/
--
-- >>> :{
-- m = addConstraint #rooms (unique #location (#number :* #building :* Nil))
--   :: Migration
--        '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]]
--        '["rooms" :@ Table '["location" :@ Unique '["number", "building"]] '["number" :@ Text, "building" :@ Text]]
-- :}
addConstraint ::
    forall from table columns name constraint to.
    (AddConstraint from table name constraint to columns, KnownSymbol table)
 => Label table
 -> ConstraintDef columns (name :@ constraint)
 -> Migration from to
addConstraint tbl e = action \sr Cfg{c} ->
    let (sql, params) = case coerce e of
            UniqueC cn cs -> (renderAddUniqueConstraint cn (schemaObject sr tbl) cs, [])
            CheckC cn p _ -> (renderAddCheckConstraint cn (schemaObject sr tbl) pv, toList ps)
              where
                (pv, ps) = AST.renderE mempty p
    in oneshotCmd c sql params >>= \case
        AlterTableTag -> pure $ coerce sr
        tag           -> throwIO . UnexpectedResponse $ show tag

-- | Drop an existing constraint.
--
-- /Example:/
--
-- >>> :{
-- m = dropConstraint #rooms #location
--   :: Migration
--        '["rooms" :@ Table '["location" :@ Unique '["number", "building"]] '["number" :@ Text, "building" :@ Text]]
--        '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]]
-- :}
dropConstraint ::
    forall from table name to.
    (DropConstraint from table name to, KnownSymbol table, KnownSymbol name)
 => Label table
 -> Label name
 -> Migration from to
dropConstraint tbl name = action \sr Cfg{c} ->
    oneshotCmd c (renderDropConstraint (symbolText name) (schemaObject sr tbl)) [] >>= \case
        AlterTableTag -> pure $ coerce sr
        tag           -> throwIO . UnexpectedResponse $ show tag

-- $doesnottypecheck1
-- >>> :{
-- m = addConstraint #rooms (unique #location #numer)
--   :: Migration
--        '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]]
--        '["rooms" :@ Table '["location" :@ Unique '["number", "building"]] '["number" :@ Text, "building" :@ Text]]
-- :}
-- <BLANKLINE>
-- <interactive>:295:45: error: [GHC-64725]
--     • No attribute named "numer" exists here
--     • In the second argument of ‘unique’, namely ‘#numer’
--       In the second argument of ‘addConstraint’, namely
--         ‘(unique #location #numer)’
--       In the expression:
--           addConstraint #rooms (unique #location #numer) ::
--             Migration '["rooms"
--                         :@ Table '[] '["number" :@ Text, "building" :@ Text]] '["rooms"
--                                                                                 :@
--                                                                                 Table '["location"
--                                                                                         :@
--                                                                                         Unique '["number",
--                                                                                                  "building"]] '["number"
--                                                                                                                 :@
--                                                                                                                 Text,
--                                                                                                                 "building"
--                                                                                                                 :@
--                                                                                                                 Text]]

-- $doesnottypecheck2
-- >>> :{
-- m = addConstraint #ooms (unique #location (#number :* #building :* Nil))
--   :: Migration
--        '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]]
--        _
-- :}
-- <BLANKLINE>
-- <interactive>:328:6: error: [GHC-64725]
--     • No attribute named "ooms" exists here
--     • In the expression:
--           addConstraint
--             #ooms (unique #location (#number :* #building :* Nil)) ::
--             Migration '["rooms"
--                         :@ Table '[] '["number" :@ Text, "building" :@ Text]] _
--       In an equation for ‘m’:
--           m = addConstraint
--                 #ooms (unique #location (#number :* #building :* Nil)) ::
--                 Migration '["rooms"
--                             :@ Table '[] '["number" :@ Text, "building" :@ Text]] _

-- $doesnottypecheck3
-- >>> :{
-- m = dropConstraint #rooms #location
--   :: Migration
--        '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]]
--        _
-- :}
-- <BLANKLINE>
-- <interactive>:361:6: error: [GHC-64725]
--     • No attribute named "location" exists here
--     • In the expression:
--           dropConstraint #rooms #location ::
--             Migration '["rooms"
--                         :@ Table '[] '["number" :@ Text, "building" :@ Text]] _
--       In an equation for ‘m’:
--           m = dropConstraint #rooms #location ::
--                 Migration '["rooms"
--                             :@ Table '[] '["number" :@ Text, "building" :@ Text]] _

-- | A reference to an existing column named @n@ in @ctx@, for use in migrations.
--
-- /Example:/
--
-- >>> a = #frob :: Elem ctx "frob" i t => ColRef ctx "frob"
data ColRef ctx (n :: Symbol) = ColRef
  deriving (Show)

instance (Elem ctx k i t, a ~ k) => IsLabel k (ColRef ctx a) where
    fromLabel = ColRef

-- | As a convenience, a single label can be provided.
instance (Elem ctx k i t, as ~ '[k]) => IsLabel k (NP (ColRef ctx) as) where
    fromLabel = ColRef :* Nil

-- | Unique constraints ensure that the data contained in a column, or a group of columns, is
-- unique among all the rows in the table.
unique ::
    forall name col0 cols columns.
    (KnownSymbol col0, SOP.All KnownSymbol cols, KnownSymbol name)
 => Label name
 -> NP (ColRef columns) (col0 : cols)
 -> ConstraintDef columns (name :@ Unique (col0 : cols))
unique name _ = coerce $ UniqueC
    (symbolText name)
    (pack (symbolVal @col0 Proxy) :| collapse_NP (reflectCs @cols))

reflectCs :: forall as. (SOP.All KnownSymbol as) => NP (K Text) as
reflectCs = cpure_NP @KnownSymbol @as Proxy mkA
  where
    mkA :: forall a. KnownSymbol a => K Text a
    mkA = K . pack $ symbolVal @a Proxy

-- | Check constraints ensure that the data contained in a row satisfies a predicate.
--
-- The predicate expression cannot contain subqueries or refer to variables other than columns of
-- the current row. PostgreSQL also assumes that check constraints' conditions will always give the
-- same result for the same input row; see
-- [the PostgreSQL documentation](https://www.postgresql.org/docs/current/ddl-constraints.html#DDL-CONSTRAINTS-CHECK-CONSTRAINTS)
-- for further detail.
check ::
    forall name columns.
    (KnownSymbol name, SOP.All KnownAttr columns)
 => Label name
 -> Expr (Checking (Across columns Ø)) Bool
 -> ConstraintDef columns (name :@ Check)
check name (Expr e) = coerce $
    CheckC (symbolText name) e (AST.aName <$> collapse_NP (reflects @columns))

-- $checkdoesnottypecheck
-- >>> :{
-- m = addConstraint #rooms
--     (check #known_number (lit @Int64 1 =.. some (
--         values [#n <~ "1", #n <~ "2"]
--         & join (values [#n <~ outer #number])
--         & reexpress (#x <~ 1))))
--   :: Migration
--       '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]]
--       '["rooms" :@ Table '["known_number" :@ Check] '["number" :@ Text, "building" :@ Text]]
-- :}
-- <BLANKLINE>
-- <interactive>:423:45: error: [GHC-64725]
--     • Cannot use quantified expressions within a check constraint
--     • In the second argument of ‘(=..)’, namely
--         ‘some
--            (values [#n <~ "1", #n <~ "2"]
--               & join (values [#n <~ outer #number])
--               & reexpress (#x <~ 1))’
--       In the second argument of ‘check’, namely
--         ‘(lit @Int64 1
--             =..
--               some
--                 (values [#n <~ "1", #n <~ "2"]
--                    & join (values [#n <~ outer #number])
--                    & reexpress (#x <~ 1)))’
--       In the second argument of ‘addConstraint’, namely
--         ‘(check
--             #known_number
--             (lit @Int64 1
--                =..
--                  some
--                    (values [#n <~ "1", #n <~ "2"]
--                       & join (values [#n <~ outer #number])
--                       & reexpress (#x <~ 1))))’

-- $checkdoesnottypecheck1
-- >>> :{
-- m = addConstraint #rooms (check #known_building (#building =.. some (someTable #buildings (Proxy @'["building" :@ Text]))))
--   :: Migration
--       '["rooms" :@ Table '[] '["number" :@ Text, "building" :@ Text]]
--       '["rooms" :@ Table '["known_building" :@ Check] '["number" :@ Text, "building" :@ Text]]
-- :}
-- <BLANKLINE>
-- <interactive>:459:65: error: [GHC-64725]
--     • Cannot use quantified expressions within a check constraint
--     • In the second argument of ‘(=..)’, namely
--         ‘some (someTable #buildings (Proxy @'["building" :@ Text]))’
--       In the second argument of ‘check’, namely
--         ‘(#building
--             =.. some (someTable #buildings (Proxy @'["building" :@ Text])))’
--       In the second argument of ‘addConstraint’, namely
--         ‘(check
--             #known_building
--             (#building
--                =.. some (someTable #buildings (Proxy @'["building" :@ Text]))))’

-- | Incorporate a table that already exists.
--
-- Some details are not checked:
--
-- * The dimensionality of array types (PostgreSQL does not preserve this information).
-- * Check expressions; only the columns used in the expression are checked.
tableExists ::
    forall from name attrs constraints.
    (KnownSymbol name, NewKey name from)
 => Label name
 -> NP ColDef attrs
 -> NP (ConstraintDef attrs) constraints
 -> Migration from (name :@ Table constraints attrs : from)
tableExists name cs os = action \sr Cfg{c} -> do
    sn <- currentSchema c sr
    let expectedColumns = expectColumns cs
        expectedConstraints = expectConstraints os
        tableName = symbolText name
    foundColumns <- maybe (throwIO TableMissing{tableName}) pure =<< tableColInfo sn tableName c
    foundConstraints <- tableConstraintInfo sn tableName c
    bool (throwIO TableMismatch{..}) (pure (coerce sr)) $
        expectedColumns == foundColumns
        && expectedConstraints == foundConstraints

-- | Perform an IO action during a migration.
-- Since a migration may be retried upon serialisation failure, the action
-- should be idempotent.
ioAction :: (SchemaRef from -> IO (SchemaRef to)) -> Migration from to
ioAction a = action \sr Cfg{c, tracking} ->
    a sr <*
    for_ tracking \Tracking{tm} -> unless (tm == Caller) $ checkTransactionState c P.Block

-- | Perform a simple IO action during a migration.
-- Since a migration may be retried upon serialisation failure, the action
-- should be idempotent.
ioAction_ :: (SchemaRef schema -> IO a) -> Migration schema schema
ioAction_ a = ioAction (\sr -> sr <$ a sr)

expectColumns :: NP ColDef attrs -> [ColInfo]
expectColumns cs = sortOn column_name $ colInfo <$> collapse_NP (coerce cs)
  where
    colInfo (Col n t u) = ColInfo
        { column_name = SqlIdentifier n
        , data_type   = CharacterData case t of
            Int2             -> "smallint"
            Int4             -> "integer"
            Int8             -> "bigint"
            Boolean          -> "boolean"
            Varchar          -> "text"
            Bytea            -> "bytea"
            TimestampTz      -> "timestamp with time zone"
            NumericFixed _ _ -> "numeric"
            ArrayOf _        -> "ARRAY"
        , udt_schema  = SqlIdentifier "pg_catalog"
        , udt_name    = SqlIdentifier case t of
            ArrayOf et -> "_" <> udt (elemt et)
            _          -> udt t
        , is_nullable = case u of
            Nullable    -> yes
            NotNullable -> no
        , numeric_precision = case t of
            Int4             -> Just 32
            Int8             -> Just 64
            NumericFixed p _ -> Just (fromIntegral p)
            _                -> Nothing
        , numeric_scale = case t of
            Int4             -> Just 0
            Int8             -> Just 0
            NumericFixed _ s -> Just (fromIntegral s)
            _                -> Nothing
        }
      where
        udt = \case
            Int2             -> "int2"
            Int4             -> "int4"
            Int8             -> "int8"
            Boolean          -> "bool"
            Varchar          -> "text"
            Bytea            -> "bytea"
            TimestampTz      -> "timestamptz"
            NumericFixed _ _ -> "numeric"
            ArrayOf et       -> "_" <> udt (elemt et)
        elemt = \case
            ArrayOf et -> elemt et
            et         -> et

expectConstraints :: NP (ConstraintDef attrs) constraints -> [ConstraintInfo]
expectConstraints cs = sortOn constraint_name $ conInfo <$> collapse_NP (coerce cs)
  where
    conInfo (UniqueC n cols) = ConstraintInfo
        { constraint_name = SqlIdentifier n
        , constraint_type = CharacterData "UNIQUE"
        , column_names    = oneDArray $ fromList (sort (coerce (toList cols)))
        }
    conInfo (CheckC n e allCols) = ConstraintInfo
        { constraint_name = SqlIdentifier n
        , constraint_type = CharacterData "CHECK"
        , column_names    = oneDArray $ fromList (coerce (Set.toAscList (colsUsed e `Set.intersection` Set.fromList allCols)))
        }
    colsUsed = \case
        AST.Param{}       -> mempty
        AST.AttrE a       -> used a
        AST.OuterE{}      -> mempty
        AST.Lit{}         -> mempty
        AST.BinOp _ _ l r -> colsUsed l <> colsUsed r
        AST.UnaryOp _ _ e -> colsUsed e
        AST.FunOp _ _ es  -> Set.unions $ colsUsed <$> es
        AST.QuantOp{}     -> error "bug: QuantOp in expect"
    used = Set.singleton . AST.aName

action :: (SchemaRef from -> Cfg -> IO (SchemaRef to)) -> Migration from to
action f = Migration $ Kleisli \sr -> StateT \cfg -> (, cfg) <$> f sr cfg

-- | Define a column of another type.
col ::
    forall name ty.
    (KnownSymbol name, SqlType ty)
 => Label name
 -> Proxy ty
 -> ColDef (name :@ ty)
col name _ = ColDef . K $ Col (symbolText name) (haskT (proxy# @ty)) (haskN (proxy# @ty))

int2 :: forall name. (KnownSymbol name) => Label name -> ColDef (name :@ Int16)
int2 = cd Int2

int4 :: forall name. (KnownSymbol name) => Label name -> ColDef (name :@ Int32)
int4 = cd Int4

int8 :: forall name. (KnownSymbol name) => Label name -> ColDef (name :@ Int64)
int8 = cd Int8

boolean :: forall name. (KnownSymbol name) => Label name -> ColDef (name :@ Bool)
boolean = cd Boolean

text :: forall name. (KnownSymbol name) => Label name -> ColDef (name :@ Text)
text = cd Varchar

bytea :: forall name. (KnownSymbol name) => Label name -> ColDef (name :@ ByteString)
bytea = cd Bytea

-- TODO: should this be timestampTz?
utcTime :: forall name. (KnownSymbol name) => Label name -> ColDef (name :@ UTCTime)
utcTime = cd TimestampTz

cd :: forall name t. (KnownSymbol name) => T -> Label name -> ColDef (name :@ t)
cd t name = ColDef . K $ Col (symbolText name) t NotNullable

data ColInfo
  = ColInfo
    { column_name       :: SqlIdentifier
    , data_type         :: CharacterData
    , is_nullable       :: YesOrNo
    , udt_name          :: SqlIdentifier
    , udt_schema        :: SqlIdentifier
    , numeric_precision :: Maybe CardinalNumber
    , numeric_scale     :: Maybe CardinalNumber
    }
  deriving stock (Eq, Generic, Show)
  deriving anyclass (SOP.Generic, SOP.HasDatatypeInfo)

tableColInfo ::
    Text -- ^ Schema
 -> Text -- ^ Table name
 -> Connection
 -> IO (Maybe [ColInfo])
tableColInfo sn tn c = do
    foundTables <- flip (query c) Nil $
        table informationSchema #tables
        & join nameFilter
        & project #table_name
        & unordered
    bool
        (pure Nothing)
        (Just <$> queryInto c q Nil)
        (foundTables == [#table_name <~ I sqli])
  where
    sqli = SqlIdentifier tn
    q = table informationSchema #columns
        & join nameFilter
        & project
            (  #column_name
            :* #data_type
            :* #is_nullable
            :* #udt_schema
            :* #udt_name
            :* #numeric_precision
            :* #numeric_scale
            )
        & ordered (asc #column_name :| [])
    nameFilter = values
        [  #table_schema <~ lit (SqlIdentifier sn)
        :* #table_name   <~ lit (SqlIdentifier tn)
        ]

data ConstraintInfo
  = ConstraintInfo
    { constraint_name :: SqlIdentifier
    , column_names    :: Array D1 SqlIdentifier
    , constraint_type :: CharacterData
    }
  deriving stock (Eq, Generic, Show)
  deriving anyclass (SOP.Generic, SOP.HasDatatypeInfo)

tableConstraintInfo ::
    Text -- ^ Schema
 -> Text -- ^ Table name
 -> Connection
 -> IO [ConstraintInfo]
tableConstraintInfo sn tn c = queryInto c q Nil
  where
    q = table informationSchema #table_constraints
        & join (values
            [  #table_schema    <~ lit (SqlIdentifier sn)
            :* #table_name      <~ lit (SqlIdentifier tn)
            ])
        -- Don't want to pick up multi-table FK constraints
        & select (#constraint_type =.. some (values
            [ #x <~ lit (CharacterData "UNIQUE")
            , #x <~ lit (CharacterData "CHECK")
            ]))
        & join (table informationSchema #constraint_column_usage)
        & summarise
            (#constraint_name :* #constraint_type)
            (#column_names <~ arrayAggSelectiveOrderBy (lit True) (asc #column_name :| []) #column_name)
        & ordered (asc #constraint_name :| [])

currentSchema :: Connection -> SchemaRef s -> IO Text
currentSchema c (SchemaRef mn) = maybe fetch pure mn
  where
    fetch =
        P.oneshot c (RenderedSelect "select current_schema()") [] [P.oneOid (Proxy @(Maybe Text))] >>= \case
            [Just n] -> pure n
            _        -> throwIO NoCurrentSchema

symbolText :: (KnownSymbol t) => proxy t -> Text
symbolText = pack . symbolVal

-- TODO: MigrationError

data TableMismatch
  = TableMismatch
    { tableName           :: Text
    , expectedColumns     :: [ColInfo]
    , foundColumns        :: [ColInfo]
    , expectedConstraints :: [ConstraintInfo]
    , foundConstraints    :: [ConstraintInfo]
    }
  deriving (Exception, Show)

newtype TableMissing
  = TableMissing { tableName :: Text }
  deriving stock (Show)
  deriving anyclass (Exception)

data NoCurrentSchema = NoCurrentSchema
  deriving (Exception, Show)

data TrackingTableInvalid = TrackingTableInvalid
  deriving (Exception, Show)

data SequenceMismatch
  = SequenceMismatch
    { haveNames  :: [Text]
    , foundNames :: [Text]
    }
  deriving (Exception, Show)

data TransactionStateInvalid
  = TransactionStateInvalid
    { expected :: P.TransactionState
    , found    :: P.TransactionState
    }
  deriving (Exception, Show)

mismatchedTableName :: TableMismatch -> Text
mismatchedTableName TableMismatch{..} = tableName

missingTableName :: TableMissing -> Text
missingTableName TableMissing{..} = tableName
