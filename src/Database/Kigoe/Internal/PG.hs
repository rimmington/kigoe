{-# language GADTs, OverloadedStrings, UndecidableInstances #-}
{-# options_ghc -Wno-simplifiable-class-constraints -Wno-orphans #-}
{-# options_haddock hide #-}

module Database.Kigoe.Internal.PG (
    Config (..), defConfig
  , Param, Oid, param, uvecParam
  , P.Connection, P.withConnection
  , P.FromRecord (..), P.ColumnInfo, Parser, PgEncoding (oneOid, arrOid), column
  , oneshot
  , P.CommandTag (..), oneshotCmd
  , P.TransactionState (..), currentTransactionState
  , UnexpectedResponse (..)
  ) where

import BinaryParser qualified as BP
import Data.Attoparsec.ByteString (Parser)
import Data.Coerce (coerce)
import Data.Scientific (Scientific)
import Data.SOP (All, K (..), Top, cpara_SList)
import Data.SOP.NP (NP (..), collapse_NP, pure_NP)
import Data.Vector.Generic qualified as G
import Data.Vector.Unboxed qualified as U
import Database.Kigoe.Internal.Absurdity (NotArray, absurdArray)
import Database.Kigoe.Internal.Array
  (Array, DVector (..), Lengths (..), arrayDVector, dVectorArray, foldlD', foldlD1')
import Database.Kigoe.Internal.SQL (RenderedCmd (..), RenderedSelect (..))
import Database.Kigoe.Numeric (Numeric (..))
import Database.PostgreSQL.Pure.List (Config (..))
import Database.PostgreSQL.Pure.List qualified as P
import Database.PostgreSQL.Pure.Oid (Oid)
import Database.PostgreSQL.Pure.Oid qualified as Oid
import Database.PostgreSQL.Pure.Parser (column)
import PostgreSQL.Binary.Decoding qualified as BD
import PostgreSQL.Binary.Encoding qualified as BE
import RIO
import RIO.Text qualified as T
import RIO.Time (UTCTime)

-- | User @postgres@ on @127.0.0.1:5432@.
defConfig :: Config
defConfig = Config
    { address             = P.AddressNotResolved "127.0.0.1" "5432"
    , user                = "postgres"
    , password            = ""
    , database            = ""
    , sendingBufferSize   = 4096
    , receptionBufferSize = 4096
    }

prep ::
    P.PreparedStatementName
 -> ByteString
 -> [Oid] -- ^ types of parameters
 -> [Oid] -- ^ types of result
 -> P.PreparedStatementProcedure
prep psn sql ps rs = P.parse psn (P.Query sql) (Right (ps, rs))

bind ::
    (P.Bind prepared, MonadFail m)
 => P.Connection
 -> P.PortalName
 -> [Param]
 -> prepared
 -> m P.PortalProcedure
bind conn pn =
    P.bind pn P.BinaryFormat P.BinaryFormat (P.parameters conn) (const $ fail "string")

exec ::
    (P.FromRecord r)
 => P.Connection
 -> P.PortalProcedure
 -> Word -- ^ # records to get, 0 = unlimited
 -> IO (P.Executed r, P.TransactionState)
exec conn bound rs = do
    ((_, _, executed, _), trState) <- P.sync conn (P.execute rs (const $ fail "string") bound)
    pure (executed, trState)

currentTransactionState :: P.Connection -> IO P.TransactionState
currentTransactionState conn = do
    let prepped = prep "" "select 1::int4" [] [Oid.int4]
    bound <- bind conn "" [] prepped
    (_, st) <- exec @Int32 conn bound 0
    pure st

oneshot ::
    (P.FromRecord r)
 => P.Connection
 -> RenderedSelect
 -> [Param]
 -> [Oid] -- ^ types of result
 -> IO [r]
oneshot conn (RenderedSelect sql) ps rs = do
    let prepped = prep "" sql (oidOf <$> ps) rs
    bound <- bind conn "" ps prepped
    (executed, _) <- exec conn bound 0
    pure $ P.records executed

oneshotCmd :: P.Connection -> RenderedCmd -> [Param] -> IO P.CommandTag
oneshotCmd conn (RenderedCmd sql) ps = do
    let prepped = prep "" sql (oidOf <$> ps) []
    bound <- bind conn "" ps prepped
    (executed, _) <- exec @() conn bound 0
    case P.result executed of
        P.ExecuteComplete t -> pure t
        P.ExecuteEmptyQuery -> throwIO $ UnexpectedResponse "ExecuteEmptyQuery"
        P.ExecuteSuspended  -> throwIO $ UnexpectedResponse "ExecuteSuspended"

newtype UnexpectedResponse
  = UnexpectedResponse String
  deriving stock (Show)
  deriving anyclass (Exception)

oidOf :: Param -> Oid
oidOf (Param o _) = o

param :: forall a. (PgEncoding a) => a -> Param
param = Param (oneOid (Proxy @a))

uvecParam :: forall a dims. (PgEncoding a, NotArray a, G.Vector U.Vector a) => DVector dims U.Vector a -> Param
uvecParam = Param (arrOid (Proxy @a))

class (P.FromField a, P.ToField a) => PgEncoding a where
    encodeElement :: NotArray a => a -> BE.Array
    decodeElement :: NotArray a => Proxy a -> BD.Value (Maybe a)
    oneOid :: Proxy a -> Oid
    arrOid :: NotArray a => Proxy a -> Oid

instance PgEncoding Bool where
    encodeElement = BE.encodingArray . BE.bool
    decodeElement _ = onContent BD.bool
    oneOid _ = Oid.bool
    arrOid _ = Oid.boolArray
instance PgEncoding Int16 where
    encodeElement = BE.encodingArray . BE.int2_int16
    decodeElement _ = onContent BD.int
    oneOid _ = Oid.int2
    arrOid _ = Oid.int2Array
instance PgEncoding Int32 where
    encodeElement = BE.encodingArray . BE.int4_int32
    decodeElement _ = onContent BD.int
    oneOid _ = Oid.int4
    arrOid _ = Oid.int4Array
instance PgEncoding Int64 where
    encodeElement = BE.encodingArray . BE.int8_int64
    decodeElement _ = onContent BD.int
    oneOid _ = Oid.int8
    arrOid _ = Oid.int8Array
instance PgEncoding Text where
    encodeElement = BE.encodingArray . BE.text_strict
    decodeElement _ = coerce <$> onContent BD.text_strict
    oneOid _ = Oid.text
    arrOid _ = Oid.textArray
instance PgEncoding ByteString where
    encodeElement = BE.encodingArray . BE.bytea_strict
    decodeElement _ = onContent BD.bytea_strict
    oneOid _ = Oid.bytea
    arrOid _ = Oid.byteaArray
instance PgEncoding UTCTime where
    encodeElement = BE.encodingArray . BE.timestamptz_int
    decodeElement _ = onContent BD.timestamptz_int
    oneOid _ = Oid.timestamptz
    arrOid _ = Oid.timestamptzArray
instance PgEncoding Scientific where
    encodeElement = BE.encodingArray . BE.numeric
    decodeElement _ = onContent BD.numeric
    oneOid _ = Oid.numeric
    arrOid _ = Oid.numericArray
instance (PgEncoding a) => PgEncoding (Maybe a) where
    encodeElement = maybe BE.nullArray encodeElement
    decodeElement _ = Just <$> decodeElement (Proxy @a)
    oneOid _ = oneOid (Proxy @a)
    arrOid _ = arrOid (Proxy @a)
instance (PgEncoding a, All Top dims, NotArray a) => PgEncoding (Array dims a) where
    encodeElement = absurdArray (Proxy @(Array dims a))
    decodeElement = absurdArray (Proxy @(Array dims a))
    arrOid = absurdArray
    oneOid _ = arrOid (Proxy @a)

deriving newtype instance PgEncoding (Numeric precision scale)
deriving newtype instance P.ToField (Numeric precision scale)

-- https://hackage.haskell.org/package/postgresql-binary-0.12.4/docs/src/PostgreSQL.Binary.Decoding.html#valueArray
onContent :: BD.Value a -> BD.Value (Maybe a)
onContent decoder = BP.sized 4 (BD.int @Int32) >>= \case
    -1 -> pure Nothing
    n  -> Just <$> BP.sized (fromIntegral n) decoder

instance P.ToField Text where
    toField b c d e a = P.toField b c d e (encodeUtf8 a)

instance P.FromField Text where
    fromField a b c =
        either (fail . displayException) pure . decodeUtf8'
        =<< P.fromField a b c

instance P.FromField (Numeric precision scale) where
    fromField a b c = coerce @Scientific <$> P.fromField a b c

arrToField ::
    forall a v dims m.
    (MonadFail m, PgEncoding a, NotArray a)
 => (DVector dims v a -> BE.Array)
 -> P.BackendParameters
 -> P.StringEncoder
 -> Maybe Oid
 -> P.FormatCode
 -> DVector dims v a
 -> m (Maybe ByteString)
arrToField _ _ _ Nothing P.TextFormat =
    const $ fail $ "no text format (ToField): " <> show (arrOid (Proxy @a))
arrToField f _ _ Nothing P.BinaryFormat =
    pure . Just . BE.encodingBytes . BE.array oidWord32 . f
  where
    oidWord32 = case oneOid (Proxy @a) of
        Oid.Oid i -> fromIntegral i
arrToField f backendParams strEncoder (Just o) fc
  | o == ao   = arrToField f backendParams strEncoder Nothing fc
  | otherwise = const $ fail $
    "type mismatch (ToField): OID: " <> show o <> ", expected: " <> show ao
  where
    ao = arrOid (Proxy @a)

instance (PgEncoding a, G.Vector v a, NotArray a)
    => P.ToField (DVector dims v a) where

    toField = arrToField \arr@DVector{lengths} -> dimension lengths arr
      where
        dimension :: forall ds d. Lengths (d : ds) -> DVector ds v a -> BE.Array
        dimension = \case
            Lengths (_ :* Nil)        -> BE.dimensionArray foldlD1' encodeElement
            Lengths (_ :* t@(_ :* _)) -> BE.dimensionArray foldlD' (dimension (Lengths t))

newtype DimDec dims
  = DimDec { dimDec :: forall d. Int -> BD.Value (NP (K Int) (d : dims)) }

instance (PgEncoding a, G.Vector v a, All Top dims, NotArray a) => P.FromField (DVector dims v a) where
    -- TODO: check oid
    fromField _ _ci (Just v) = case BD.valueParser (contentsP =<< dimP) v of
        Right a -> pure a
        Left e  -> fail $ T.unpack e
      where
        -- https://hackage.haskell.org/package/postgresql-binary-0.12.4/docs/src/PostgreSQL.Binary.Decoding.html#array
        dimP = BP.sized 4 (BD.int @Int) >>= \case
            -- Empty arrays have no dimension
            0 -> pure $ pure_NP (K 0)
            n -> BP.unitOfSize 8 *> dimDec (cpara_SList (Proxy @Top) nil cons) n
        nil = DimDec \case
            1 -> dimensionSize <&> \s -> K (fromIntegral s) :* Nil
            n -> BP.failure $ "Dimensions mismatch (" <> fromString (show n) <> ")"
        cons DimDec{dimDec} = DimDec \case
            0 -> BP.failure "Dimensions mismatch (0)"
            1 -> BP.failure "Dimensions mismatch (1)"
            n -> dimensionSize >>= \s -> (K (fromIntegral s) :*) <$> dimDec (n - 1)
        dimensionSize = BP.sized 4 (BD.int @Word32) <* BP.unitOfSize 4

        contentsP lengths = do
            contents <- G.replicateM total expect
            pure DVector{lengths = Lengths lengths, contents}
          where
            total = product $ collapse_NP lengths
            expect = maybe (BP.failure "Unexpected NULL") pure =<< decodeElement (Proxy @a)
    fromField _ _ Nothing = fail "type mismatch (FromField): Nothing, Haskell: Array"

instance (PgEncoding a, All Top dims, NotArray a) => P.FromField (Array dims a) where
    fromField a b c = dVectorArray <$> P.fromField a b c

instance (PgEncoding a, NotArray a) => P.ToField (Array dims a) where
    toField a b c d = P.toField a b c d . arrayDVector

data Param
  = forall a. (P.ToField a) => Param Oid a

instance Show Param where
    show _ = "Param _"

instance P.ToField Param where
    toField a b c d (Param _ x) = P.toField a b c d x
