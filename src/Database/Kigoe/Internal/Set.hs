{-# language TypeFamilies, UndecidableInstances #-}
{-# options_ghc -Wno-redundant-constraints -Wno-unticked-promoted-constructors #-}

module Database.Kigoe.Internal.Set
  ( Attr (..)
  , type (:++)
  , type Lookup  -- used in Ctx
  , type Rename
  , type Remove
  , type UnionAttrs
  , Elem, Expect, Subset
  , IntersectAttrs
  , UniqKeys, NewKey, Equiv
  ) where

import Data.SOP (All)
import Data.Type.Equality (type (==), type (~))
import GHC.Exts (type (~~))
import GHC.TypeLits (ErrorMessage (..), Nat, TypeError, type (+))
import RIO (Bool (..))

-- $setup
-- >>> import RIO

-- | The kind of names @n@ paired with types @t@.
--
-- /Example/:
--
-- >>> type K = "count" :@ Int32 :: Attr
type data Attr
  = forall n t. (:@) n t

-- TODO: Consider using this Concat which can provide better inference
-- https://gist.github.com/effectfully/bbe7f739b1d3cc40da47d3f223bd0621

type family (:++) (as :: [k]) (bs :: [k]) :: [k] where
    '[]      :++ ys = ys
    (x : xs) :++ ys = x : (xs :++ ys)

type family UnionAttrs (xs :: [Attr]) (ys :: [Attr]) :: [Attr] where
    UnionAttrs xs ys = xs :++ DeleteAll xs ys '[]

type family Rename (a :: Attr) (b :: k) (as :: [Attr]) :: [Attr] where
    Rename (a :@ t) b as = (b :@ t) : DeleteAll '[a :@ t] as '[]

type family Remove (a :: Attr) (as :: [Attr]) :: [Attr] where
    Remove a as = DeleteAll '[a] as '[]

-- Tail-recursive
type family DeleteAll (xs :: [Attr]) (ys :: [Attr]) (zs :: [Attr]) :: [Attr] where
    DeleteAll '[] ys      _  = ys
    DeleteAll xs '[]      zs = Reverse zs '[]
    DeleteAll xs (y : ys) zs = DeleteAll xs ys (Dedup y xs :++ zs)

type family Reverse (xs :: [k]) (zs :: [k]) :: [k] where
    Reverse '[]      zs = zs
    Reverse (x : xs) zs = Reverse xs (x : zs)

-- | Return '[a] only if its key is not already in the list of Attrs.
type family Dedup (a :: Attr) (as :: [Attr]) :: [Attr] where
    Dedup a '[]                      = '[a]
    Dedup (a :@ _) ((a :@ _) : rest) = '[]
    Dedup a        (b        : rest) = Dedup a rest

type family Lookup (x :: k) (as :: [Attr]) (i :: Nat) :: [(Nat, Attr)] where
    Lookup x '[]               _ = '[]
    Lookup x ((x :@ v) : rest) i = '[ '(i, x :@ v) ]
    Lookup x (b        : rest) i = Lookup x rest (i + 1)

class NewKey (key :: k) (as :: [Attr])
instance (IsUniq (key :@ ()) (Dedup (key :@ ()) as)) => NewKey key as

class UniqKeys (as :: [Attr])
instance UniqKeys '[]
instance (IsUniq a (Dedup a as), UniqKeys as) => UniqKeys (a : as)

class IsUniq (a :: Attr) (res :: [Attr])
instance
    TypeError (Text "The attribute " :<>: ShowType a
          :<>: Text " cannot be declared multiple times")
    => IsUniq (a :@ x) '[]
instance IsUniq a '[a]

-- | Expect xs to be identical to ys.
class Equiv (xs :: [Attr]) (ys :: [Attr])
instance (All (Expect xs) ys, All (CompatibleAttr ys) xs) => Equiv (xs :: [Attr]) (ys :: [Attr])

class Subset (super :: [Attr]) (sub :: [Attr])
instance (All (CompatibleAttr super) sub) => Subset super sub

class Elem (as :: [Attr]) (x :: xk) (i :: Nat) y | as x -> i y
instance (IsElem "No attribute named " " exists here" x i y (Lookup x as 1))
    => Elem as x i y

class Expect (as :: [Attr]) (a :: Attr)
instance (IsElem "Expected an attribute named " "" x i y (Lookup x as 1))
    => Expect as (x :@ y)

type family CannotBeFound :: k

class IsElem pfx sfx x i y res | res -> i y
instance
    ( TypeError (Text pfx :<>: ShowType x :<>: Text sfx)
    , y ~~ (CannotBeFound :: CannotBeFound)
    , i ~ (CannotBeFound :: Nat))
    => IsElem pfx sfx x i y '[]
instance (y ~ y', i ~ i') => IsElem pfx sfx x i y '[ '( i' , x :@ y' ) ]

class IntersectAttrs (xs :: [Attr]) (ys :: [Attr]) (zs :: [Attr]) | xs ys -> zs
instance IntersectAttrs xs '[] '[]
instance
  (IntersectAttrs xs ys zts, IsAttrJoinable (yk :@ yv) (Lookup yk xs 1) zhs, zs ~ (zhs :++ zts))
  => IntersectAttrs xs ((yk :@ yv) : ys) zs

class IsAttrJoinable (x :: Attr) (yis :: [(Nat, Attr)]) (ys :: [Attr]) | x yis -> ys
instance IsAttrJoinable x '[] '[]
instance (AttrSame x y (x == y), x ~ y) => IsAttrJoinable x '[ '(i, y) ] '[y]

class CompatibleAttr (xs :: [Attr]) (y :: Attr)
instance
  (Elem xs yk i yv', AttrSame (yk :@ yv) (yk :@ yv') (yv == yv'), yv ~ yv')
  => CompatibleAttr xs (yk :@ yv)

class AttrSame (x :: Attr) (y :: Attr) (eq :: Bool)
instance AttrSame x y True
instance
    TypeError (Text "The attributes "
          :$$: Text "  " :<>: ShowType x
          :$$: Text "and"
          :$$: Text "  " :<>: ShowType y
          :$$: Text "are incompatible")
    => AttrSame x y False
