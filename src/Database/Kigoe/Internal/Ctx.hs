{-# language TypeFamilies, UndecidableInstances #-}

module Database.Kigoe.Internal.Ctx
  ( Context (..)
  , Ø, HasAttr, HasOuter, HasParam, QuantCtx, CommonCtx
  ) where

import Data.Kind (Type)
import Data.Type.Equality (type (~), type (~~))
import Database.Kigoe.Internal.Set (Attr, Elem, Lookup, (:@))
import GHC.TypeError (ErrorMessage (ShowType, Text, (:<>:)), TypeError)
import GHC.TypeLits (Symbol)
import GHC.TypeNats (Nat)

type Ø = Params '[]

data Context
  = Across [Attr] Context
  | Under [Attr] Context
  | Summarising Context
  | Checking Context
  | Params [Attr]


class HasParam (ctx :: Context) (name :: Symbol) (i :: Nat) a | ctx name -> i a
instance (Elem params name i a) => HasParam (Params params) name i a
instance (HasParam ctx name i a) => HasParam (Across as ctx) name i a
instance (HasParam ctx name i a) => HasParam (Under as ctx) name i a
instance (HasParam ctx name i a) => HasParam (Summarising ctx) name i a
instance (HasParam ctx name i a) => HasParam (Checking ctx) name i a

class HasAttr (ctx :: Context) (name :: Symbol) (typ :: Type) | ctx name -> typ
instance (Elem as name i typ) => HasAttr (Across as ctx') name typ
instance (HasAttr ctx name a) => HasAttr (Under as ctx) name a
instance (HasAttr ctx name a) => HasAttr (Summarising ctx) name a
instance (HasAttr ctx name a) => HasAttr (Checking ctx) name a

class HasOuter (ctx :: Context) (name :: Symbol) (typ :: Type) | ctx name -> typ
instance (HasOuter' ctx name typ (Lookup name as 1)) => HasOuter (Under as ctx) name typ
instance (HasOuter ctx name typ) => HasOuter (Across as ctx) name typ
instance
    ( TypeError (Text "No outer attribute named " :<>: ShowType name :<>: Text " exists here")
    , typ ~~ (CannotBeFound :: CannotBeFound))
    => HasOuter (Params as) name typ
instance
    ( TypeError (Text "Cannot use outer within a Summary")
    , typ ~~ (CannotBeFound :: CannotBeFound))
    => HasOuter (Summarising ctx) name typ

type family CannotBeFound :: k

class HasOuter' ctx name typ res | ctx name res -> typ
instance (HasOuter ctx name typ) => HasOuter' ctx name typ '[]
instance (typ ~ typ') => HasOuter' ctx name typ '[ '( i' , x :@ typ' ) ]

class QuantCtx ctx quant | ctx -> quant
instance QuantCtx (Across as ctx) (Under as ctx)
instance (QuantCtx ctx quant) => QuantCtx (Summarising ctx) quant
instance
    ( TypeError (Text "Cannot use quantified expressions within a check constraint")
    , quant ~~ (CannotBeFound :: CannotBeFound))
    => QuantCtx (Checking ctx) quant

class CommonCtx ctx base | ctx -> base
instance CommonCtx (Params params) (Params params)
instance (CommonCtx ctx base) => CommonCtx (Across as ctx) base
instance (CommonCtx ctx base) => CommonCtx (Under as ctx) base
instance (CommonCtx ctx base) => CommonCtx (Summarising ctx) base
