{-# language OverloadedStrings, StrictData #-}

module Database.Kigoe.Internal.Transaction where

import Database.Kigoe.Internal.PG
  (CommandTag (..), Connection, UnexpectedResponse (UnexpectedResponse), oneshotCmd)
import Database.Kigoe.Internal.SQL (RenderedCmd (..))
import Database.PostgreSQL.Pure.List (ErrorResponse (..))
import RIO

-- | Transaction isolation levels per the SQL standard.
data IsolationLevel = Serialisable | RepeatableRead | ReadCommitted | ReadUncommitted deriving
  ( Bounded
  , Enum
  , Eq
  , Ord
  , Show
  )

-- | The SQL standard @SERIALIZABLE@ isolation level.
serialisable :: TransactionSettings -> TransactionSettings
serialisable s = s{isolationLevel = Serialisable}

-- | The SQL standard @REPEATABLE READ@ isolation level.
repeatableRead :: TransactionSettings -> TransactionSettings
repeatableRead s = s{isolationLevel = RepeatableRead}

-- | The SQL standard @READ COMMITTED@ isolation level.
readCommitted :: TransactionSettings -> TransactionSettings
readCommitted s = s{isolationLevel = ReadCommitted}

-- | The SQL standard @READ UNCOMMITTED@ isolation level.
readUncommitted :: TransactionSettings -> TransactionSettings
readUncommitted s = s{isolationLevel = ReadUncommitted}

-- | Declaring a transaction read-only can assist performance.
data ReadWriteMode = ReadOnly | ReadWrite deriving (Bounded, Enum, Eq, Ord, Show)

data RetryMode
  = RetrySerializationFailure -- ^ Retry the transaction on @serialization_failure@ errors.
  | DontRetry -- ^ Do not retry.
  deriving (Bounded, Enum, Eq, Ord, Show)

-- | Retry the transaction on @serialization_failure@ errors.
retrySerializationFailure :: TransactionSettings -> TransactionSettings
retrySerializationFailure s = s{retryMode = RetrySerializationFailure}

-- | Do not retry.
dontRetry :: TransactionSettings -> TransactionSettings
dontRetry s = s{retryMode = DontRetry}

-- | Settings for the execution of a particular transaction.
data TransactionSettings
  = TransactionSettings
    { isolationLevel :: IsolationLevel
    , readWriteMode  :: ReadWriteMode
    , retryMode      :: RetryMode
    }
  deriving (Eq, Show)

-- | Read-only serialisable transaction, retrying on serialisation failure.
readOnly :: TransactionSettings
readOnly = TransactionSettings Serialisable ReadOnly RetrySerializationFailure

-- | Read-write serialisable transaction, retrying on serialisation failure.
readWrite :: TransactionSettings
readWrite = TransactionSettings Serialisable ReadWrite RetrySerializationFailure

-- | Start a transaction. Consider using 'transact' or 'attempt' instead.
startTransaction :: Connection -> TransactionSettings -> IO ()
startTransaction c mode = oneshotCmd c (RenderedCmd cmd) [] >>= \case
    StartTransactionTag -> pure ()
    t                   -> throwIO . UnexpectedResponse $ show t
  where
    cmd = "start transaction isolation level " <> isoSql <> ", " <> rwSql
    isoSql = case isolationLevel of
        Serialisable    -> "serializable"
        RepeatableRead  -> "repeatable read"
        ReadCommitted   -> "read committed"
        ReadUncommitted -> "read uncommitted"
    rwSql = case readWriteMode of
        ReadWrite -> "read write"
        ReadOnly  -> "read only"
    TransactionSettings{isolationLevel, readWriteMode} = mode

-- | Commit the current transaction. Consider using 'transact' or 'attempt' instead.
commit :: Connection -> IO ()
commit c = oneshotCmd c (RenderedCmd "commit") [] >>= \case
    CommitTag -> pure ()
    t         -> throwIO . UnexpectedResponse $ show t

-- | Roll back the current transaction. Consider using 'transact' or 'attempt' instead.
rollback :: Connection -> IO ()
rollback c = oneshotCmd c (RenderedCmd "rollback") [] >>= \case
    RollbackTag -> pure ()
    t           -> throwIO . UnexpectedResponse $ show t

-- | Run an action in a database transaction.
-- Rollback on exception, commit otherwise.
-- Note the IO action may be retried under 'retrySerializationFailure' (the default).
transact :: MonadUnliftIO m => Connection -> TransactionSettings -> m a -> m a
transact c mode f = either id id <$> attempt c mode (pure <$> f)

-- | Run an action in a database transaction.
-- Rollback on 'Left' (or exception), commit on 'Right'.
-- Note the IO action may be retried under 'retrySerializationFailure' (the default).
attempt :: MonadUnliftIO m => Connection -> TransactionSettings -> m (Either a b) -> m (Either a b)
attempt c mode f = mask \restore -> do
    e <- shot restore
    e <$ liftIO case e of
        Left _  -> rollback c
        Right _ -> commit c
  where
    shot restore = do
        liftIO $ startTransaction c mode
        dealWith restore do
            e <- restore f
            -- WHNF so it's safe to match on
            pure $! e
    dealWith restore = case retryMode mode of
        DontRetry -> (`onException` liftIO (rollback c))
        RetrySerializationFailure -> handleSyncOrAsync \e -> case fromException e of
            Just ErrorResponse{code = "40001"} -> liftIO (rollback c) *> shot restore
            _                                  -> liftIO (rollback c) *> throwIO e
