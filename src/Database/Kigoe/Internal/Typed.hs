{-# language GADTs, OverloadedStrings, StrictData, TypeFamilies, UndecidableInstances #-}
{-# options_ghc -Wno-orphans -Wno-redundant-constraints #-}
{-# options_haddock hide #-}

module Database.Kigoe.Internal.Typed where

import Data.Coerce (Coercible, coerce)
import Data.Kind (Type)
import Data.Semigroup (Endo (..), stimesMonoid)
import Data.SOP (All, I (..), K (..), Top, cpara_SList)
import Data.SOP.NP (NP (..), cmap_NP, collapse_NP, cpure_NP, ctraverse'_NP, czipWith_NP)
import Data.SOP.NP qualified as NP
import Data.Type.Equality (type (~))
import Data.Vector.Unboxed qualified as U
import Database.Kigoe.Fielded (Fielded (..), Label, Named (..))
import Database.Kigoe.Internal.Absurdity (NotArray, NotMaybe)
import Database.Kigoe.Internal.Array qualified as A
import Database.Kigoe.Internal.AST
import Database.Kigoe.Internal.Ctx
  (CommonCtx, Context (..), HasAttr, HasOuter, HasParam, QuantCtx, Ø)
import Database.Kigoe.Internal.PG qualified as P
import Database.Kigoe.Internal.Schema (SchemaRef (..), Table, schemaObject)
import Database.Kigoe.Internal.Set
import Database.Kigoe.Internal.SQL qualified as S
import Database.Kigoe.Numeric (Numeric (..))
import Generics.SOP.Subrecord (IsSubrecordOf (..))
import GHC.Exts (Proxy#, proxy#)
import GHC.OverloadedLabels (IsLabel (..))
import GHC.TypeLits (KnownNat, KnownSymbol, TypeError, natVal, symbolVal)
import GHC.TypeLits qualified as TL
import RIO hiding (Int8, join, sets)
import RIO.NonEmpty (nonEmpty)
import RIO.Text qualified as T
import RIO.Time (UTCTime)
import RIO.Vector (fromList)

-- $setup
-- >>> :set -Wno-simplifiable-class-constraints -XBlockArguments -XNoImplicitPrelude -XUndecidableInstances
-- >>> import Data.SOP
-- >>> import Database.Kigoe.Fielded
-- >>> import Database.Kigoe.Internal.Array (Array, D1)
-- >>> import Database.Kigoe.Internal.Ctx (HasParam, Ø)
-- >>> import Database.Kigoe.Internal.PG (Connection)
-- >>> import Database.Kigoe.Internal.Schema (uncheckedTableSchema)
-- >>> import Database.Kigoe.Internal.Set
-- >>> import GHC.TypeLits
-- >>> import RIO hiding (Int8, join, max, some)
-- >>> someTable n as = table (uncheckedTableSchema Nothing n as) n

-- | A bag relation value with heading @heading@ in a context @ctx@.
--
-- /Definitions:/
--
--   [bag relation value]:
--     Or /relation/ for short; a bag of rel-tuples with the same heading.
--
--   [rel-tuple]:
--     A set of (name, value) pairs; the pairs are called /attribute values/. The term /rel-tuple/
--     is used here to avoid confusion with Haskell tuples, which are ordered and lack names.
--
--   [heading]:
--     A set of (name, type) pairs; the pairs are called /attributes/. The heading of a rel-tuple
--     is the names and types of its attribute values. The heading of a relation is the common
--     heading of the rel-tuples that make up the relation.
--
-- GHC has no type-level sets, so we use lists instead.
--
-- /Examples:/
--
-- >>> r = values [#count <~ lit @Int32 5] :: Rel ctx '["count" :@ Int32]
-- >>> v = values [Nil] :: Rel ctx '[]
newtype Rel (ctx :: Context) (heading :: [Attr])
  = Rel W
  deriving (Show)

-- | A list of tuples with heading @heading@ in a context with parameters @params@.
newtype List (params :: [Attr]) (heading :: [Attr])
  = List L
  deriving (Show)

-- | A scalar expression of type @typ@ in a context @ctx@.
newtype Expr (ctx :: Context) (typ :: Type)
  = Expr E
  deriving (Show)

-- | String literals can be used for 'Text'-typed expressions with @-XOverloadedStrings@.
instance (a ~ Text) => IsString (Expr ctx a) where
    fromString = lit . fromString

-- | A operation for use with 'summarise' in a context @ctx@ producing a value
-- of type @typ@.
newtype Summary (ctx :: Context) (typ :: Type)
  = Summary Agg
  deriving (Show)

-- | The class of Haskell 'Type's that can be represented in SQL.
--
-- /Example:/
--
-- >>> :{
-- newtype PrimaryKey a = PrimaryKey Int32
--   deriving newtype (SqlType)
-- :}
class (Coercible a (Rep a), P.PgEncoding (Rep a)) => SqlType a where
    type Rep a
    haskT :: Proxy# a -> T
    haskN :: Proxy# a -> S.Nullable
    haskN _ = S.NotNullable
    haskLit :: Proxy# a -> Rep a -> Lit
    hasksP :: (NotArray (Rep a)) => Proxy# a -> [Rep a] -> P.Param
    hasksP _ = P.param . A.oneDArray . fromList

instance SqlType Int64 where
    type Rep Int64 = Int64
    haskT _ = Int8
    haskLit _ = LitInt8
    hasksP _ = P.uvecParam . A.oneD . U.fromList
instance SqlType Int32 where
    type Rep Int32 = Int32
    haskT _ = Int4
    haskLit _ = LitInt4
    hasksP _ = P.uvecParam . A.oneD . U.fromList
instance SqlType Int16 where
    type Rep Int16 = Int16
    haskT _ = Int2
    haskLit _ = LitInt2
    hasksP _ = P.uvecParam . A.oneD . U.fromList
instance SqlType Bool where
    type Rep Bool = Bool
    haskT _ = Boolean
    haskLit _ = LitBoolean
    hasksP _ = P.uvecParam . A.oneD . U.fromList
instance SqlType Text where
    type Rep Text = Text
    haskT _ = Varchar
    haskLit _ = LitAppendParam Varchar . P.param
instance SqlType ByteString where
    type Rep ByteString = ByteString
    haskT _ = Bytea
    haskLit _ = LitAppendParam Bytea . P.param
instance SqlType UTCTime where
    type Rep UTCTime = UTCTime
    haskT _ = TimestampTz
    haskLit _ = LitTimestampTz
instance (KnownNat precision, KnownNat scale) => SqlType (Numeric precision scale) where
    type Rep (Numeric precision scale) = Numeric precision scale
    haskT _ = NumericFixed (fromInteger (natVal (Proxy @precision))) (fromInteger (natVal (Proxy @scale)))
    haskLit _ = LitNumF (fromInteger (natVal (Proxy @precision))) (fromInteger (natVal (Proxy @scale))) . coerce
instance (SqlType a, NotArray (Rep a), All Top dims) => SqlType (A.Array dims a) where
    type Rep (A.Array dims a) = A.Array dims (Rep a)
    haskT p = appEndo (stimesMonoid (arrayLen p) (Endo ArrayOf)) (haskT (proxy# @a))
    haskLit _ = LitAppendParam (haskT (proxy# @a)) . P.param
instance (SqlType a, NotMaybe (Rep a)) => SqlType (Maybe a) where
    type Rep (Maybe a) = Maybe (Rep a)
    haskT _ = haskT (proxy# @a)
    haskN _ = S.Nullable
    haskLit _ = maybe (LitNull (haskT (proxy# @a))) (haskLit (proxy# @a))

newtype Len xs
  = Len { unLen :: Int }

arrayLen :: forall dims a. (All Top dims) => Proxy# (A.Array dims a) -> Int
arrayLen _ = unLen $ cpara_SList @_ @Top @dims Proxy (Len 1) (\(Len i) -> Len (i + 1))

data SingAT (a :: Attr) where
  SingAT :: (SqlType t) => SingAT (n :@ t)

class KnownAttr (a :: Attr) where
    reflectN :: Proxy a -> Text
    singAT :: Proxy a -> SingAT a
instance (KnownSymbol n, SqlType t, a ~ (n :@ t)) => KnownAttr a where
    reflectN _ = T.pack (symbolVal (Proxy @n))
    singAT _ = SingAT

reflectT :: KnownAttr a => Proxy a -> T
reflectT p = case singAT p of
    s@SingAT -> case s of
        (_ :: SingAT (n :@ t)) -> haskT (proxy# @t)

attribute :: forall a. KnownAttr a => Proxy a -> A
attribute _ = A (reflectN @a Proxy) (reflectT @a Proxy)

-- | Use a parameter in an expression.
--
-- /Example:/
--
-- >>> p = param #id :: (SqlType t, KnownNat i, HasParam ctx "id" i t) => Expr ctx t
param ::
    forall t name i ctx.
    (HasParam ctx name i t, KnownNat i, SqlType t)
 => Label name
 -> Expr ctx t
param _ = Expr $ Param (haskT (proxy# @t)) (fromIntegral (natVal (Proxy @i)))

-- | A relation value passed as a parameter.
--
-- /Example:/
--
-- >>> :{
-- r = paramValues @'["x" :@ Int32] #numbers
--   :: (HasParam ctx "numbers" i (Rel Ø '["x" :@ Int32]), KnownNat i)
--   => Rel ctx '["x" :@ Int32]
-- :}
paramValues ::
    forall as name i ctx.
    (HasParam ctx name i (Rel Ø as), KnownNat i, All KnownAttr as)
 => Label name
 -> Rel ctx as
paramValues _ = Rel $ ParamValues (collapse_NP (reflects @as)) (fromIntegral (natVal (Proxy @i)))

-- | A literal makes a trivial expression.
lit :: forall t ctx. (SqlType t) => t -> Expr ctx t
lit = Expr . Lit . haskLit (proxy# @t) . coerce

(∧) ::
    forall ctx.
    Expr ctx Bool
 -> Expr ctx Bool
 -> Expr ctx Bool
Expr l ∧ Expr r = Expr $ BinOp And Boolean l r
infixr 3 ∧

(∨) ::
    forall ctx.
    Expr ctx Bool
 -> Expr ctx Bool
 -> Expr ctx Bool
Expr l ∨ Expr r = Expr $ BinOp Or Boolean l r
infixr 2 ∨

-- | @not@; Boolean negation.
nicht :: forall ctx. Expr ctx Bool -> Expr ctx Bool
nicht (Expr e) = Expr $ UnaryOp Not Boolean e

(≥) ::
    forall t ctx.
    (SqlOrd t)
 => Expr ctx t
 -> Expr ctx t
 -> Expr ctx Bool
e@(Expr l) ≥ Expr r = ord e . Expr $ BinOp GEq Boolean l r
infix 4 ≥

(≠) ::
    forall t ctx.
    (SqlOrd t)
 => Expr ctx t
 -> Expr ctx t
 -> Expr ctx Bool
e@(Expr l) ≠ Expr r = ord e . Expr $ BinOp NEq Boolean l r
infix 4 ≠

-- | The number of characters.
characterLength :: Expr ctx Text -> Expr ctx Int32
characterLength (Expr e) = Expr $ FunOp "character_length" Int4 [e]

-- | The number of octets (bytes); this depends on server encoding.
octetLength :: Expr ctx Text -> Expr ctx Int32
octetLength (Expr e) = Expr $ FunOp "octet_length" Int4 [e]

byteLength :: Expr ctx ByteString -> Expr ctx Int32
byteLength (Expr e) = Expr $ FunOp "octet_length" Int4 [e]

-- | A reference to a database function that takes @args@ and returns @ret@.
newtype FunRef (args :: [Type]) (ret :: Type)
  = FunRef Text

-- | Assert a database function exists, without checking.
uncheckedFunction ::
    forall args ret.
    (SqlType ret, All SqlType args)
 => Text
 -> Proxy args
 -> Proxy ret
 -> FunRef args ret
uncheckedFunction name _ _ = FunRef name

-- | Apply a database function.
apply ::
    forall args ret ctx.
    (SqlType ret, All SqlType args)
 => FunRef args ret
 -> NP (Expr ctx) args
 -> Expr ctx ret
apply (FunRef name) args = Expr $ FunOp name (haskT (proxy# @ret)) $
    collapse_NP (cmap_NP (Proxy @SqlType) (\(Expr e) -> K e) args)

-- | The class of Haskell 'Type's with SQL representations that support basic arithmetic.
--
-- /Examples:/
--
-- >>> :{
-- newtype Count = Count Int32
--   deriving newtype (SqlType, SqlNum)
-- :}
--
-- >>> :{
-- p = param @Count #count - 1
--   :: (HasParam ctx "count" i Count, KnownNat i)
--   => Expr ctx Count
-- :}
---
class (SqlType t) => SqlNum t where
    exprFromInteger :: Integer -> Expr ctx t

instance SqlNum Int16 where
    exprFromInteger = lit . fromInteger
instance SqlNum Int32 where
    exprFromInteger = lit . fromInteger
instance SqlNum Int64 where
    exprFromInteger = lit . fromInteger

-- | Expressions with type in 'SqlNum' support basic arithmetic.
instance (SqlNum t) => Num (Expr ctx t) where
    fromInteger = exprFromInteger
    Expr a + Expr b = Expr $ BinOp Plus (haskT (proxy# @t)) a b
    Expr a - Expr b = Expr $ BinOp Subtract (haskT (proxy# @t)) a b
    Expr a * Expr b = Expr $ BinOp Mul (haskT (proxy# @t)) a b
    abs (Expr a) = Expr $ UnaryOp Abs (haskT (proxy# @t)) a
    signum (Expr a) = Expr $ UnaryOp Signum (haskT (proxy# @t)) a

-- | Remainder after division.
remainder ::
    forall t ctx.
    (SqlNum t)
 => Expr ctx t
 -> Expr ctx t
 -> Expr ctx t
remainder (Expr a) (Expr b) = Expr $ BinOp Rem (haskT (proxy# @t)) a b

-- | The class of Haskell 'Type's with SQL representations that have a total ordering.
class (SqlType t) => SqlOrd t where
    sqlComp :: Proxy t -> ()

instance SqlOrd Int16 where
    sqlComp _ = ()
instance SqlOrd Int32 where
    sqlComp _ = ()
instance SqlOrd Int64 where
    sqlComp _ = ()
instance SqlOrd Bool where
    sqlComp _ = ()
instance SqlOrd Text where
    sqlComp _ = ()
instance SqlOrd ByteString where
    sqlComp _ = ()
instance SqlOrd UTCTime where
    sqlComp _ = ()
instance (KnownNat precision, KnownNat scale) => SqlOrd (Numeric precision scale) where
    sqlComp _ = ()
instance (SqlOrd t, SqlType (A.Array dims t)) => SqlOrd (A.Array dims t) where
    sqlComp _ = ()

ord :: forall t ctx b. SqlOrd t => Expr ctx t -> b -> b
ord _ b = case sqlComp (Proxy @t) of
    () -> b

-- | A relation from a list of rel-tuples.
--
-- /Examples:/
--
-- >>> :{
-- r = values [#count <~ lit @Int32 5]
--   :: Rel ctx '["count" :@ Int32]
-- :}
--
-- >>> :{
-- r = values [#q <~ param @Bool #p]
--   :: (HasParam ctx "p" i Bool, KnownNat i)
--   => Rel ctx '["q" :@ Bool]
-- :}
--
values ::
    forall ctx as.
    (UniqKeys as, All KnownAttr as)
 => [NP (Named (Expr ctx)) as]
 -> Rel ctx as
values vs = Rel $ Values (collapse_NP (reflects @as)) (rw <$> vs)
  where
    rw :: NP (Named (Expr ctx)) as -> [E]
    rw = collapse_NP . cmap_NP (Proxy @KnownAttr) (\(Named (Expr e)) -> K e)

-- | A relation from a preexisting database table.
--
-- /Example:/
--
-- >>> r = table (uncheckedTableSchema Nothing #products (Proxy @'["id" :@ Int32])) #products :: Rel ctx '["id" :@ Int32]
table ::
    forall ctx name as cs schema i.
    (All KnownAttr as, KnownSymbol name, Elem schema name i (Table cs as))
 => SchemaRef schema
 -> Label name
 -> Rel ctx as
table sch name = Rel $
    Table (collapse_NP (reflects @as)) (schemaObject sch name)

-- | Define a common relation that can be referred to with 'common'. Useful to avoid redundant work when
-- a complex or expensive relational subexpression is used more than once. Prefer to use @let@ or @where@
-- if you're factoring out subexpressions just for readability.
--
-- >>> :{
-- r = withCommon
--       ( someTable #tracks (Proxy @'["title" :@ Text, "genre" :@ Text, "bpm" :@ Int32])
--       & join (values [#genre <~ "trance"])
--       & project (#title :* #bpm))
--     \trance ->
--       common trance
--       & join (common trance & reexpress (#title2 <~ #title :* #bpm2 <~ #bpm))
--       & select (#title ≠ #title2)
--       & extend (#bpmDiff <~ abs (#bpm - #bpm2))
--    :: Rel ctx '["bpmDiff" :@ Int32, "title" :@ Text, "bpm" :@ Int32, "title2" :@ Text, "bpm2" :@ Int32]
-- :}
withCommon ::
    forall ctx base as bs.
    (CommonCtx ctx base)
 => Rel base as
 -> (CommonRef as -> Rel ctx bs)
 -> Rel ctx bs
withCommon (Rel w) f = Rel $ CAbs r w x
  where
    Rel x = f (CommonRef r (wa w))
    r = Name (withDepth x)

-- | A relation from a surrounding 'withCommon' call.
common :: forall ctx as. CommonRef as -> Rel ctx as
common (CommonRef r as) = Rel $ COcc as r

-- | A reference to a relation from a surrounding 'withCommon' call with heading @heading@.
data CommonRef heading
  = CommonRef ~Name [A]

-- | Add new attributes to a relation.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #people (Proxy @'["age" :@ Int32, "height" :@ Int32])
--   & extend (#tall <~ (#height ≥ 180))
--   :: Rel ctx '["tall" :@ Bool, "age" :@ Int32, "height" :@ Int32]
-- :}
--
extend ::
    forall ctx bs as.
    (UniqKeys bs, All KnownAttr bs)
 => NP (Named (Expr (Across as ctx))) bs
 -> Rel ctx as
 -> Rel ctx (UnionAttrs bs as)
extend vs (Rel w) = Rel . maybe w (Extend w) . nonEmpty $ namedExprEs vs

namedExprEs ::
    forall ctx as bs.
    (All KnownAttr bs)
 => NP (Named (Expr (Across as ctx))) bs
 -> [(Text, E)]
namedExprEs vs = collapse_NP $ cmap_NP (Proxy @KnownAttr) mkP vs
  where
    mkP :: forall a. KnownAttr a => Named (Expr (Across as ctx)) a -> K (Text, E) a
    mkP (Named (Expr e)) = K . (,e) $ reflectN @a Proxy

-- | A reference to an existing attribute @a@ in @ctx@.
--
-- /Example:/
--
-- >>> a = #frob :: Elem ctx "frob" i t => AttrRef ctx ("frob" :@ t)
data AttrRef (ctx :: [Attr]) (a :: Attr) = AttrRef
  deriving (Show)

instance (Elem ctx k i t, a ~ (k :@ t)) => IsLabel k (AttrRef ctx a) where
    fromLabel = AttrRef

-- | As a convenience, a single label can be provided to 'project'.
instance (Elem ctx k i t, as ~ '[k :@ t]) => IsLabel k (NP (AttrRef ctx) as) where
    fromLabel = AttrRef :* Nil

-- | An attribute can be used in an expression context by label.
instance (HasAttr ctx k u, u ~ t, KnownAttr (k :@ t)) => IsLabel k (Expr ctx t) where
    fromLabel = Expr . AttrE $ attribute (Proxy @(k :@ t))

-- | Keep only some attributes of a relation.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #points (Proxy @'["x" :@ Int32, "y" :@ Int32, "z" :@ Int32])
--   & project (#x :* #y)
--   :: Rel ctx '["x" :@ Int32, "y" :@ Int32]
-- :}
project ::
    forall ctx bs as.
    (UniqKeys bs, All KnownAttr bs)
 => NP (AttrRef as) bs
 -> Rel ctx as
 -> Rel ctx bs
project _ (Rel w) = Rel $ Project (collapse_NP (reflects @bs)) w

-- | Rename an attribute in a relation.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #songs (Proxy @'["title" :@ Text, "artist" :@ Text])
--   & rename #artist #genius
--   :: Rel ctx '["genius" :@ Text, "title" :@ Text]
-- :}
rename ::
    forall ctx a b bs as.
    (UniqKeys bs, KnownAttr a, KnownSymbol b, bs ~ Rename a b as)
 => AttrRef as a
 -> Label b
 -> Rel ctx as
 -> Rel ctx bs
rename _ _ (Rel w) = Rel $ Rename f t w
  where
    a = attribute @a Proxy
    f = aName a
    t = a{aName = T.pack (symbolVal (Proxy @b))}

-- | Transform a relation to contain new attributes.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #people (Proxy @'["name" :@ Text, "age" :@ Int32, "height" :@ Int32])
--   & reexpress (#name <~ #name :* #canDrive <~ (#age ≥ 18))
--   :: Rel ctx '["name" :@ Text, "canDrive" :@ Bool]
-- :}
reexpress ::
    (UniqKeys bs, All KnownAttr bs)
 => NP (Named (Expr (Across as ctx))) bs
 -> Rel ctx as
 -> Rel ctx bs
reexpress vs (Rel w) = Rel . Reexpress w $ namedExprEs vs

-- | Join two relations on their common attributes. To be joinable, attributes with the same name
-- must have the same type.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #classes (Proxy @'["subject" :@ Text, "room" :@ Int32])
--   & join (someTable #rooms (Proxy @'["room" :@ Int32, "building" :@ Text]))
--   :: Rel ctx '["subject" :@ Text, "room" :@ Int32, "building" :@ Text]
-- :}
join ::
    forall ctx as bs js.
    (IntersectAttrs as bs js)
 => Rel ctx bs
 -> Rel ctx as
 -> Rel ctx (UnionAttrs as bs)
join (Rel w0) (Rel w1) = Rel $ Join w1 w0

-- | Join two relations on their common attributes. To be joinable, attributes with the same name
-- must have the same type.
--
-- This is the same operation as 'join', but may make the common attributes clearer to readers.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #classes (Proxy @'["subject" :@ Text, "room" :@ Int32])
--   & joinOn #room
--     (someTable #rooms (Proxy @'["room" :@ Int32, "building" :@ Text]))
--   :: Rel ctx '["subject" :@ Text, "room" :@ Int32, "building" :@ Text]
-- :}
joinOn ::
    forall js ctx as bs js'.
    (IntersectAttrs as bs js, All (Expect js') js)
 => NP (AttrRef js) js'
 -> Rel ctx bs
 -> Rel ctx as
 -> Rel ctx (UnionAttrs as bs)
joinOn _ = join

-- | Keep only those rel-tuples that satisfy a predicate.
--
-- /Example:/
--
-- >>> :{
-- r = someTable #people (Proxy @'["age" :@ Int32, "height" :@ Int32])
--   & select (#height ≥ 180)
--   :: Rel ctx '["age" :@ Int32, "height" :@ Int32]
-- :}
select :: Expr (Across as ctx) Bool -> Rel ctx as -> Rel ctx as
select (Expr e) (Rel w) = Rel $ Select e w

-- | Use the contents of an n-dimensional array as a rel-tuple.
--
-- /Example:/
--
-- >>> :{
-- r = unnestAs #name (param @(Array D1 Text) #names)
--   :: (HasParam ctx "names" i (Array D1 Text), KnownNat i)
--   => Rel ctx '["name" :@ Text]
-- :}
unnestAs ::
    forall t ctx name dims.
    (KnownSymbol name, SqlType t)
 => Label name
 -> Expr ctx (A.Array dims t)
 -> Rel ctx '[name :@ t]
unnestAs _ (Expr e) = Rel $ Unnest WithoutOrdinality (attribute $ Proxy @(name :@ t)) e

-- | Use the contents of an n-dimensional array as a rel-tuple, with an additional attribute
-- for the storage order.
--
-- /Example:/
--
-- >>> :{
-- l = unnestWithOrdinality #track #order (param @(Array D1 Text) #tracks)
--   & firstNOrdered 10 (asc #order :| [])
--   :: (Elem params "tracks" i (Array D1 Text), KnownNat i)
--   => List params '["track" :@ Text, "order" :@ Int64]
-- :}
unnestWithOrdinality ::
    forall t ctx name ordName dims.
    (KnownSymbol name, KnownSymbol ordName, SqlType t)
 => Label name
 -> Label ordName
 -> Expr ctx (A.Array dims t)
 -> Rel ctx '[name :@ t, ordName :@ Int64]
unnestWithOrdinality _ _ (Expr e) = Rel $ Unnest
    (WithOrdinality . T.pack . symbolVal $ Proxy @ordName)
    (attribute $ Proxy @(name :@ t))
    e

-- | Group rel-tuples with the same values for attributes @by@ and calculate
-- summaries of those groups. The resulting tuples contain the common attributes
-- @by@ and the summaries @sums@.
--
-- Note that zero rel-tuples always produce zero groups.
--
-- /Examples:/
--
-- >>> :{
-- r = someTable #songs (Proxy @'["title" :@ Text, "artist" :@ Text])
--   & summarise #artist (#songCount <~ count)
--   :: Rel ctx '["artist" :@ Text, "songCount" :@ Int64]
-- :}
--
-- If there are zero classes, the following relation will have zero tuples.
--
-- >>> :{
-- r = someTable #classes (Proxy @'["subject" :@ Text, "room" :@ Int32])
--   & summarise Nil (#classCount <~ count)
--   :: Rel ctx '["classCount" :@ Int64]
-- :}
summarise ::
    forall ctx by sums as.
    (UniqKeys (by :++ sums), All KnownAttr by, All KnownAttr sums)
 => NP (AttrRef as) by
 -> NP (Named (Summary (Summarising (Across as ctx)))) sums
 -> Rel ctx as
 -> Rel ctx (by :++ sums)
summarise _ sums (Rel w) = Rel $ Summarise (collapse_NP (reflects @by)) as w
  where
    as = collapse_NP $ cmap_NP (Proxy @KnownAttr) mkP sums
    mkP :: forall a. KnownAttr a => Named (Summary (Summarising (Across as ctx))) a -> K (Text, Agg) a
    mkP (Named (Summary a)) = K . (,a) $ reflectN @a Proxy

-- | Count the number of tuples in the group.
count :: Summary ctx Int64
count = Summary $ CountTuples Nothing

-- | Count the number of tuples in the group where a predicate holds.
countSelective :: Expr ctx Bool -> Summary ctx Int64
countSelective (Expr p) = Summary $ CountTuples (Just p)

-- | Sum an expression of type 'Int32' over all tuples in the group.
sumInt32 :: Expr ctx Int32 -> Summary ctx Int64
sumInt32 (Expr e) = Summary $ SumInt32 e

-- | True if an expression is true over all tuples in the group, otherwise
-- false.
boolAnd :: Expr ctx Bool -> Summary ctx Bool
boolAnd (Expr e) = Summary $ BoolAnd e

-- | Maximum value of an expression over all tuples in the group.
max :: (SqlOrd t) => Expr ctx t -> Summary ctx t
max x@(Expr e) = ord x . Summary $ Max e Nothing

-- | Collect an array from an expression over all tuples in the group.
--
-- Aggregated array values must all have the same non-zero length.
arrayAgg :: (ArrayOf t ts) => Expr ctx t -> Summary ctx ts
arrayAgg (Expr e) = Summary $ ArrayAgg e S.Indistinct Nothing []

-- | Collect an array from all unique values of an expression over all tuples in
-- the group where an additional predicate holds.
--
-- Unlike PostgreSQL array_agg, this evaluates to an empty array over an empty
-- group.
arrayAggSelectiveDistinct ::
    (ArrayOf t ts)
 => Expr ctx Bool
 -> Expr ctx t
 -> Summary ctx ts
arrayAggSelectiveDistinct (Expr p) (Expr e) = Summary $ ArrayAgg e S.Distinct (Just p) []

-- | Collect an array from all values of an expression over all tuples in the
-- group ordered by some expressions where an additional predicate holds.
--
-- Unlike PostgreSQL array_agg, this evaluates to an empty array over an empty
-- group.
arrayAggSelectiveOrderBy ::
    (ArrayOf t ts)
 => Expr ctx Bool
 -> NonEmpty (Order ctx)
 -> Expr ctx t
 -> Summary ctx ts
arrayAggSelectiveOrderBy (Expr p) os (Expr e) =
    Summary $ ArrayAgg e S.Indistinct (Just p) ((\(Order o) -> o) <$> toList os)

class ArrayOf t ts | t -> ts, ts -> t
instance (ts ~ ArrayUp t, t ~ ArrayDown ts) => ArrayOf t ts

type family ArrayUp t where
    ArrayUp (A.Array dims t)         = A.Array (A.D : dims) t
    ArrayUp (Maybe (A.Array dims t)) =
        TypeError (TL.Text "Null arrays cannot be accumulated"  TL.:$$:
                   TL.Text "  arising from a use of ‘arrayAgg’" TL.:$$:
                   TL.Text "  at type " TL.:<>: TL.ShowType (Maybe (A.Array dims t))
                   )
    ArrayUp t                        = A.Array A.D1 t

type family ArrayDown ts where
    ArrayDown (A.Array ('[] :: [A.Dim]) t) = t
    ArrayDown (A.Array (d : dims)       t) = A.Array dims t
    ArrayDown (Maybe (A.Array ('[] :: [A.Dim]) t)) = Maybe t
    ArrayDown (Maybe (A.Array (d : dims)       t)) = Maybe (A.Array dims t)

-- | A quantified expression from 'some'.
data Quant (ctx :: Context) (t :: Type)
  = Quant Q W

-- | Produce a quantified expression for comparison (with '≥..' etc.) which is true when
-- comparison against some value in the provided relation is true. That is, @expr op 'some' rel@
-- is true if there is at least one value in @rel@ where @expr op value@.
--
-- /Example:/
--
-- Drivers who have been fined.
--
-- >>> :{
-- r = someTable #drivers (Proxy @'["name" :@ Text, "license_no" :@ Text])
--   & select (#license_no =.. some (someTable #fines (Proxy @'["license_no" :@ Text])))
--   :: Rel ctx '["name" :@ Text, "license_no" :@ Text]
-- :}
some :: (QuantCtx ctx quant) => Rel quant '[n :@ t] -> Quant ctx t
some (Rel w) = Quant Some w

-- | Refer to a name from outside this quantified expression. If quantified expressions are nested,
-- inner names shadow outermore names.
--
-- /Example:/
--
-- People who have the same name as someone in another city.
--
-- >>> :{
-- people = someTable #people (Proxy @'["name" :@ Text, "city" :@ Text])
-- r = people
--   & select (#name =.. some (
--       people
--       & select (#city ≠ outer #city)
--       & project #name))
--   :: Rel ctx '["name" :@ Text, "city" :@ Text]
-- :}
outer :: forall t n ctx. (HasOuter ctx n t, KnownAttr (n :@ t)) => Label n -> Expr ctx t
outer _ = Expr . OuterE $ attribute (Proxy @(n :@ t))

(=..) :: (SqlOrd t) => Expr ctx t -> Quant ctx t -> Expr ctx Bool
e@(Expr l) =.. Quant q r = ord e . Expr $ QuantOp Eq q l r
infix 4 =..

(≥..) :: (SqlOrd t) => Expr ctx t -> Quant ctx t -> Expr ctx Bool
e@(Expr l) ≥.. Quant q r = ord e . Expr $ QuantOp GEq q l r
infix 4 ≥..

(≠..) :: (SqlOrd t) => Expr ctx t -> Quant ctx t -> Expr ctx Bool
e@(Expr l) ≠.. Quant q r = ord e . Expr $ QuantOp NEq q l r
infix 4 ≠..

-- $outerdoesnottypecheck
-- >>> :{
-- r = table (uncheckedTableSchema Nothing #dbl (Proxy @'["x" :@ Int64, "twice" :@ Int64])) #dbl
--     & summarise Nil
--         (#a <~ arrayAgg (#x =.. some (
--             values [#x <~ lit @Int64 1]
--             & summarise Nil (#c <~ max (outer #twice)))))
--     & unordered
-- :}
-- <BLANKLINE>
-- <interactive>:1326:42: error: [GHC-64725]
--     • Cannot use outer within a Summary
--     • In the first argument of ‘max’, namely ‘(outer #twice)’
--       In the second argument of ‘(<~)’, namely ‘max (outer #twice)’
--       In the second argument of ‘summarise’, namely
--         ‘(#c <~ max (outer #twice))’

-- $outerdoesnottypecheck1
-- >>> :{
-- r = table (uncheckedTableSchema Nothing #dbl (Proxy @'["x" :@ Int64, "twice" :@ Int64])) #dbl
--     & summarise Nil
--         (#a <~ arrayAggSelectiveOrderBy
--             (#x `remainder` 2 ≥ 1)
--             ( asc (#x =.. some (
--                 values [#x <~ lit @Int64 1]
--                 & summarise Nil (#c <~ max (outer #twice))))
--             :| [])
--             #x)
--     & unordered
-- :}
-- <BLANKLINE>
-- <interactive>:1363:46: error: [GHC-64725]
--     • Cannot use outer within a Summary
--     • In the first argument of ‘max’, namely ‘(outer #twice)’
--       In the second argument of ‘(<~)’, namely ‘max (outer #twice)’
--       In the second argument of ‘summarise’, namely
--         ‘(#c <~ max (outer #twice))’

-- $outerdoesnottypecheck2
-- >>> :{
-- r = table (uncheckedTableSchema Nothing #dbl (Proxy @'["x" :@ Int64, "twice" :@ Int64])) #dbl
--     & summarise Nil
--         (#a <~ arrayAggSelectiveOrderBy
--             (#x =.. some (
--                 values [#x <~ lit @Int64 1]
--                 & summarise Nil (#c <~ max (outer #twice))))
--             (asc #x :| [])
--             #x)
--     & unordered
-- :}
-- <BLANKLINE>
-- <interactive>:1401:46: error: [GHC-64725]
--     • Cannot use outer within a Summary
--     • In the first argument of ‘max’, namely ‘(outer #twice)’
--       In the second argument of ‘(<~)’, namely ‘max (outer #twice)’
--       In the second argument of ‘summarise’, namely
--         ‘(#c <~ max (outer #twice))’

-- $outerdoesnottypecheck3
-- >>> :{
-- r = table (uncheckedTableSchema Nothing #dbl (Proxy @'["x" :@ Int64, "twice" :@ Int64])) #dbl
--      & select (lit @Int64 1 =.. some (
--          withCommon (
--              values [#x <~ lit @Int64 1]
--              & select (#x ≥ outer #twice))
--          \c ->
--          common c & reexpress (#i <~ 1)))
--      & unordered
-- :}
-- <BLANKLINE>
-- <interactive>:1438:30: error: [GHC-64725]
--     • No outer attribute named "twice" exists here
--     • In the second argument of ‘(≥)’, namely ‘outer #twice’
--       In the first argument of ‘select’, namely ‘(#x ≥ outer #twice)’
--       In the second argument of ‘(&)’, namely
--         ‘select (#x ≥ outer #twice)’

-- | Produce tuples in an arbitrary order.
unordered :: Rel (Params params) as -> List params as
unordered (Rel w) = List $ Unordered w

-- | Produce tuples ordered by some expressions. If the expressions do not
-- determine a total order, the final ordering is not defined.
ordered :: NonEmpty (Order (Across as (Params params))) -> Rel (Params params) as -> List params as
ordered = ordered' Nothing

-- | Produce the first N tuples ordered by some expressions. If the expressions
-- do not determine a total order, the final ordering is not defined.
firstNOrdered :: Word32 -> NonEmpty (Order (Across as (Params params))) -> Rel (Params params) as -> List params as
firstNOrdered = ordered' . Just

ordered' :: Maybe Word32 -> NonEmpty (Order (Across as (Params params))) -> Rel (Params params) as -> List params as
ordered' f os (Rel w) = List $ Ordered (coerce os) All f w

-- | An expression used for ordering a context @ctx@.
newtype Order ctx
  = Order (E, Dir)

-- TODO: should this demand SqlOrd?
-- | Order by expression ascending.
asc :: Expr ctx a -> Order ctx
asc (Expr e) = Order (e, Asc)

-- | Order by expression descending.
desc :: Expr ctx a -> Order ctx
desc (Expr e) = Order (e, Desc)

reflects :: forall as. (All KnownAttr as) => NP (K A) as
reflects = cpure_NP @KnownAttr @as Proxy mkA
  where
    mkA :: forall a. KnownAttr a => K A a
    mkA = K $ attribute @a Proxy

data NotArrayA (a :: Attr) where
  NotArrayA :: (NotArray (Rep t)) => NotArrayA (n :@ t)

-- | Something that can be passed as a parameter.
newtype Is (a :: Attr)
  = Is (K [P.Param] a)

-- | A simple 'SqlType' can be passed as a parameter.
--
-- /Example:/
--
-- >>> i = #x `is` (0 :: Int32) :: Is ("x" :@ Int32)
is :: forall t name. (SqlType t) => Label name -> t -> Is (name :@ t)
is _ = Is . K . pure . P.param . coerce @t @(Rep t)

newtype Meres a
  = Meres [Named I a]

unzipMeres :: forall c as. (All c as) => Proxy c -> [NP (Named I) as] -> NP Meres as
unzipMeres p = foldl' prep (cpure_NP p (Meres []))
  where
    prep :: NP Meres bs -> NP (Named I) bs -> NP Meres bs
    prep = \case
        Nil              -> (\Nil -> Nil)
        (Meres hs :* ts) -> (\(h :* t) -> Meres (h : hs) :* prep ts t)

class (KnownAttr a) => KnownNonArrayAttr (a :: Attr) where
    notArrayA :: NotArrayA a
instance (KnownAttr a, a ~ (n :@ t), NotArray (Rep t)) => KnownNonArrayAttr a where
    notArrayA = NotArrayA

-- | A bag of rel-tuples can be passed as a parameter. Array values are not supported.
--
-- /Example:/
--
-- >>> i = #xs `isValues` [#x <~ I (0 :: Int32)] :: Is ("xs" :@ Rel Ø '["x" :@ Int32])
isValues ::
    forall as name.
    (All KnownNonArrayAttr as)
 => Label name
 -> [NP (Named I) as]
 -> Is (name :@ Rel Ø as)
isValues _ ars = case cpure_NP @KnownNonArrayAttr @as Proxy refT of
    Nil         -> Is $ K [P.uvecParam $ A.oneD (U.fromList $ replicate (length ars) True)]
    ts@(_ :* _) -> Is . K $ vectorise ts ars
  where
    refT :: forall a. (KnownAttr a) => SingAT a
    refT = singAT (Proxy @a)
    vectorise ts as = collapse_NP $ czipWith_NP (Proxy @KnownNonArrayAttr) envec ts $
        unzipMeres (Proxy @KnownNonArrayAttr) as
    envec :: forall a. KnownNonArrayAttr a => SingAT a -> Meres a -> K P.Param a
    envec s@SingAT (Meres ts) = case s of
        (_ :: SingAT (n :@ t)) -> case notArrayA @a of
            NotArrayA -> K . hasksP (proxy# @t) $ coerce ts

newtype Row xs
  = Row { unRow :: NP (Named I) xs }

deriving instance (Show (NP (Named I) xs)) => Show (Row xs)
deriving instance (Eq (NP (Named I) xs)) => Eq (Row xs)

instance (All KnownAttr as) => P.FromRecord (Row as) where
    fromRecord sd cis = case NP.fromList @as cis of
        Nothing  -> fail "record length mismatch"
        Just cnp -> coerce $ ctraverse'_NP (Proxy @KnownAttr) dec cnp
      where
        dec :: forall a. KnownAttr a => K P.ColumnInfo a -> P.Parser (Named I a)
        dec (K ci) = case singAT (Proxy @a) of
            s@SingAT -> case s of
                (_ :: SingAT (n :@ t)) -> coerce $ P.column @(Rep t) sd ci

newtype Into r as
  = Into r

instance (All KnownAttr as, IsSubrecordOf (Fielded as) r) => P.FromRecord (Into r as) where
    fromRecord sd cis = do
        Row nps <- P.fromRecord @(Row as) sd cis
        pure $! Into . subrecord $ Fielded nps

prepare :: (Offsets -> a -> (b, Vector P.Param)) -> a -> NP Is params -> (b, [P.Param])
prepare render w npp = (sel, ps)
  where
    (sel, addParams) = render o w
    o = ptsOffsets $ length <$> pss
    ps = concat pss <> toList addParams
    pss = collapse_NP $ coerce npp

query ::
    forall params as.
    (All KnownAttr as)
 => P.Connection
 -> List params as
 -> NP Is params
 -> IO [NP (Named I) as]
query conn (List w) npp = case knownOids @as of
    Nil  -> fmap (const Nil) <$> P.oneshot @Bool conn sql ps [P.oneOid (Proxy @Bool)]
    oids -> coerce @[Row as] <$> P.oneshot conn sql ps (collapse_NP oids)
  where
    (S.renderSelect -> sql, ps) = prepare renderL w npp

queryInto ::
    forall r params as.
    (All KnownAttr as, IsSubrecordOf (Fielded as) r)
 => P.Connection
 -> List params as
 -> NP Is params
 -> IO [r]
queryInto conn (List w) npp = case knownOids @as of
    Nil  -> fmap (const $ subrecord (Fielded Nil)) <$> P.oneshot @Bool conn sql ps [P.oneOid (Proxy @Bool)]
    oids -> coerce @[Into r as] <$> P.oneshot conn sql ps (collapse_NP oids)
  where
    (S.renderSelect -> sql, ps) = prepare renderL w npp

knownOids :: forall as. (All KnownAttr as) => NP (K P.Oid) as
knownOids = cpure_NP @KnownAttr @as Proxy mkOid
  where
    mkOid :: forall a. KnownAttr a => K P.Oid a
    mkOid = case singAT (Proxy @a) of
        s@SingAT -> case s of
            (_ :: SingAT (n :@ t)) -> K $ P.oneOid (Proxy @(Rep t))

-- | /Example:/
--
-- >>> sch = uncheckedTableSchema Nothing #classes (Proxy @'["subject" :@ Text, "room" :@ Text])
-- >>> add = \c -> insert c sch #classes (values [#room <~ "Pool" :* #subject <~ "Basket-weaving"]) Nil
insert ::
    forall params as as' cs name schema i.
    (All KnownAttr as, KnownSymbol name, Elem schema name i (Table cs as'), Equiv as as')
 => P.Connection
 -> SchemaRef schema
 -> Label name
 -> Rel (Params params) as
 -> NP Is params
 -> IO Int
insert conn sch name (Rel w) npp = P.oneshotCmd conn sql ps >>= \case
    P.InsertTag _ i -> pure i
    t               -> throwIO $ P.UnexpectedResponse (show t)
  where
    (S.renderInsert -> sql, ps) = prepare renderC c npp
    c = Insert (schemaObject sch name) (Unordered w)

-- $doesnottypecheck
-- >>> :{
-- i = insert undefined (uncheckedTableSchema Nothing #xs (Proxy @'[])) #ys (values [Nil]) Nil
-- :}
-- <BLANKLINE>
-- <interactive>:1557:6: error: [GHC-64725]
--     • No attribute named "ys" exists here
--     • In the expression:
--         insert
--           undefined (uncheckedTableSchema Nothing #xs (Proxy @'[])) #ys
--           (values [Nil]) Nil
--       In an equation for ‘i’:
--           i = insert
--                 undefined (uncheckedTableSchema Nothing #xs (Proxy @'[])) #ys
--                 (values [Nil]) Nil

-- $doesnottypecheck1
-- >>> :{
-- i = insert undefined (uncheckedTableSchema Nothing #xs (Proxy @'[])) #xs
--   (values [#count <~ lit @Int32 5])
--   Nil
-- :}
-- <BLANKLINE>
-- <interactive>:1587:6: error: [GHC-64725]
--     • No attribute named "count" exists here
--     • In the expression:
--         insert
--           undefined (uncheckedTableSchema Nothing #xs (Proxy @'[])) #xs
--           (values [#count <~ lit @Int32 5]) Nil
--       In an equation for ‘i’:
--           i = insert
--                 undefined (uncheckedTableSchema Nothing #xs (Proxy @'[])) #xs
--                 (values [#count <~ lit @Int32 5]) Nil

-- $doesnottypecheck2
-- >>> :{
-- i = insert undefined (uncheckedTableSchema Nothing #xs (Proxy @'["count" :@ Int64])) #xs
--   (values [#count <~ lit @Int32 5])
--   Nil
-- :}
-- <BLANKLINE>
-- <interactive>:1619:6: error: [GHC-18872]
--     • Couldn't match type ‘Int64’ with ‘Int32’
--         arising from a use of ‘insert’
--     • In the expression:
--         insert
--           undefined
--           (uncheckedTableSchema Nothing #xs (Proxy @'["count" :@ Int64])) #xs
--           (values [#count <~ lit @Int32 5]) Nil
--       In an equation for ‘i’:
--           i = insert
--                 undefined
--                 (uncheckedTableSchema Nothing #xs (Proxy @'["count" :@ Int64])) #xs
--                 (values [#count <~ lit @Int32 5]) Nil

-- | Update existing tuples in the target relation. For a given @target@, @source@ and @condition@:
--
-- * Set @source' = target & 'join' source & 'select' condition@
-- * For each tuple in @source'@, update the originating tuple in @target@ per the provided updates
-- specification. It is an error if a tuple in @target@ produces more than on tuple in @source'@.
--
-- /Examples:/
--
-- >>> classSch = uncheckedTableSchema Nothing #classes (Proxy @'["lecturer" :@ Text, "subject" :@ Text, "room" :@ Text])
-- >>> roomSch = uncheckedTableSchema Nothing #rooms (Proxy @'["room" :@ Text, "available" :@ Bool])
--
-- Move all classes in unavailable rooms:
--
-- >>> :{
-- move :: Connection -> IO Int
-- move = \c -> update c classSch #classes
--     (table roomSch #rooms & select (nicht #available))
--     (lit True)
--     (#room <~ "The Quad")
--     Nil
-- :}
--
-- Improve all classes but those by Einstein:
--
-- >>> :{
-- improve :: Connection -> IO Int
-- improve = \c -> update c classSch #classes
--     (values [Nil])
--     (#lecturer ≠ "Albert Einstein")
--     (#subject <~ "Advanced Napping")
--     Nil
-- :}
update ::
    forall params as tas js sets0 sets cs name schema i.
    (All KnownAttr (sets0 : sets), All KnownAttr js, KnownSymbol name, Elem schema name i (Table cs tas), Subset tas (sets0 : sets), IntersectAttrs as tas js)
 => P.Connection
 -> SchemaRef schema
 -> Label name  -- ^ Target relation
 -> Rel (Params params) as  -- ^ Source relation
 -> Expr (Across (UnionAttrs tas as) (Params params)) Bool  -- ^ Condition
 -> NP (Named (Expr (Across (UnionAttrs tas as) (Params params)))) (sets0 : sets)  -- ^ Updates
 -> NP Is params
 -> IO Int
update conn sch name (Rel w) (Expr sel) sets npp = P.oneshotCmd conn sql ps >>= \case
    P.MergeTag i -> pure i
    t            -> throwIO $ P.UnexpectedResponse (show t)
  where
    (S.renderMerge -> sql, ps) = prepare renderU u npp
    jn = collapse_NP (reflects @js)
    u = Update (schemaObject sch name) w jn sel setting
    setting = case sets of
        Named (Expr e) :* vs -> (reflectN @sets0 Proxy, e) :| namedExprEs vs
