{-# language TypeFamilies #-}

module Database.Kigoe.Internal.Array
  ( DVector (..)
  , Dim (..), D1, D2, D3
  , arrayEq
  , oneD, unsafeManyD, foldlD', foldlD1'
  , Lengths (..)
  , nDims
  , Array (..)
  , dVectorArray, arrayDVector
  , oneDArray, manyDArray
  , arrayElements, arrayLengths
  , (!?), (!*)
  ) where

import Data.SOP (K (..), NP (..))
import Data.SOP.NP (collapse_NP)
import Data.Vector.Generic qualified as G
import RIO

-- $setup
-- >>> import Data.Vector (fromList)

-- | Type representing a single dimension for use with 'Array'.
type data Dim = D

newtype Lengths dims
  = Lengths (NP (K Int) dims)

type D1 = '[]
type D2 = '[D]
type D3 = '[D, D]

instance Ord (Lengths dims) where
    compare (Lengths Nil) (Lengths Nil) = EQ
    compare (Lengths (a :* as)) (Lengths (b :* bs)) =
        compare a b <> compare (Lengths as) (Lengths bs)

instance Eq (Lengths dims) where
    a == b = a `compare` b == EQ

instance Show (Lengths dims) where
    show (Lengths Nil) = "Lengths Nil"
    show (Lengths as) = "Lengths (" <> cs as <> ")"
      where
        cs :: forall xs. NP (K Int) xs -> String
        cs Nil       = "Nil"
        cs (b :* bs) = show b <> " :* " <> cs bs

data DVector (dims :: [Dim]) v a
  = DVector
    { lengths  :: Lengths (D : dims)
    , contents :: v a
    }
  deriving (Eq, Foldable, Functor, Ord, Show)

-- | An n-dimensional array or matrix.
--
-- /Example:/
--
-- >>> a = oneDArray (fromList [1..3 :: Int]) :: Array D1 Int
--
data Array (dims :: [Dim]) a
  = Array
    { vlengths  :: Lengths (D : dims)
    , vcontents :: Vector a
    }
  deriving (Eq, Functor, Ord, Show)
type role Array nominal representational

arrayDVector :: Array dims a -> DVector dims Vector a
arrayDVector (Array a b) = DVector a b

dVectorArray :: DVector dims Vector a -> Array dims a
dVectorArray (DVector a b) = Array a b

arrayEq :: (Eq (v a)) => DVector dims0 v a -> DVector dims1 v a -> Bool
arrayEq (DVector lens0 contents0) (DVector lens1 contents1) =
    lengthsEq lens0 lens1 && contents0 == contents1

lengthsEq :: Lengths a -> Lengths b -> Bool
lengthsEq (Lengths Nil) (Lengths Nil) = True
lengthsEq (Lengths (K l :* as)) (Lengths (K r :* bs)) =
    l == r && lengthsEq (Lengths as) (Lengths bs)
lengthsEq _ _ = False

stripD :: G.Vector v a => DVector (d : dims) v a -> [DVector dims v a]
stripD (DVector (Lengths (K n :* K m :* lt)) vs) = slice <$> [0..n - 1]
  where
    ls = K m :* lt
    slice i = DVector (Lengths ls) $ G.slice (i * len) len vs
    len = product $ collapse_NP ls

foldlD' :: G.Vector v a => (b -> DVector dims v a -> b) -> b -> DVector (d : dims) v a -> b
foldlD' f b = foldl' f b . stripD

foldlD1' :: G.Vector v a => (b -> a -> b) -> b -> DVector '[] v a -> b
foldlD1' f b = G.foldl' f b . contents

oneD :: G.Vector v a => v a -> DVector '[] v a
oneD v = DVector (Lengths (K (G.length v) :* Nil)) v

unsafeManyD :: G.Vector v a => NP (K Int) (D : dims) -> v a -> DVector dims v a
unsafeManyD ls v
  | G.length v == total = DVector (Lengths ls) v
  | otherwise           = error "unsafeManyD: lengths do not match length of provided vector"
  where
    total = product $ collapse_NP ls

nDims :: Lengths dims -> Int
nDims (Lengths Nil)       = 0
nDims (Lengths (_ :* ls)) = nDims (Lengths ls) + 1

-- | Produce a one-dimensional array from a Vector.
oneDArray :: Vector a -> Array D1 a
oneDArray v = Array (Lengths (K (G.length v) :* Nil)) v

-- | Produce an array with dimensions of the specified lengths.
-- Returns 'Nothing' if the provided lengths do not match the length of the content Vector.
manyDArray :: NP (K Int) (D : dims) -> Vector a -> Maybe (Array dims a)
manyDArray ls v
  | G.length v == total = Just $ Array (Lengths ls) v
  | otherwise           = Nothing
  where
    total = product $ collapse_NP ls

-- | Lengths of each dimension of the array.
arrayLengths :: Array dims a -> NP (K Int) (D : dims)
arrayLengths (Array (Lengths ls) _) = ls

-- | All elements of the array in storage order.
arrayElements :: Array dims a -> Vector a
arrayElements = vcontents

-- | Safe indexing into an array.
(!?) :: Array D1 a -> Int -> Maybe a
Array _ v !? i = v G.!? i
infixl 9 !?

-- | Safe multi-dimensional indexing into an array.
(!*) :: Array dims a -> NP (K Int) (D : dims) -> Maybe a
Array (Lengths ls) v !* is = v G.!? ix ls is 0
  where
    ix :: NP (K Int) (d : ds) -> NP (K Int) (d : ds) -> Int -> Int
    ix (K l :* Nil)         (K i :* Nil) acc = acc * l + i
    ix (K l :* lt@(_ :* _)) (K i :* it)  acc = ix lt it $ acc * l + i
infixl 9 !*
