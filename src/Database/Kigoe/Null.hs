-- | For some of the examples in this module, define:
--
-- @
-- someTable n as = table (uncheckedTableSchema Nothing n as) n
-- @
module Database.Kigoe.Null
  (
  -- * Operations on expressions
    liftNull
  , coalesce
  , forNull
  -- * Operations on relations
  , selectNullable
  , selectNotNull
  -- * Summaries
  , maxSelective
  -- * Types
  , Nulled
  , NullableAttrRef
  ) where

import Database.Kigoe.Internal.Null
