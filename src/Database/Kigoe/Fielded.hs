{-# language AllowAmbiguousTypes, TypeFamilies, UndecidableInstances #-}

module Database.Kigoe.Fielded
  ( Label, Named (..), (<~), forget, Name
  , Fielded (..)
  -- * Re-exports
  , HasField (..)
  ) where

import Data.Kind (Type)
import Data.SOP.BasicFunctors (I (..))
import Data.SOP.NP (NP (..))
import Data.Type.Equality (type (==), type (~))
import Database.Kigoe.Internal.Set (Attr (..))
import GHC.OverloadedLabels (IsLabel (..))
import GHC.Records (HasField (..))
import GHC.TypeLits (Symbol, TypeError)
import GHC.TypeLits qualified as TL
import RIO

-- $setup
-- >>> import Database.Kigoe
-- >>> import RIO

-- | An arbitrary name, for use with the OverloadedLabels extension.
--
-- /Example:/
--
-- >>> #name :: Label "name"
-- Label
data Label (a :: Symbol) = Label
  deriving (Show)

instance (a ~ b) => IsLabel a (Label b) where
    fromLabel = Label

type family AttrType (a :: Attr) where
    AttrType (_ :@ t) = t

-- | An expression given a name via '<~'.
newtype Named (expr :: Type -> Type) (a :: Attr)
  = Named (expr (AttrType a))
deriving instance (a ~ (n :@ t), Show (expr t)) => Show (Named expr a)
deriving instance (a ~ (n :@ t), Eq (expr t)) => Eq (Named expr a)

class Name name f a b | b -> name f a where
    -- | Name an 'Expr' or 'Summary' for use with 'values', 'extend' or 'summarise',
    -- or a value for use with 'isValues'. Overloaded to allow omitting 'Nil'.
    --
    -- /Examples:/
    --
    -- >>> e = #truthiness <~ lit False :: Named (Expr ctx) ("truthiness" :@ Bool)
    -- >>> s = #count <~ count :: Named (Summary ctx) ("count" :@ Int64)
    -- >>> v0 = #x <~ I True :: Named I ("x" :@ Bool)
    -- >>> v1 = #x <~ I True :: NP (Named I) '["x" :@ Bool]
    -- >>> v2 = #x <~ I True :* #y <~ I 2 :: NP (Named I) '["x" :@ Bool, "y" :@ Int32]
    (<~) :: Label name -> f a -> b

instance (f ~ f', b' ~ (name :@ a)) => Name name f a (Named f' b') where
    (<~) _ = Named

instance (f ~ f', bs ~ '[name :@ a]) => Name name f a (NP (Named f') bs) where
    (<~) _ = (:* Nil) . Named

infix 6 <~

-- | Strip the name from a value.
forget :: Label name -> Named f (name :@ a) -> f a
forget _ (Named x) = x

newtype Fielded xs
  = Fielded (NP (Named I) xs)

instance
    (t ~ TypeError
        ( TL.Text "No field named " TL.:<>: TL.ShowType l TL.:<>: TL.Text " exists" TL.:$$:
          TL.Text "  at type " TL.:<>: TL.ShowType (Fielded '[])
        )
    )
 => HasField l (Fielded '[]) t where
    getField = error "HasField '[]"
instance (FindField ((m :@ u) : as) l (l == m) (m :@ u) as t)
 => HasField l (Fielded ((m :@ u) : as)) t where
    getField = found @((m :@ u) : as) @l @(l == m) @(m :@ u) @as

class FindField (ctx :: [Attr]) (l :: Symbol) (eq :: Bool) a as t | ctx a as eq l -> t where
    found :: Fielded (a : as) -> t

instance (t ~ u) => FindField ctx l 'True (l :@ u) as t where
    found (Fielded (Named (I h) :* _)) = h
instance
    (t ~ TypeError
        ( TL.Text "No field named " TL.:<>: TL.ShowType l TL.:<>: TL.Text " exists" TL.:$$:
          TL.Text "  at type " TL.:<>: TL.ShowType (Fielded ctx)
        )
    )
 => FindField ctx l 'False (m :@ u) '[] t where
    found = error "FindField '[]"
instance (FindField ctx l (l == m) (m :@ u) as t)
 => FindField ctx l 'False x ((m :@ u) : as) t where
    found (Fielded (_ :* t)) = found @ctx @l @(l == m) $ Fielded t
